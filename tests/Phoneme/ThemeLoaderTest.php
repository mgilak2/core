<?php

use AdminTheme\AdminThemeServiceProvider;
use Phoneme\Contracts\PhonemeTheme;
use Phoneme\ThemeManager;

class ThemeLoaderTest extends TestCase
{
    /**
     * @var ThemeManager
     */
    protected $themeLoader;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockApp;
    protected $baseFolder = __DIR__ . "/../../app/Themes";
    public function setUp()
    {
        parent::setUp();

        $this->mockApp = Mockery::mock("\\Illuminate\\Contracts\\Foundation\\Application");
        $this->themeLoader = new ThemeManager($this->mockApp);
    }

    public function testLoadShouldCreateAnInstanceOfServiceProviderAndCallRegisterFunctionOnThem()
    {
        $mockAdmin = Mockery::mock(PhonemeTheme::class);
        $mockAdmin->shouldReceive("register")->once();

        $this->mockApp->shouldReceive("make")
            ->with(AdminThemeServiceProvider::class, [$this->mockApp])->once()
        ->andReturn($mockAdmin);

        $this->themeLoader->setAllServiceProvidersFrom($this->baseFolder);
        $this->themeLoader->registerServiceProviders();
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }
}