<?php

use Mockery as m;
use Phoneme\Installation\ComposerManager;
use Phoneme\Installation\InstallPhoneme;
use Phoneme\Installation\Migrator;
use Phoneme\Installation\Seeder;

class InstallPhonemeTest extends TestCase
{
    /**
     * @var InstallPhoneme
     */
    protected $installer;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockMigrator;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockSeeder;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockComposer;

    public function setUp()
    {
        parent::setUp();
        $this->mockMigrator = m::mock(Migrator::class);
        $this->mockSeeder = m::mock(Seeder::class);
        $this->mockComposer = m::mock(ComposerManager::class);
        $this->installer = new InstallPhoneme($this->mockComposer, $this->mockMigrator, $this->mockSeeder);
    }

    public function testMigrateShouldMigrateAllMigratableComponents()
    {
        $this->mockMigrator->shouldReceive("setAllServiceProvidersFrom")->once();
        $this->mockMigrator->shouldReceive("migrateAll")->once();
        $this->mockComposer->shouldReceive("setEnvironment")->with("")->once();
        $this->installer->migrate();
    }

    public function testSeedShouldMigrateAllSeedableComponents()
    {
        $this->mockSeeder->shouldReceive("setAllServiceProvidersFrom")->once();
        $this->mockSeeder->shouldReceive("seedAll")->once();
        $this->mockComposer->shouldReceive("setEnvironment")->with("")->once();
        $this->installer->seed();
    }

    public function testComposeShouldComposeAllComponents()
    {
        $this->mockSeeder->shouldReceive("setAllServiceProvidersFrom")->once();
        $this->mockSeeder->shouldReceive("seedAll")->once();
        $this->mockComposer->shouldReceive("setEnvironment")->with("")->once();
        $this->installer->seed();
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}