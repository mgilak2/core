<?php

use Illuminate\Support\Facades\Schema;
use Mockery as m;
use Phoneme\Installation\Migrator;

class MigratorTest extends TestCase
{
    /**
     * @var Migrator
     */
    protected $migrator;

    public function setUp()
    {
        parent::setUp();
        $this->artisan("migrate");
        $this->artisan("migrate", ["--path" => "app/Components/Settings/migrations"]);
        $this->migrator = $this->createApplication()->make(Migrator::class);
    }

    public function testMigrateAll()
    {
        $this->migrator->setAllServiceProvidersFrom(__DIR__ . "/../../../app/Components");
        $this->migrator->migrateAll();
        $this->assertTrue(Schema::hasTable('settings'));
    }

    public function tearDown()
    {
        parent::tearDown();
        m::close();
    }
}