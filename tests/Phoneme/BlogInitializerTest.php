<?php

use Illuminate\Contracts\Foundation\Application;
use Phoneme\BlogInitializer;
use Phoneme\ComponentManager;
use Phoneme\HookManager;
use Phoneme\ThemeManager;

class BlogInitializerTest extends PHPUnit_Framework_TestCase
{
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockComponentManager;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockThemeManager;
    /**
     * @var BlogInitializer
     */
    protected $blogInitializer;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockHookManager;

    public function setUp()
    {
        parent::setUp();

        $this->mockComponentManager = Mockery::mock(ComponentManager::class);
        $this->mockThemeManager = Mockery::mock(ThemeManager::class);
        $this->mockHookManager = Mockery::mock(HookManager::class);
        $mockApp = Mockery::mock(Application::class);
        $this->blogInitializer = new BlogInitializer($mockApp,
            $this->mockHookManager,
            $this->mockComponentManager, $this->mockThemeManager);
    }

    public function testLoadShouldStepComponentsAndThemesIntoPlatfrom()
    {
        $this->mockComponentManager->shouldReceive("setAllServiceProvidersFrom")->once();
        $this->mockComponentManager->shouldReceive("registerServiceProviders")
            ->once();
        $this->mockComponentManager->shouldReceive("callBootMethodOfServiceProviders")
            ->once();

        $this->mockThemeManager->shouldReceive("setAllServiceProvidersFrom")->once();
        $this->mockThemeManager->shouldReceive("registerServiceProviders")
            ->once();
        $this->mockThemeManager->shouldReceive("callBootMethodOfServiceProviders")
            ->once();

        $this->mockHookManager->shouldReceive("initialsAllResponders")->once();

        $this->blogInitializer->load();
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }
}