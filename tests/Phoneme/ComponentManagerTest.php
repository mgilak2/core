<?php

use Phoneme\ComponentManager;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class ComponentManagerTest extends TestCase
{
    protected $baseFolder = __DIR__ . "/../../app/Components";

    /**
     * @var ComponentManager
     */
    protected $componentLoader;
    /**
     * @var \Mockery\MockInterface
     */
    protected $mockApp;

    public function setUp()
    {
        parent::setUp();

        $this->mockApp = Mockery::mock("\\Illuminate\\Contracts\\Foundation\\Application");
        $this->componentLoader = new ComponentManager($this->mockApp);
    }

    public function testLoadShouldCreateAnInstanceOfServiceProviderAndCallRegisterFunctionOnThem()
    {
        $mockComponent = Mockery::mock(PhonemeComponent::class);
        $mockComponent->shouldReceive("register");
        $this->mockApp->shouldReceive("make")
            ->andReturn($mockComponent)
            ->times(21);

        $this->componentLoader->setAllServiceProvidersFrom($this->baseFolder);
        $this->componentLoader->registerServiceProviders();
    }

    public function testLoadShouldCreateAnInstanceOfServiceProviderAndCallBootFunctionOnThem()
    {
        $mockComponent = Mockery::mock(Initializable::class);
        $mockComponent->shouldReceive("boot");
        $this->mockApp->shouldReceive("make")
            ->andReturn($mockComponent)
            ->times(21);

        $this->componentLoader->setAllServiceProvidersFrom($this->baseFolder);
        $this->componentLoader->callBootMethodOfServiceProviders();
    }

    public function tearDown()
    {
        parent::tearDown();
        Mockery::close();
    }
}