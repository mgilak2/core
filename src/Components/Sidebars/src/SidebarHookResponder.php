<?php

namespace Sidebars;

use Phoneme\Contracts\CanManageHooks;
use Phoneme\Contracts\Hook;
use Sidebars\Contracts\SidebarArea;

class SidebarHookResponder implements CanManageHooks
{
    protected $hook;

    public function add(Hook $hook)
    {
        $this->hook = $hook;
    }

    /**
     * @return SidebarArea
     */
    public function getHook()
    {
        return $this->hook;
    }
}