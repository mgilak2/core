<?php

namespace Sidebars;

use Illuminate\Support\ServiceProvider;
use Phoneme\Contracts\PhonemeComponent;

class SidebarsServiceProvider extends ServiceProvider implements PhonemeComponent
{
    public function register()
    {
        $this->app->singleton(SidebarHookResponder::class, function ($app) {
            return new SidebarHookResponder();
        });
    }
}