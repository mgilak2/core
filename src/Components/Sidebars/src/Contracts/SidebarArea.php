<?php

namespace Sidebars\Contracts;

interface SidebarArea
{
    public function getAreas();
}