@extends("Admin::dashboard")

@section("master.ticket.form.add")
    <section id="rootwizard" class="tabbable transparent tile wizard-forms">

        <div class="tile-header transparent">
            <h1>لسیت اسلاید ها</h1>

            <div class="controls">
                <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
                <a href="#" class="remove"><i class="fa fa-times"></i></a>
            </div>
        </div>

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>id</th>
                <th>عنوان</th>
                <th>تصویر</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach ($comments as $index => $comment)
                    <td>{{ ++$index }}</td>
                    <td>{{ $comment->id }}</td>
                    <td>{{ $comment->subject }}</td>
                    <td>{{ $comment->featured_image }}</td>
            </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@stop