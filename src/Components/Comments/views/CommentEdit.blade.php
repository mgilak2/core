@extends("Admin::dashboard")

@section("master.ticket.form.add")
    <section id="rootwizard" class="tabbable transparent tile wizard-forms">

        <div class="tile-header transparent">
            <h1>ویرایش مطلب</h1>

            <div class="controls">
                <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
                <a href="#" class="remove"><i class="fa fa-times"></i></a>
            </div>
        </div>

        <div class="tile-body tile color transparent-black">
            @if(isset($comment))
                {!! Form::model($comment, ['route' => ['comment.edit', $comment->id]]) !!}
            @else
                {!! Form::open(['route' => 'comment.add']) !!}
            @endif
            <div>
                <label class="control-label">نظر :</label>
                {!! Form::textarea('body', null, ['class' => 'redactor']) !!}
            </div>
            <div class="row">
                <div class="col-sm-2">
                    <button type="submit" class="btn btn-primary btn-block" role="button">ثبت</button>
                </div>
                <div class="col-sm-2">
                    <button class="btn btn-warning btn-block" role="button" type="reset">انصراف</button>
                </div>
            </div>
            {!! Form::close() !!}
        </div>
    </section>
@stop