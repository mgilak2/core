@extends("Admin::dashboard")

@section("master.ticket.form.add")
    <div class="grid grid-max center-box-shadow">
        <div class="grid-title">
            <h4>
                <i class="icon-plus"></i>
                افزودن کامنت
            </h4>
        </div>

        <div class="grid-body">
            <div class="row">
                @if(isset($comment))
                    {!! Form::model($comment, ['route'=> ['comment.add', $comment->id]]) !!}
                @else
                    {!! Form::open(['route'=>'comment.add']) !!}
                @endif
                <div>
                    <label class="control-label">توضیحات :</label>
                    {!! Form::textarea('body', null, ['class' => 'redactor']) !!}
                </div>
                <div class="row">
                    <div class="col-sm-4 form-group">
                        <label class="control-label">تصویر اسلایدر :</label>
                        {!! Form::text('featured_image',null
                          , ['class' => 'select-transparent form-control']) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-2">
                        <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
                    </div>
                    <div class="col-sm-2">
                        <button class="btn btn-warning btn-block" role="button" type="reset">انصراف</button>
                    </div>
                </div>
                {!! Form::close() !!}
            </div>
        </div>
    </div>
@stop