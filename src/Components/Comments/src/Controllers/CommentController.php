<?php namespace Comments;

use Illuminate\View\Factory;

class CommentController extends \BaseController
{
    protected $View;

    public function __construct(Factory $view)
    {
        $this->View = $view;

    }

//    ------------------------------------------ GET -----------------------------------------

    public function getManage()
    {
        return $this->View->make("CommentModule::CommentManage")
            ->with('cats', Comment::all());
    }

    public function getAdd()
    {
        $theme = "Admin";

        return $this->View->make("CommentModule::CommentAdd",compact("cats","theme"));
    }

    public function getEdit(Comment $comment)
    {
        $theme = "Admin";

        return $this->View->make("CommentModule::CommentEdit",compact("theme",'comment'));
    }

    public function getDelete(Comment $comment)
    {
        $comment->destroy($comment->id);

        return \Redirect::route('comment.add');
    }

//    ------------------------------------------ POST -----------------------------------------

    public function postAdd()
    {
       $result = Comment::create(\Input::all());

        return \Redirect::route('comment.list');
    }

    public function postEdit(Comment $comment)
    {

        $comment->title = \Input::get('title');
        $comment->description =\Input::get('description');
        $comment->featured_image = \Input::get('featured_image');
        $comment->save();

        return \Redirect::route('comment.edit', array('id' => $comment->id));
    }

    public function getList()
    {
        $comments=Comment::all();
        $theme = "Admin";
        return $this->View->make("CommentModule::CommentList",compact("theme",'comments'));
    }
}