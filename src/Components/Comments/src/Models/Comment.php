<?php namespace Comments;

class Comment extends \Eloquent
{
    public $timestamps = false;
    protected $table = "comments";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'user_id', 'body', 'parent_id', 'post_id'];
}