<?php

namespace Comments;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class CommentsServiceProvider extends ServiceProvider implements PhonemeComponent, Initializable, CanMigrate
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var View
     */
    private $view;

    public function __construct(Application $app, Router $router, View $view)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->router = $router;
        $this->view = $view;
    }

    public function register()
    {
        // do nothing :)
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {

        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Comments', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->router->group(['prefix' => "admin/comment", 'namespace' => "Comment"], function () {
            $this->router->model("comment_id", Comment::class);

            $this->router->get('/create', ['as' => 'comment.add', 'uses' => 'CommentController@getAdd']);
            $this->router->post('/create', ['as' => 'comment.add', 'uses' => 'CommentController@postAdd']);
            $this->router->get('/edit/{comment_id}', ['as' => 'comment.edit', 'uses' => 'CommentController@getEdit']);
            $this->router->post('/edit/{comment_id}', ['as' => 'comment.edit', 'uses' => 'CommentController@postEdit']);
            $this->router->get('/delete/{comment_id}', ['as' => 'comment.delete', 'uses' => 'CommentController@getDelete']);
            $this->router->get('/', ['as' => 'comment.list', 'uses' => 'CommentController@getList']);
        });
    }
}