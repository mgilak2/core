<?php

namespace NewsLetter;

use Illuminate\Routing\Router;
use Route;
class NewsLetterRoutes
{
    protected $Router;

    public function __construct(Router $route)
    {
        $this->Router = $route;
    }

    public function routes()
    {
        \Route::filter('NewsLetter', function()
        {
            $permission = module("UserManagement");

            if(! $permission->hasPermission('NewsLetter'))
            {
                return \Redirect::route('dashboard');
            }
        });


        Route::pattern('id', '[0-9]+');

        $this->NewsLetter();
    }


    public function NewsLetter()
    {
        Route::group(['before'=>'NewsLetter','prefix'=>"admin/newsletter",'namespace'=>"NewsLetter"],function()
        {
            Route::model("newsletter_id","NewsLetter");

            Route::get('/add',['as'=>'newsletter.add','uses'=>'NewsLetterController@getAdd']);
            Route::post('/add',['as' => 'newsletter.add','uses' => 'NewsLetterController@postAdd']);

            Route::get('/edit/{newsletter_id}',['as'=>'newsletter.edit','uses'=>'NewsLetterController@getEdit']);
            Route::post('/edit/{newsletter_id}',['as' => 'newsletter.edit','uses' =>'NewsLetterController@postEdit']);

            //Route::get('/manage',['as'=>'newsletter.manage','uses'=>'NewsLetterController@getManage']);
            Route::get('/delete/{newsletter_id}', ['as' => 'newsletter.delete', 'uses' => 'NewsLetterController@getDelete']);

            Route::get('/list', ['as' => 'newsletter.list', 'uses' => 'NewsLetterController@getList']);
        });
    }

}

