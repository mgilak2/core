<?php namespace NewsLetter;

class NewsLetter extends Eloquent
{
    protected $table = "newsletter";
    protected $primaryKey = "id";
    public $timestamps = false;

    protected $fillable = ['id','email','firstname','lastname','tell'];
}