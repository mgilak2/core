<?php
namespace NewsLetter;

class NewsLetterModule extends \Aone\Module\ModuleFactory
{
    /**
     * @var RoutesTwo
     */
    protected $NewsLetterRoutes;

    public function __construct(NewsLetterRoutes $Routes)
    {
        $this->NewsLetterRoutes = $Routes;
    }

    public function route()
    {
        $this->NewsLetterRoutes->routes();
    }

    public function run()
    {
        $this->route();
    }

    public function installation()
    {
        $newsLetter = module("Menu")->add(['en'=>'News Letter','fa'=>'خبر نامه','route'=>'newsletter.list','module'=>'NewsLetter']);
        $newsLetter->add(['en'=>'create','fa'=>'اضافه کردن','route'=>'newsletter.edit']);

        module("UserManagement")->add('NewsLetter','خبر نامه','NewsLetter');
    }

    public function deactivation()
    {
        module("Menu")->remove('NewsLetter');
        module("UserManagement")->remove('NewsLetter');
    }

}