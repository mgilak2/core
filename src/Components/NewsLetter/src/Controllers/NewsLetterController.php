<?php
namespace NewsLetter;

use Illuminate\View\Factory;

class NewsLetterController extends \BaseController
{
    protected $Environment;
    protected $View;

    public function __construct(Factory $view)
    {
        $this->View = $view;
    }

//    ------------------------------------------ GET -----------------------------------------

    public function getManage()
    {
        return $this->View->make("NewsLetterModule::NewsLetterManage")
            ->with('cats', NewsLetter::all());
    }

    public function getAdd()
    {
        $theme = "Admin";

        return $this->View->make("NewsLetterModule::NewsLetterAdd", compact("theme"));
    }

    public function getEdit(NewsLetter $newsletter)
    {
        $theme = "Admin";

        return $this->View->make("NewsLetterModule::NewsLetterAdd", compact("theme", 'newsletter'));
    }

    public function getDelete(NewsLetter $newsletter)
    {
        $newsletter->destroy($newsletter->id);

        return \Redirect::route('newsletter.add');
    }

//    ------------------------------------------ POST -----------------------------------------

    public function postAdd()
    {
        $result = NewsLetter::create(\Input::all());

        return \Redirect::route('newsletter.list');
    }

    public function postEdit(NewsLetter $newsletter)
    {

        $newsletter->email = \Input::get('email');
        $newsletter->firstname = \Input::get('firstname');
        $newsletter->lastname = \Input::get('lastname');
        $newsletter->tell = \Input::get('tell');
        $newsletter->save();

        return \Redirect::route('newsletter.edit', array('id' => $newsletter->id));
    }

    public function getList()
    {
        $newsletters = NewsLetter::all();
        $theme = "Admin";
        return $this->View->make("NewsLetterModule::NewsLetterList", compact("theme", 'newsletters'));
    }
}