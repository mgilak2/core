<?php

namespace NewsLetter;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class NewsLetterServiceProvider extends ServiceProvider implements PhonemeComponent, Initializable, CanMigrate
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Application $app, View $view, Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
    }

    public function register()
    {
        // do nothing :)
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {
        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('NewsLetter', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->router->group(['before' => 'NewsLetter', 'prefix' => "admin/newsletter", 'namespace' => "NewsLetter"], function () {
            $this->router->model("newsletter_id", "NewsLetter");

            $this->router->get('/add', ['as' => 'newsletter.add', 'uses' => 'NewsLetterController@getAdd']);
            $this->router->post('/add', ['as' => 'newsletter.add', 'uses' => 'NewsLetterController@postAdd']);

            $this->router->get('/edit/{newsletter_id}', ['as' => 'newsletter.edit', 'uses' => 'NewsLetterController@getEdit']);
            $this->router->post('/edit/{newsletter_id}', ['as' => 'newsletter.edit', 'uses' => 'NewsLetterController@postEdit']);

            $this->router->get('/delete/{newsletter_id}', ['as' => 'newsletter.delete', 'uses' => 'NewsLetterController@getDelete']);

            $this->router->get('/list', ['as' => 'newsletter.list', 'uses' => 'NewsLetterController@getList']);
        });
    }
}