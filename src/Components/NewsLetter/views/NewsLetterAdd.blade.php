@extends("Admin::dashboard")

@section("master.ticket.form.add")
    @if(isset($newsletter))
        {!! Form::model($newsletter, ['route' => ['newsletter.edit', $newsletter->id]]) !!}
    @else
        {!! Form::open(['route' => 'newsletter.add']) !!}
    @endif
    <div class="row">
        <div class="col-sm-3">
            <label class="control-label">ایمیل :</label>
            {!! Form::text('email', isset($newsletter) ? $newsletter->email : Input::get("email"), ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-3">
            <label class="control-label">نام :</label>
            {!! Form::text('firstname', isset($newsletter) ? $newsletter->firstname : Input::get("firstname"), ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-3">
            <label class="control-label">نام خانوادگی :</label>
            {!! Form::text('lastname', isset($newsletter) ? $newsletter->lastname : Input::get("lastname"), ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-3">
            <label class="control-label">شماره تماس :</label>
            {!! Form::text('tell', isset($newsletter) ? $newsletter->tell : Input::get("tell"), ['class' => 'form-control']) !!}
        </div>
    </div>

    <div class="row">
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-warning btn-block" role="button" type="reset">انصراف</button>
        </div>
    </div>
    {!! Form::close() !!}
@stop