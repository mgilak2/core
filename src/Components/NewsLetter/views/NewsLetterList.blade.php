@extends("Admin::dashboard")

@section("master.ticket.form.add")

    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>id</th>
            <th>عنوان</th>
            <th>تصویر</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            @foreach ($newsletters as $index => $newsletter)
                <td>{{ ++$index }}</td>
                <td>{{ $newsletter->id }}</td>
                <td>{{ $newsletter->email }}</td>
                <td>{{ $newsletter->firstname }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
@stop