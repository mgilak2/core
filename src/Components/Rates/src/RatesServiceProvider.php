<?php

namespace Rates;

use Illuminate\Support\ServiceProvider;
use Phoneme\Contracts\PhonemeComponent;

class RatesServiceProvider extends ServiceProvider implements PhonemeComponent
{
    public function register()
    {
    }
}