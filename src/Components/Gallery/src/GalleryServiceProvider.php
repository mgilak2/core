<?php

namespace Gallery;

use Illuminate\Support\ServiceProvider;
use Phoneme\Contracts\PhonemeComponent;

class GalleryServiceProvider extends ServiceProvider implements PhonemeComponent
{
    public function register()
    {
    }
}