<?php

namespace Sliders;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class SliderServiceProvider extends ServiceProvider implements PhonemeComponent, Initializable, CanMigrate
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Application $app, View $view, Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
    }

    public function register()
    {
        // noting to do :)
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {
        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Sliders', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->router->group(['prefix' => "dashboard/slider", 'namespace' => "Sliders"], function () {
            $this->router->model("slider_id", Slider::class);

            $this->router->get('/create', ['as' => 'slider.add.get', 'uses' => 'SliderController@getAdd']);
            $this->router->post('/create', ['as' => 'slider.add', 'uses' => 'SliderController@postAdd']);
            $this->router->get('/edit/{slider_id}', ['as' => 'slider.edit', 'uses' => 'SliderController@getEdit']);
            $this->router->post('/edit/{slider_id}', ['as' => 'slider.edit', 'uses' => 'SliderController@postEdit']);
            $this->router->get('/delete/{slider_id}', ['as' => 'slider.delete', 'uses' => 'SliderController@getDelete']);
            $this->router->get('/list', ['as' => 'slider.list', 'uses' => 'SliderController@getList']);
        });
    }
}