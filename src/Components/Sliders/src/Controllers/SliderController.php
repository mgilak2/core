<?php
namespace Sliders;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class SliderController extends Controller
{
    /**
     * @var View
     */
    protected $view;
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;

    public function __construct(View $view, Redirector $redirector,
                                AdminTheme $theme, Request $request)
    {
        $this->view = $view;
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->request = $request;
    }

    public function getManage()
    {
        return $this->theme->baseDashboard("Sliders::SliderManage", ['cats' => Slider::all()]);
    }

    public function getAdd()
    {
        return $this->theme->baseDashboard("Sliders::SliderAdd");
    }

    public function getEdit(Slider $slider)
    {
        return $this->theme->baseDashboard("Sliders::SliderEdit", compact('slider'));
    }

    public function getDelete(Slider $slider)
    {
        $slider->destroy($slider->id);

        return $this->redirector->route('slider.list');
    }

    public function postAdd()
    {
        $inputs = $this->request->all();
        Slider::create($inputs);

        return $this->redirector->route('slider.list');
    }

    public function postEdit(Slider $slider)
    {
        $slider->title = $this->request->get('title');
        $slider->description = $this->request->get('description');
        $slider->featured_image = $this->request->get('featured_image');
        $slider->save();

        return $this->redirector->route('slider.edit', ['id' => $slider->id]);
    }

    public function getList()
    {
        $sliders = Slider::all();
        return $this->theme->baseDashboard("Sliders::SliderList", compact('sliders'));
    }
}