<?php namespace Sliders;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Slider extends Eloquent
{
    public $timestamps = false;
    protected $primaryKey = "id";
    protected $fillable = ['title', 'description',
        'featured_image'];
}