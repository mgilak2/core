@extends("Admin::dashboard")

@section("pageTitle")
    <i class="fa fa-list"></i>
    لیست اسلایدها

    <a href="/admin/slider/create" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>
@endsection

@section("master.ticket.form.add")
    <table class="table table-striped">
        <colgroup>
            <col style="width: 80px"/>
            <col/>
            <col style="width: 120px"/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th>تصویر</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($sliders as $index => $slider)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $slider->title }}</td>
                <td><img src="{{ $slider->featured_image }}" class="img-thumbnail" alt="" style="width: 100%"></td>
                <td>
                    <a href="{{ route('slider.edit',array($slider->id)) }}" class="btn btn-primary btn-sm"><i
                                class="icon-pencil"></i> </a>
                    <a href="{{ route('slider.delete',array($slider->id)) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')"><i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop