@extends("Admin::dashboard")

@section("pageTitle")
    <i class="fa fa-pencil"></i>
    ویرایش اسلاید
@endsection

@section("content")
    @if(isset($slider))
        {!! Form::model($slider,['route' => ['slider.edit', $slider->id]]) !!}
    @else
        {!! Form::open(['route'=> 'slider.add']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-4">
            <label class="control-label">عنوان اسلاید :</label>
            {!! Form::text('title', Input::get("title"), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <label class="control-label">توضیحات :</label>
            {!! Form::textarea('description', null, ['class' => 'redactor']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <label class="control-label">تصویر اسلایدر :</label>

            <div class="control-group pull-left" id="updateImage">
                <img src="{{ isset($slider) ? $slider->featured_image : '' }}" style="width:120px"
                     class="img-thumbnail pull-left" id="imgUpload">
                <a href="#" class="badge badge-danger animated bounceIn pull-left" id="deleteImg">حذف تصویر</a>
                {!! Form::hidden('featured_image', isset($slider) ? $slider->featured_image:Input::get("featured_image")
                              , array('id'=>'image')) !!}
                <input name="thumbs" type="hidden" id="thumbs" value=""/>
            </div>
        </div>
        <div class="col-sm-4">
            <label class="control-label db-block">&nbsp;</label>

            <div class="form-group col-sm-6">
                <button type="submit" class="btn btn-primary btn-block" role="button">ایجاد</button>
            </div>
            <div class="form-group col-sm-6">
                <a href="{{ route("slider.list") }}" class="btn btn-warning btn-block" role="button" type="reset">انصراف</a>
            </div>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('extensionScript')
    @include("Admin::extensions.filemanager")
@stop