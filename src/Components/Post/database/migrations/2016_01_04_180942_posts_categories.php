<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class PostsCategories extends Migration
{
    public function up()
    {
        Schema::create('post_category', function (Blueprint $table) {
            $table->increments('id');
            $table->string('post_id');
            $table->integer('cat_id');
        });
    }

    public function down()
    {
        Schema::drop("post_category");
    }
}
