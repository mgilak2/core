<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Posts extends Migration
{
    public function up()
    {
        Schema::create('posts', function (Blueprint $table) {
            $table->increments('id');
            $table->string('subject');
            $table->longText('content')->nullable();
            $table->string('permalink')->nullable();
            $table->string('featured_image')->nullable();
            $table->integer('type')->nullable();
            $table->integer('gallery_id')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop("posts");
    }
}
