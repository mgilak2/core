@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-pencil"></i>
    ویرایش دسته بندی
@endsection

@section("master.ticket.form.add")
    @if(isset($cat))
        {!! Form::model($cat,['route'=>['cat.edit',$cat->id]]) !!}
    @else
        {!! Form::open(['route'=>'cat.edit']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-4">
            <label class="control-label">عنوان دسته :</label>
            {!! Form::text('title',null, ['class' => 'form-control']) !!}
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label">مادر :</label>
            {!! Form::select('parent_id', $ddl_cats, isset($cat) ? $cat->parent_id : 0
              , ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-4">
            <label class="control-label db-block">&nbsp;</label>

            <div class="row">
                <div class="form-group col-sm-6">
                    <button type="submit" class="btn btn-primary btn-block" role="button">ثبت</button>
                </div>
                <div class="form-group col-sm-6">
                    <a href="{{ route("cat.add.get") }}" class="btn btn-warning btn-block" role="button" type="reset">انصراف</a>
                </div>
            </div>
        </div>
    </div>
    <table class="table table-striped">
        <colgroup>
            <col style="width: 80px"/>
            <col/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان دسته بندی</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($cats as $index => $cat)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $cat->title }} </td>
                <td>
                    <a href="{{ route('cat.getEdit',array($cat->id)) }}" class="btn btn-primary btn-sm"><i
                                class="icon-pencil"></i> </a>
                    <a href="{{ route('cat.delete',array($cat->id)) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')"><i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! Form::close() !!}
@stop