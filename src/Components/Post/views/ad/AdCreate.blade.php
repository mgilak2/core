@extends(extendAdmin("dashboard"))

@section("master.ticket.form.add")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن تبلیغ

    <a href="{{ route('ad.list') }}" class="btn btn-primary pull-left">
        <i class="fa fa-list"></i>
        لیست تبلیغات
    </a>
@endsection
@if(isset($ad))
    {{Form::model($ad,['route'=>['ad.create.post',$page->id]])}}
@else
    {{Form::open(['route'=>'ad.create.post'])}}
@endif
<div class="row">
    <div class="form-group col-sm-4 col-xs-4">
        <label class="control-label">عنوان تبلیغ :</label>
        {{Form::text('subject', null, array('class' => 'form-control'))}}
    </div>
    <div class="form-group col-sm-4 col-xs-4">
        <label class="control-label">آدرس تبلیغ :</label>
        {{Form::text('permalink', null, array('class' => 'form-control'))}}
    </div>
    <div class="form-group col-sm-4">
        <label class="control-label">زبان :</label>
        {{Form::select('language',$languages,null, array('class' => 'form-control'))}}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">

        <label class="control-label">متن تبلیغ :</label>
        {{Form::textarea('content', null, array('class' => 'redactor'))}}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-2">
        <button type="submit" class="btn btn-primary btn-block" role="button">ثبت تبلیغ</button>
    </div>
    <div class="form-group col-sm-2">
        <a href="{{route("ad.list")}}" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
    </div>
</div>
{{Form::close()}}
@stop