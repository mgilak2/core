@extends(extendAdmin("dashboard"))

@section("master.ticket.form.add")
@section("pageTitle")
    <i class="icon-pencil"></i>
    ویرایش تبلیغ
@endsection
@if(isset($ad))
    {{Form::model($ad,['route'=>['ad.update.put',$ad->id], 'method' => 'put'])}}
@else
    {{Form::open(['route'=>'ad.update.put', 'method' => 'put'])}}
@endif
<div class="row">
    <div class="form-group col-sm-4">
        <label class="control-label">عنوان صفحه :</label>
        {{Form::text('subject',Input::get("title"), array('class' => 'form-control'))}}
    </div>
    <div class="form-group col-sm-4">
        <label class="control-label">آدرس صفحه :</label>
        {{Form::text('permalink',null, array('class' => 'form-control'))}}
    </div>
    <div class="form-group col-sm-4">
        <label class="control-label">زبان :</label>
        {{Form::select('language',$languages,null, array('class' => 'form-control'))}}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        <label class="control-label">متن صفحه :</label>
        {{Form::textarea('content',null, array('class' => 'redactor'))}}
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-2">
        <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
    </div>
    <div class="form-group col-sm-2">
        <a href="{{route("ad.list")}}" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
    </div>
</div>
{{Form::close()}}
@stop