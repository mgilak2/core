@extends("Admin::dashboard")

@section("pageTitle")
    <i class="fa fa-list"></i>
    لیست صفحات

    <a href="{{ route("cat.add.get") }}" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>
@endsection

@section("master.ticket.form.add")
    <table class="table table-striped">
        <colgroup>
            <col style="width: 50px"/>
            <col/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>

        @foreach ($pages as $index => $page)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $page->subject }}</td>
                <td>
                    <a href="{{ route('page.edit',array($page->id)) }}" class="btn btn-primary btn-sm"><i
                                class="icon-pencil"></i> </a>
                    <a href="{{ route('page.delete',array($page->id)) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')"><i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop