@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن زبانه

    <a href="{{ route('tab.list') }}" class="btn btn-primary pull-left">
        <i class="fa fa-list"></i>
        لیست زبانه ها
    </a>
@endsection

@section("master.ticket.form.add")


    @if(isset($tab))
        {!! Form::model($tab,['route' => ['tab.create.post', $page->id]]) !!}
    @else
        {!! Form::open(['route' => 'tab.create.post']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-6 col-xs-6">
            <label class="control-label">عنوان:</label>
            {!! Form::text('subject', null, ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6">
            <label class="control-label">زبان :</label>
            {!! Form::select('language', $languages, null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">

            <label class="control-label">متن:</label>
            {!! Form::textarea('content', null, ['class' => 'redactor']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت زبانه</button>
        </div>
        <div class="form-group col-sm-2">
            <a href="{{ route("tab.list") }}" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
        </div>
    </div>
    {!! Form::close() !!}
@stop