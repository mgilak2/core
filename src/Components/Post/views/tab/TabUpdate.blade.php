@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-pencil"></i>
    ویرایش تبلیغ
@endsection

@section("master.ticket.form.add")

    @if(isset($tab))
        {!! Form::model($tab, ['route'=> ['tab.update.put', $tab->id], 'method' => 'put']) !!}
    @else
        {!! Form::open(['route'=> 'tab.update.put', 'method' => 'put']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-6">
            <label class="control-label">عنوان صفحه :</label>
            {!! Form::text('subject', Input::get("title"), ['class' => 'form-control']) !!}
        </div>

        <div class="form-group col-sm-6">
            <label class="control-label">زبان :</label>
            {!! Form::select('language', $languages, null, ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">
            <label class="control-label">متن صفحه :</label>
            {!! Form::textarea('content', null, ['class' => 'redactor']) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
        </div>
        <div class="form-group col-sm-2">
            <a href="{{ route("tab.list") }}" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
        </div>
    </div>
    {!! Form::close() !!}
@stop