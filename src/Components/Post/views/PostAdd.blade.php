@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن مطلب


    <a href="{{ route("post.list") }}" class="btn btn-primary pull-left">
        <i class="fa fa-list"></i>
        لیست مطالب
    </a>
@endsection

@section("master.ticket.form.add")
    @if(isset($post))
        {!! Form::model($post, ['route' => ['post.add',$post->id]]) !!}
    @else
        {!! Form::open(['route' =>'post.add']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-4">
            <label class="control-label">عنوان مطلب :</label>
            {!! Form::text('subject', Input::get("airline"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label">آدرس مطلب :</label>
            {!! Form::text('permalink', null, array('class' => 'form-control')) !!}
        </div>
    </div>


    <div class="row">
        <div class="form-group col-sm-12">
            <label class="control-label">متن مطلب :</label>
            {!! Form::textarea('content', null, array('class' => 'redactor')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-4 col-xs-4">
            <label class="control-label">دسته بندی :</label>
            {!! Form::select('post_cats', $categories, null, array('class' => 'form-control')) !!}
        </div>

    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
        </div>
        <div class="form-group col-sm-2">
            <a href="{{ route("post.list") }}" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
        </div>
    </div>
    {!! Form::close() !!}
@stop

@section('extensionScript')
    @include("Admin::extensions.filemanager")
@stop