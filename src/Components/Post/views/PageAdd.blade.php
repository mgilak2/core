@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن صفحه

    <a href="/admin/page/list/" class="btn btn-primary pull-left">
        <i class="fa fa-list"></i>
        لیست صفحات
    </a>
@endsection

@section("master.ticket.form.add")
    @if(isset($page))
        {!! Form::model($page,['route'=>['page.add',$page->id]]) !!}
    @else
        {!! Form::open(['route'=>'page.add']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-4 col-xs-4">
            <label class="control-label">عنوان صفحه :</label>
            {!! Form::text('subject',Input::get("airline"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-4 col-xs-4">
            <label class="control-label">آدرس صفحه :</label>
            {!! Form::text('permalink',null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-12">

            <label class="control-label">متن صفحه :</label>
            {!! Form::textarea('content',null, array('class' => 'redactor')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
        </div>
        <div class="form-group col-sm-2">
            <a href="{{ route("page.list") }}" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
        </div>
    </div>
    {!! Form::close() !!}
@stop