@extends("Admin::dashboard")

@section("pageTitle")

    <a href="{{ route("post.add.get") }}" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>

    <i class="fa fa-list"></i>
    لیست مطالب
@endsection

@section("master.ticket.form.add")
    <table class="table table-striped">
        <colgroup>
            <col style="width: 50px"/>
            <col/>
            <col style="width: 120px"/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th>تصویر</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($posts as $index => $post)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $post->subject }}</td>
                <td>
                    <img src="{{ $post->featured_image }}" alt="" class="img-thumbnail">
                </td>
                <td>
                    <a href="{{ route('post.edit',array($post->id)) }}" class="btn btn-primary btn-sm"><i
                                class="icon-pencil"></i> </a>
                    <a href="{{ route('post.delete',array($post->id)) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')"><i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop