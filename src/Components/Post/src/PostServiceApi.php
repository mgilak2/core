<?php

namespace Post;

class PostServiceApi
{
    /**
     * @var Post
     */
    private $post;
    /**
     * @var Post_Cat_Attached
     */
    private $postCatageroyAttachment;

    public function __construct(Post $post, Post_Cat_Attached $postCatageroyAttachment)
    {
        $this->post = $post;
        $this->postCatageroyAttachment = $postCatageroyAttachment;
    }

    public function getPost($in = "")
    {
        if ($in == "")
            return null;

        if (filter_var($in, FILTER_VALIDATE_INT)) {
            return $this->post->where('type', 1)->find($in);
        }

        return $this->post->where('type', 1)->where('permalink', $in)->first();
    }

    public function getPage($in = "")
    {

        if (empty($in))
            return null;

        if (filter_var($in, FILTER_VALIDATE_INT)) {
            return $this->post->where('type', 2)->find($in);

        }

        return $this->post->where('type', 2)->where('permalink', $in)->first();
    }

    public function getPostFromCategory($cats = "")
    {
        if (empty($cats))
            return null;

        if (is_array($cats))
            $posts = $this->postCatageroyAttachment->whereIn('cat_id', $cats)->get(['post_id']);
        else
            $posts = $this->postCatageroyAttachment->where('cat_id', $cats)->get(['post_id']);

        return $posts;
    }

    public function getPostByType($type, $limit = 0)
    {
        $posts = $this->post->where('type', $type)->orderBy('id', 'desc');
        return $limit ? $posts->limit($limit)->get() : $posts->get();
    }
}