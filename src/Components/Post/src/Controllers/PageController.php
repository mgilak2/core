<?php

namespace Post;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class PageController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var View
     */
    private $view;

    public function __construct(View $view,
                                Redirector $redirector,
                                AdminTheme $theme,
                                Request $request)
    {
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->request = $request;
        $this->view = $view;
    }

    public function getManage()
    {

        return $this->theme->baseDashboard("Post::PageManage")
            ->with('cats', Post::all());
    }

    public function getAdd()
    {
        return $this->theme->baseDashboard("Post::PageAdd");
    }

    public function getEdit(Post $page)
    {
        return $this->theme->baseDashboard("Post::PageEdit")
            ->with(compact('page'));
    }

    public function getDelete(Post $Page)
    {
        $Page->destroy($Page->id);

        return $this->redirector->route('page.list');
    }

    public function postAdd()
    {
        $Page = new Post;
        $Page->type = PostType::page;
        $Page->subject = $this->request->get('subject');
        $Page->permalink = $this->request->get('permalink');
        $Page->content = $this->request->get('content');
        $Page->Featured_image = '';
        $Page->save();

        return $this->redirector->route('page.list');
    }

    public function postEdit(Post $Page)
    {
        $Page->subject = $this->request->get('subject');
        $Page->permalink = $this->request->get('permalink');
        $Page->content = $this->request->get('content');
        $Page->save();

        return $this->redirector
            ->route('page.list', ['id' => $Page->id]);
    }

    public function getList()
    {
        $pages = Post::where('type', PostType::page)->get();

        return $this->theme->baseDashboard("Post::PageList")
            ->with('pages', $pages);
    }
}