<?php

namespace Post;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Validator;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class AdController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var View
     */
    private $view;

    public function __construct(View $view,
                                Redirector $redirector,
                                AdminTheme $theme,
                                Request $request)
    {
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->request = $request;
        $this->view = $view;
    }

    public function getCreate()
    {
        return $this->theme->baseDashboard('Post::ad.AdCreate');
    }

    public function postCreate()
    {
        $inputs = $this->request->only('subject', 'language', 'permalink', 'content');
        $rules = [
            'subject' => ['required', 'max:200'],
            'permalink' => ['max:200'],
            'content' => ['']
        ];

        $inputs['permalink'] = $this->slug($inputs['permalink']);

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            return $this->redirector->back()->withErrors($validator)->withInput($inputs);
        }

        $inputs['type'] = PostType::ad;
        Post::insert($inputs);

        return $this->redirector->route('ad.list');
    }

    private function slug($str)
    {
        return preg_replace('/[^A-Za-z0-9-]+/', '-', $str);
    }

    public function getList()
    {
        $ads = Post::where('type', PostType::ad)
            ->orderBy('id', 'desc')->get();
        return $this->theme->baseDashboard("Post::ad.AdList", compact('ads'));
    }

    public function getUpdate(Post $ad)
    {
        return $this->theme->baseDashboard('Post::ad.AdUpdate')
            ->with(compact('ad'));
    }

    public function putUpdate(Post $ad)
    {
        $inputs = $this->request->only('subject', 'permalink', 'content');
        $rules = [
            'subject' => ['required', 'max:200'],
            'permalink' => ['max:200'],
            'content' => ['']
        ];

        $inputs['permalink'] = $this->slug($inputs['permalink']);

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            return $this->redirector->back()->withErrors($validator)->withInput($inputs);
        }

        $ad->subject = $inputs['subject'];
        $ad->permalink = $inputs['permalink'];
        $ad->content = $inputs['content'];
        $ad->save();

        return $this->redirector->route('ad.list');
    }

    public function getDelete(Post $ad)
    {
        if ($ad)
            $ad->delete();

        return $this->redirector->route('ad.list');
    }
}