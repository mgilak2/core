<?php

namespace Post;

use Input;
use Phoneme\Http\Controllers\Controller;
use Redirect;
use Validator;

class TabController extends Controller
{

    public function getCreate()
    {
        $languages = get_languages();
        return admin('PostModule::tab.TabCreate')->with("languages", $languages);
    }

    public function postCreate()
    {
        $inputs = Input::only("language", 'subject', 'permalink', 'content');
        $rules = [
            'subject' => ['required', 'max:200'],
            'content' => ['']
        ];

        $inputs['permalink'] = $this->slug($inputs['permalink']);

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput($inputs);
        }

        $inputs['type'] = PostType::tab;
        Post::insert($inputs);

        return Redirect::route('tab.list');
    }

    public function getList()
    {
        $tabs = Post::where('type', PostType::tab)->orderBy('id', 'desc')->get();
        return admin("PostModule::tab.TabList")->with(compact('tabs'));
    }

    public function getUpdate(Post $tab)
    {
        $languages = get_languages();
        return admin('PostModule::tab.TabUpdate')
            ->with(compact('tab'))
            ->with("languages", $languages);
    }

    public function putUpdate(Post $tab)
    {
        $inputs = Input::only("language", 'subject', 'permalink', 'content');
        $rules = [
            'subject' => ['required', 'max:200'],
            'permalink' => ['max:200'],
            'content' => ['']
        ];

        $inputs['permalink'] = $this->slug($inputs['permalink']);

        $validator = Validator::make($inputs, $rules);
        if ($validator->fails()) {
            return \Redirect::back()->withErrors($validator)->withInput($inputs);
        }

        $tab->subject = $inputs['subject'];
        $tab->permalink = $inputs['permalink'];
        $tab->content = $inputs['content'];
        $tab->content = $inputs['language'];
        $tab->save();

        return Redirect::route('tab.list');
    }

    public function getDelete(Post $tab)
    {
        if ($tab) {
            $tab->delete();
        }

        return Redirect::route('tab.list');
    }

    private function slug($str)
    {
        return preg_replace('/[^A-Za-z0-9-]+/', '-', $str);
    }
}