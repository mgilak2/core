<?php

namespace Post;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class CatController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var View
     */
    private $view;

    public function __construct(View $view,
                                Redirector $redirector,
                                AdminTheme $theme,
                                Request $request)
    {
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->request = $request;
        $this->view = $view;
    }
    public function getManage()
    {
        return $this->theme->baseDashboard("Post::CatManage")
            ->with('cats', Category::all());
    }

    public function getAdd()
    {
        $cats = Category::all();
        $theme = "Admin";
        $ddl_cats = array();
        $ddl_cats[0] = 'انتخاب کنید';

        $route = ['route' => 'cat.add'];

        foreach ($cats as $cat) {
            $ddl_cats[$cat->id] = $cat->title;
        }

        return $this->theme->baseDashboard("Post::CatAdd", compact("ddl_cats", "cats", "theme", 'route'));
    }

    public function getEdit(Category $cat)
    {
        $cats = Category::all();
        $theme = "Admin";

        $ddl_cats = array();
        $ddl_cats[0] = 'انتخاب کنید';

        foreach ($cats as $innercat) {
            $ddl_cats[$innercat->id] = $innercat->title;
        }
        return $this->theme->baseDashboard("Post::CatEdit", compact("ddl_cats", "cats", "theme", 'cat'));
    }

    public function getDelete(Category $cat)
    {
        $cat->destroy($cat->id);

        return $this->redirector->route('cat.add');
    }

    public function postAdd()
    {
        $inputs = $this->request->all();
        Category::create($inputs);

        $cats = Category::all();
        $theme = "Admin";
        $ddl_cats = array();
        $ddl_cats[0] = 'انتخاب کنید';

        foreach ($cats as $cat) {
            $ddl_cats[$cat->id] = $cat->title;
        }

        return $this->theme->baseDashboard("Post::CatAdd", compact("ddl_cats", "cats", "theme"));
    }

    public function postEdit(Category $cat)
    {
        $cat->title = $this->request->get('title');
        $cat->parent_id = $this->request->get('parent_id');
        $cat->language = $this->request->get('language');

        $cat->save();

        return $this->redirector->route('cat.getEdit', ['id' => $cat->id]);
    }
}