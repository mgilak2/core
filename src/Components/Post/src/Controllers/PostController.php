<?php

namespace Post;

use AdminTheme\AdminTheme;
use Illuminate\Database\DatabaseManager as DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class PostController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var View
     */
    private $view;
    /**
     * @var DB
     */
    private $db;

    public function __construct(View $view,
                                Redirector $redirector,
                                DB $db,
                                AdminTheme $theme,
                                Request $request)
    {
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->request = $request;
        $this->view = $view;
        $this->db = $db;
    }

    public function getManage()
    {
        return $this->view->make("Post::PostManage")
            ->with('cats', Post::all());
    }

    public function getAdd()
    {
        $categories = $this->get_categories();

        return $this->view->make("Post::PostAdd", compact("categories"));
    }

    private function get_categories()
    {
        $cats = Category::all();
        $cats_array[0] = "...";
        foreach ($cats as $cat) {
            $cats_array[$cat->id] = $cat->title;
        }

        return $cats_array;
    }

    public function getEdit(Post $post)
    {
        $cats = Post_Cat_Attached::where('post_id', $post->id)->get();
        foreach ($cats as $cat) {
            $post->post_cats .= ',' . $cat->cat_id;
        }
        $post->post_cats = trim($post->post_cats, ",");
        $categories = $this->get_categories();

        return $this->theme->baseDashboard("Post::PostEdit", compact('post', 'categories'));
    }

    public function getDelete(Post $post)
    {
        $post->destroy($post->id);
        $post->cats()->detach();

        return $this->redirector->route('post.list');
    }

    public function postAdd()
    {
        $post = new Post;
        $post->type = PostType::post;
        $post->subject = $this->request->get('subject');
        $post->permalink = $this->request->get('permalink');
        $post->content = $this->request->get('content');
        $post->featured_image = $this->request->get('featured_image');
        $post->gallery_id = $this->request->get('gallery_id');
        $post->save();

        $cats = explode(',', $this->request->get("post_cats"));

        if ($this->request->get("post_cats") != '0')
            $post->cats()->sync($cats);

        return $this->redirector->route('post.list');
    }

    public function postEdit(Post $post)
    {
        $post->subject = $this->request->get('subject');
        $post->permalink = $this->request->get('permalink');
        $post->content = $this->request->get('content');
        $post->featured_image = $this->request->get('featured_image');
        $post->gallery_id = $this->request->get('gallery_id');
        $post->save();

        $cats = explode(',', $this->request->get("post_cats"));

        $post->cats()->sync($cats);

        return $this->redirector->route('post.edit', array('id' => $post->id));
    }

    public function getList()
    {
        $posts = Post::where('type', PostType::post)->get();
        return $this->theme->baseDashboard("Post::PostList", compact('posts'));
    }

    public function getShowPost($permalink = "")
    {
        if (!$permalink)
            exit('Not found.');
        $post = $this->db
            ->table('cms.cms_posts', ['permalink', $permalink])->first();
        if (!$post) {
            exit('404 not found');
        }

        return $this->theme
            ->baseDashboard("Front::view.single", ['post' => $post]);
    }
}