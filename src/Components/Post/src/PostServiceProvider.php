<?php
namespace Post;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class PostServiceProvider extends ServiceProvider implements PhonemeComponent, Initializable, CanMigrate
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Application $app, View $view, Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
    }

    public function register()
    {
        // nothing to do
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {
        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Post', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->pages();
        $this->post();
        $this->advertisement();
        $this->categories();
    }

    private function pages()
    {
        $this->router->group(['prefix' => "dashboard/page", 'namespace' => "Post"], function () {
            $this->router->model("page_id", Post::class);

            $this->router->get('/create', ['as' => 'page.add.get', 'uses' => 'PageController@getAdd']);
            $this->router->get('/edit/{page_id}', ['as' => 'page.edit', 'uses' => 'PageController@getEdit']);
            $this->router->get('/manage', ['as' => 'page.manage', 'uses' => 'PageController@getManage']);
            $this->router->get('/delete/{page_id}', ['as' => 'page.delete', 'uses' => 'PageController@getDelete']);

            $this->router->post('/add', ['as' => 'page.add', 'uses' => 'PageController@postAdd']);
            $this->router->post('/edit/{page_id}', ['as' => 'page.edit', 'uses' => 'PageController@postEdit']);

            $this->router->get('/list', ['as' => 'page.list', 'uses' => 'PageController@getList']);
        });
    }

    private function post()
    {
        $this->router->group(['prefix' => "dashboard/post", 'namespace' => "Post"], function () {
            $this->router->model("post_id", Post::class);

            $this->router->get('/create', ['as' => 'post.add.get', 'uses' => 'PostController@getAdd']);
            $this->router->get('/edit/{post_id}', ['as' => 'post.edit', 'uses' => 'PostController@getEdit']);
            $this->router->get('/manage', ['as' => 'post.manage', 'uses' => 'PostController@getManage']);
            $this->router->get('/delete/{post_id}', ['as' => 'post.delete', 'uses' => 'PostController@getDelete']);

            $this->router->post('/add', ['as' => 'post.add', 'uses' => 'PostController@postAdd']);
            $this->router->post('/edit/{post_id}', ['as' => 'post.edit', 'uses' => 'PostController@postEdit']);

            $this->router->get('/list', ['as' => 'post.list', 'uses' => 'PostController@getList']);
        });
    }

    private function advertisement()
    {
        $this->router->group(['prefix' => "dashboard/ad", 'namespace' => "Post"], function () {
            $this->router->model("ad_id", Post::class);

            $this->router->get('/create', ['as' => 'ad.create', 'uses' => 'AdController@getCreate']);
            $this->router->post('/create', ['as' => 'ad.create.post', 'uses' => 'AdController@postCreate']);
            $this->router->get('/', ['as' => 'ad.list', 'uses' => 'AdController@getList']);
            $this->router->get('/edit/{ad_id}', ['as' => 'ad.update', 'uses' => 'AdController@getUpdate']);
            $this->router->put('/edit/{ad_id}', ['as' => 'ad.update.put', 'uses' => 'AdController@putUpdate']);
            $this->router->get('/delete/{ad_id}', ['as' => 'ad.delete', 'uses' => 'AdController@getDelete']);
        });
    }

    private function categories()
    {
        $this->router->model("cat_id", Category::class);

        $this->router->group(['prefix' => "dashboard/post/category", 'namespace' => "Post"], function () {
            $this->router->model("cat_id", Category::class);

            $this->router->get('/create', ['as' => 'cat.add.get', 'uses' => 'CatController@getAdd']);
            $this->router->get('/edit/{cat_id}', ['as' => 'cat.getEdit', 'uses' => 'CatController@getEdit']);
            $this->router->get('/manage', ['as' => 'cat.manage', 'uses' => 'CatController@getManage']);
            $this->router->get('/delete/{cat_id}', ['as' => 'cat.delete', 'uses' => 'CatController@getDelete']);

            $this->router->post('/add', ['as' => 'cat.add', 'uses' => 'CatController@postAdd']);
            $this->router->post('/edit/{cat_id}', ['as' => 'cat.edit', 'uses' => 'CatController@postEdit']);
        });
    }

    public function tabs()
    {
        $this->router->group(['prefix' => "dashboard/tab", 'namespace' => "Post"], function () {
            $this->router->model("tab_id", Post::class);

            $this->router->get('/create', ['as' => 'tab.create', 'uses' => 'TabController@getCreate']);
            $this->router->post('/create', ['as' => 'tab.create.post', 'uses' => 'TabController@postCreate']);
            $this->router->get('/', ['as' => 'tab.list', 'uses' => 'TabController@getList']);
            $this->router->get('/edit/{tab_id}', ['as' => 'tab.update', 'uses' => 'TabController@getUpdate']);
            $this->router->put('/edit/{tab_id}', ['as' => 'tab.update.put', 'uses' => 'TabController@putUpdate']);
            $this->router->get('/delete/{tab_id}', ['as' => 'tab.delete', 'uses' => 'TabController@getDelete']);
        });
    }
}