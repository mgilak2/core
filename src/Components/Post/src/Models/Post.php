<?php namespace Post;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Post extends Eloquent
{
    protected $fillable = ['subject', 'type',
        'permalink', 'content', 'gallery_id', 'featured_image'];

    public function url()
    {
        if (strlen($this->permalink) > 0) {
            return route('article', $this->permalink);
        }

        return route('article', $this->id);
    }

    public function cats()
    {
        return $this->belongsToMany(Category::class, 'post_category',
            'post_id', 'category_id');
    }
}