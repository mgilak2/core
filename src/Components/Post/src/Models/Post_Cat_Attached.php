<?php

namespace Post;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Post_Cat_Attached extends Eloquent
{
    public $timestamps = false;
    protected $table = "post_category";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'post_id', 'cat_id'];
}