<?php

namespace Post;

class PostType
{
    const post = 1;
    const page = 2;
    const ad = 3;
    const tab = 4;
}
