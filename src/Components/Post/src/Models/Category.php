<?php

namespace Post;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Category extends Eloquent
{
    public $timestamps = false;
    protected $table = "categories";
    protected $fillable = ['title', 'parent_id'];


    public function posts()
    {
        return $this->belongsToMany(Post::class, 'post_category',
            'category_id', 'post_id');
    }
}