//
//
//// this application is tight to its html view
//// be careful about changing its html layouts
//
//var widgets = {
//    // helper functions
//    helper:{
//        random: function () {
//            var str = "qwertyuiopQWERTYUIOPasdfghjklASDFGHJKLzxcvbnmZXCVBNM";
//            var random = "";
//            for(var i = 0;i<7;i++){
//                random += str[(Math.floor(Math.random()* 40))];
//            }
//            return random;
//        }
//    },
//
//    //////
//    // sidebar app
//    /////
//    sidebar:{
//        // register sidebars for drag and drop ability
//        add:{
//            onDragStart: function (item, container, _super) {
//                // Duplicate items of the no drop area
//                if(!container.options.drop)
//                    item.clone().insertAfter(item);
//                _super(item)
//            },
//            onDrop: function ($item, container, _super, event) {
//                if(! $($item).attr("data-idString"))
//                {
//                    $($item).attr("data-idString",this.helper.random());
//                }
//
//                // adding hidden input with the value of data-parentId which tells the widgets which one is its sidebar
//                var widgets = [];
//                var sideBarNode = container.el;
//                this.order(sideBarNode,$(this));
//                // appending orders into widgets
//                sideBarNode.children('.order').remove();
//
//                _super($item);
//
//                this.operation.buttons();
//
//                this.operation.saveWidget($item);
//            }
//        },
//        sidebarSortable:function(selector){
//            $(selector).sortable({
//                group: 'no-drop',
//                handle: 'i.icon-move',
//                onDragStart: function (item, container, _super) {
//                    this.onDragStart(item, container, _super);
//                },
//                onDrop:function ($item, container, _super, event) {
//                    this.onDrop($item, container, _super, event);
//                }
//            });
//        },
//        order: function (sidebarNode,_this) {
//            $(sidebarNode).children("li").each(function(index){
//                // we add widget dir to help our php code to find the widgets
//                var WidgetDir      = _this.attr("data-widgetdir");
//                var WidgetId       = _this.attr("data-id");
//                var WidgetIdString = _this.attr("data-idString");
//                var SidebarId      = $(sidebarNode).attr("data-parentId");
//                widgets.push({
//                    dir      : WidgetDir,
//                    widget   : WidgetId,
//                    index    : index,
//                    idString : WidgetIdString,
//                    parentId : SidebarId
//                });
//            });
//
//            // appending orders into widgets
//            sidebarNode.children('.order').remove();
//
//            sidebarNode.append(this.orderTemplate(widgets));
//        },
//        orderTemplate: function (widgets) {
//            // i just keep operation out of template
//            var JsonedWidgets = JSON.stringify(widgets);
//            return "<input class='order' type='hidden' name='widget[order]' value='"+JsonedWidgets+"'>";
//        },
//        reOrder: function (sidebarNode,_this) {
//            this.order(sidebarNode,_this);
//        },
//        appendOrdersToTargetWidget: function (widget,widgets) {
//            var form = widget.parent().parent().parent().children("form");
//            form.children('.order').remove();
//            form.append(this.orderTemplate(widgets));
//        }
//    },
//
//
//    //////
//    // operation
//    /////
//    operation:{
//        buttons: function () {
//            // every time you drop an item into sidebar all saveme button gets noticed fo ajax request
//            $(".saveMe").unbind("click").click(function(e){
//                e.preventDefault();
//
//                $.ajax({
//                    url:"widget/save-widget",
//                    type:"POST",
//                    data:data
//                });
//            });
//
//            $(".deleteMe").unbind("click").click(function(e){
//                e.preventDefault();
//                $.ajax({
//                    url:"widget/delete-widget",
//                    type:"POST",
//                    data:{"idString":$(this).parent().parent().parent().attr('data-idstring')}
//                });
//            });
//        },
//        saveWidget: function (widget) {
//            var saveMe = $(widget).children("form").children("div").last().children('.saveMe');
//            saveMe.trigger('click');
//        }
//    },
//
//    //////
//    // controller - behavior layer
//    /////
//    controller:{
//        init:function(){
//            this.operation.buttons();
//
//            addSidebar.add("ol.simple_with_drop");
//
//            $("ol.simple_with_no_drop").sortable({
//                group: 'no-drop',
//                drop: false
//            });
//        }
//    }
//};
