app.module.widget = {
    // attaching helpers to widget
    helpers: {
        random: function () {
            var str = "qwertyuiopQWERTYUIOPasdfghjklASDFGHJKLzxcvbnmZXCVBNM";
            var random = "";
            for (var i = 0; i < 7; i++) {
                random += str[(Math.floor(Math.random() * 40))];
            }
            return random;
        }
    },

    // returns an array of widgets order
    order: function (sidebar_node, all_widgets) {
        var widgets = [];
        var that = this;
        all_widgets.each(function (index) {
            // we add widget dir to help our php code to find the widgets
            var data = that.getData($(this), $(sidebar_node));

            widgets.push({
                widget_dir: data.widget_dir,
                widget_id: data.widget_id,
                index: index,
                id_string: data.id_string,
                sidebar_id: data.sidebar_id
            });
        });

        return widgets;
    },

    getData: function (widget, sidebar) {
        return app.onDemand.get_data_attributes_from_widget(widget, sidebar);
    },

    setDataIdString: function (widget) {
        app.plant.add_id_string_to_widget(widget);
    }
};