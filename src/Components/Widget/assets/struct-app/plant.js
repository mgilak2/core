app.plant.add_id_string_to_widget = function (widget) {
    if (widget.attr('id_string') === undefined) {
        widget.attr('id_string', app.module.widget.helpers.random());
    }
};

app.plant.language = function (language) {
    $("body").attr("data-language", language);
};