$(document).ready(function () {
    var sortable = app.module.register_widgetable_sidebars(
        ".simple_with_drop li div input.saveMe",
        ".simple_with_drop li div input.deleteMe"
    );
    sortable.sidebarSortable("ol.simple_with_drop");

    app.onDemand.register_widgets_column("ol.simple_with_no_drop");

    app.onDemand.register_save_button(".simple_with_drop li div input.saveMe");
    app.onDemand.register_delete_button(".simple_with_drop li div input.deleteMe");

    app.onDemand.register_language();
});

