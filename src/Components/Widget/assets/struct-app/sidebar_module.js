app.module.register_widgetable_sidebars = function (saveButton, deleteButton) {
    var sidebar = {};
    sidebar.widget_orders = null;
    sidebar.sidebar = null;
    sidebar.form = null;
    sidebar.widget = null;

    // register sidebars for drag and drop ability
    sidebar.onDragStart = function (item, container, _super) {
        // Duplicate items of the no drop area
        if (!container.options.drop)
            item.clone().insertAfter(item);
        _super(item)
    };

    sidebar.onDrop = function (widget, container, _super) {
        app.module.widget.setDataIdString($(widget));

        // adding hidden input with the value of data-parentId which tells the widgets which one is its sidebar
        var sidebar_node = container.el;

        var all_widgets = app.onDemand.returning_all_widgets_of_sidebar(sidebar_node);

        var widgets_order = app.module.widget.order(sidebar_node, all_widgets);

        _super(widget);

        var form = $(widget).children('.widget_form');
        var sidebar = $(container.el);
        sidebar.widget_orders = widgets_order;
        sidebar.sidebar = sidebar;
        sidebar.form = form;
        sidebar.widget = $(widget);

        app.onDemand.register_save_button(saveButton);
        app.onDemand.register_delete_button(deleteButton)
        app.onDemand.return_save_button($(widget)).trigger('click');
    };

    sidebar.sidebarSortable = function (selector) {
        $(selector).each(function () {
            $(this).sortable({
                group: 'no-drop',
                handle: 'i.icon-move',
                onDragStart: function (widget, container, _super) {
                    sidebar.onDragStart(widget, container, _super);
                },
                onDrop: function (widget, container, _super) {
                    sidebar.onDrop(widget, container, _super);
                }
            });
        })
    };

    return sidebar;
};
