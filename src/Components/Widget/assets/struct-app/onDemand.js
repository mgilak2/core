app.onDemand.returning_all_widgets_of_sidebar = function (sidebarNode) {
    return sidebarNode.children("li");
};

app.onDemand.save_widget_after_dropping_widget_off = function (widget) {
    return $(widget).children("form").children("div").last().children('.saveMe');
};

app.onDemand.register_widgets_column = function (selector) {
    $(selector).sortable({
        group: 'no-drop',
        drop: false
    });
};


app.onDemand.register_save_button = function (saveButton) {
    var saveMe = $(saveButton);
    saveMe.unbind('click').click(function (e) {
        var widget = $(this).parent().parent().parent();
        var form = widget.children('.widget_form');
        var sidebar = widget.parent();
        var all_widgets = sidebar.children('li');
        var widget_orders = app.module.widget.order(sidebar, all_widgets);
        e.preventDefault();
        var formSerialized = app.onDemand.furnish_data(form, widget, sidebar, widget_orders);
        $.ajax({
            url: "widget/save-widget",
            type: "POST",
            data: formSerialized
        });

        form.find(".data_attribute").remove();
    });
};

app.onDemand.register_delete_button = function (deleteButton) {

    var deleteMe = $(deleteButton);
    deleteMe.unbind('click').click(function (e) {
        var widget = $(this).parent().parent().parent();
        var form = widget.children('.widget_form');
        var sidebar = widget.parent();
        // after finding sidebar its not necessary to hold on to widget
        // in fact this widget must be deleted in order to
        // make further code correct in result
        widget.remove();
        var all_widgets = sidebar.children('li');
        var widget_orders = app.module.widget.order(sidebar, all_widgets);
        e.preventDefault();
        var formSerialized = app.onDemand.furnish_data(form, widget, sidebar, widget_orders);
        $.ajax({
            url: "widget/delete-widget",
            type: "POST",
            data: formSerialized
        });

        form.find(".data_attribute").remove();
    });
};

app.onDemand.get_data_attributes_from_widget = function (widget, sidebar) {
    var data = {};

    data.id_string = widget.attr('id_string');

    data.theme_dir = widget.attr('data-theme_dir');

    data.widget_dir = widget.attr('data-widget_dir');

    data.widget_id = widget.attr('data-widget_id');

    data.sidebar_id = sidebar.attr('data-sidebar_id');

    var language = app.onDemand.get_language();
    if (!language) {
        language = 0;
    }

    data.language = language;

    return data;
};

app.onDemand.furnish_data = function (form, widget, sidebar, widgets_orders) {
    var data = app.onDemand.get_data_attributes_from_widget(widget, sidebar);

    form.append("<input type='hidden' class='data_attribute' name='data[id_string]' value='" + data.id_string + "'>");
    form.append("<input type='hidden' class='data_attribute' name='data[sidebar_id]' value='" + data.sidebar_id + "'>");
    form.append("<input type='hidden' class='data_attribute' name='data[theme_dir]' value='" + data.theme_dir + "'>");
    form.append("<input type='hidden' class='data_attribute' name='data[widget_dir]' value='" + data.widget_dir + "'>");
    form.append("<input type='hidden' class='data_attribute' name='data[widget_id]' value='" + data.widget_id + "'>");
    form.append("<input type='hidden' class='data_attribute' name='data[widgets_order]' value='" + JSON.stringify(widgets_orders) + "'>");
    form.append("<input type='hidden' class='data_attribute' name='_token' value='" + $("._token").val() + "'>");


    form.serialize();

    return form.serialize();
};

app.onDemand.return_save_button = function (widget) {
    return widget.children('form.widget_form').children('div.widget_buttons').children('input.saveMe');
};

app.onDemand.register_language = function () {
    var languages = $(".languages");
    languages.on("change", function () {
        var language_id = $(this).find(":selected").val();
        app.plant.language(language_id);

    });
};

app.onDemand.get_language = function () {
    var language = $("body").attr("data-language");
    if (language === undefined) {
        return false;
    }

    return language;
};
