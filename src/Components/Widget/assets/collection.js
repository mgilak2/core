var Collection = {
    factory: function () {
        var collection = {};

        collection.data = [];
        collection.temp = [];

        // adding object to collection.data with _id string
        collection.add = function (object) {
            collection.addIdString(object);
            collection.data.push(object);
        };


////////////////////////////////////////////////////////
/// end index base
///////////

// getting with index
        collection.get = function (id) {
            if (collection.data[id] !== "undefined") {
                return collection.data[id];
            }

            return null;
        };

// reIndex the given index from collection.data
        collection.reIndex = function (index, reIndex) {
            var temp = [];

            for (var i = 0; i < collection.data.length; i++) {
                temp.push(null);
            }

            var pointer = 0;

            for (var current_index = 0; current_index < collection.data.length; current_index++) {
                if (reIndex == current_index) {
                    temp[reIndex] = collection.data[index];
                    --pointer;
                }
                else if (index == current_index) {

                    if (collection.data[current_index + 1] !== "undefined") {
                        if (reIndex > index) {
                            ++pointer;
                        }
                        temp[current_index] = collection.data[pointer];
                        if (reIndex < index) {
                            ++pointer;
                        }
                    }
                }
                else {
                    temp[current_index] = collection.data[pointer];
                }

                pointer++;
            }

            collection.data = temp;
            collection.temp = [];

            return this;
        };

        collection.update = function (index, object) {
            var item = collection.data[index];
            object._id = item._id;
            collection.data[index] = object;
        };

        collection.remove = function (index) {
            if (collection.data[index] !== "undefined") {
                collection.jump(index);
            }

            return this;
        };

///////////
/// end index base
////////////////////////////////////////////////////////


////////////////////////////////////////////////////////
/// name base
///////////

// find item with its name attribute
        collection.getByName = function (name, byIndex) {
            for (var index in collection.data) {
                if (collection.data[index].name == name) {
                    if (byIndex === true) {
                        return {index: index, item: collection.data[index]};
                    }

                    return collection.data[index];
                }
            }

            return null;
        };

// reIndex the given name from collection.data
        collection.reIndexByName = function (name, reIndex) {
            var index = collection.getByName(name, true).index;
            collection.reIndex(index, reIndex);
        };

        collection.updateByName = function (name, object) {
            var item = collection.getByName(name, true);
            var index = item.index;
            object._id = item._id;
            collection.data[index] = object;
        };

// remove by name
        collection.removeByName = function (name) {
            for (var i = 0; i < collection.data.length; i++) {
                if (collection.data[i].name !== name) {
                    collection.temp.push(collection.data[i]);
                }
            }

            collection.data = collection.temp;
            collection.temp = [];
        };
///////////
/// end name base
////////////////////////////////////////////////////////


////////////////////////////////////////////////////////
/// object base
///////////

// find object , can be utilize for moment that you did`nt specify name or object does`nt have _id attribute yet
        collection.getByObject = function (object, byIndex) {
            for (var index in collection.data) {
                if (!object.hasOwnProperty("_id") || object._id === null) {
                    object._id = collection.data[index]._id;
                    if (JSON.stringify(object) === JSON.stringify(collection.data[index])) {
                        object._id = null;
                        if (byIndex === true) {
                            return {index: index, item: collection.data[index]};
                        }

                        return collection.data[index];
                    }
                    else {
                        object._id = null;
                    }
                }
                else {
                    if (object === collection.data[index]) {
                        if (byIndex === true) {
                            return {index: index, item: collection.data[index]};
                        }
                        return collection.data[index];
                    }
                }
            }

            return null;
        };

// reIndex the given object from collection.data
        collection.reIndexByObject = function (object, reIndex) {
            var index = collection.getByObject(object, true).index;
            collection.reIndex(index, reIndex);
        };

        collection.updateByObject = function (object, object_update) {
            object_update._id = object._id;
            var index = collection.getByObject(object, true).index;
            collection.data[index] = object_update;
        };

        collection.removeByObject = function (object) {
            var index = collection.getByObject(object);
            collection.remove(index);
        };

///////////
/// end object base
////////////////////////////////////////////////////////


////////////////////////////////////////////////////////
/// idString (_id) base
///////////
        collection.getByIdString = function (idString, byIndex) {
            for (var index in collection.data) {
                if (collection.data[index]._id == idString) {
                    if (byIndex === true) {
                        return {index: index, item: collection.data[index]};
                    }

                    return collection.data[index];
                }
            }

            return null;
        };

// reIndex the given index idString collection.data
        collection.reIndexByIdString = function (idString, reIndex) {
            var index = collection.getByIdString(idString, true).index;
            collection.reIndex(index, reIndex);
        };

        collection.updateByIdString = function (idString, object) {
            object._id = idString;
            var index = collection.getByIdString(idString, true).index;
            collection.data[index] = object;
        };

        collection.removeByIdString = function (idString) {
            collection.jumpByIdString(idString);
        };

///////////
/// end idString base
////////////////////////////////////////////////////////


// set _id to the given object with random string
// its good for referring
        collection.addIdString = function (object) {
            object._id = collection.randomString();
        };

// fill collection.temp with anything except item with the given index
        collection.jump = function (index) {
            for (var i = 0; i < collection.data.length; i++) {
                if (index != i) {
                    collection.temp.push(collection.data[i]);
                }
            }

            collection.data = collection.temp;
            collection.temp = [];
        };

// fill collection.temp with anything except item with the given idString
        collection.jumpByIdString = function (idString) {
            for (var i = 0; i < collection.data.length; i++) {
                if (collection.data[i]._id !== idString) {
                    collection.temp.push(collection.data[i]);
                }
            }

            collection.data = collection.temp;
            collection.temp = [];
        };

// generate random string
        collection.randomString = function () {
            var str = "qwertyuiopQWERTYUIOPasdfghjklASDFGHJKLzxcvbnmZXCVBNM";
            var random = "";
            for (var i = 0; i < 5; i++) {
                random += str[(Math.floor(Math.random() * 40))];
            }

            return random;
        };

        collection.getAll = function () {
            return collection.data;
        };

        collection.size = function () {
            return collection.data.length;
        };

        return collection;
    }
}