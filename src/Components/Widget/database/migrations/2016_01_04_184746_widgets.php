<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Widgets extends Migration
{
    public function up()
    {
        Schema::create('widgets', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('class');
            $table->string('options');
            $table->string('id_string');
            $table->string('sidebar');
            $table->string('theme');
            $table->string('dir');
            $table->timestamps();
        });

        Schema::create('sidebar_widget', function (Blueprint $table) {
            $table->increments('id');
            $table->string('sidebar');
            $table->string('widget');
            $table->integer('index');
            $table->string('id_string');
            $table->string('theme');
            $table->string('dir');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::drop("widgets");
        Schema::drop("sidebar_widget");
    }
}
