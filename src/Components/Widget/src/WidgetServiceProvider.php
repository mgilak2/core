<?php

namespace Widget;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class WidgetServiceProvider extends ServiceProvider
    implements PhonemeComponent, Initializable, CanMigrate
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Application $app, View $view,
                                Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
    }

    public function register()
    {
        // nothing to do :)
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {
        $this->publishes([
            __DIR__.'/path/to/assets' => public_path('vendor/courier'),
        ], 'public');

        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Widget', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->router->group(['prefix' => "dashboard/widget", 'namespace' => "Widget"], function () {
            $this->router->model("widget_id", Widget::class);

            $this->router->get("/", ["as" => 'widget.index', 'uses' => 'WidgetController@index']);

            $this->router->post("/save-widget", ['as' => 'widget.save', 'uses' => 'WidgetController@save']);
            $this->router->post("/delete-widget", ['as' => 'widget.delete', 'uses' => 'WidgetController@delete']);
        });
    }
}