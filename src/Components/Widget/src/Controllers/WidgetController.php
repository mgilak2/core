<?php namespace Widget;

use AdminTheme\AdminTheme;
use Illuminate\Database\DatabaseManager as DB;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Phoneme\Http\Controllers\Controller;
use Sidebars\SidebarHookResponder;
use Striped\WidgetService;

class WidgetController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var DB
     */
    private $db;
    /**
     * @var SidebarHookResponder
     */
    private $sidebar;
    /**
     * @var WidgetService
     */
    private $widgets;

    public function __construct(Redirector $redirector,
                                AdminTheme $theme, DB $db,
                                Request $request,
                                SidebarHookResponder $sidebar,
                                WidgetService $widgets)
    {
        $this->redirector = $redirector;
        $this->theme = $theme;
        $this->request = $request;
        $this->db = $db;
        $this->sidebar = $sidebar;
        $this->widgets = $widgets;
    }

    public function index()
    {
        $themeWidgets = $this->widgets->getWidgets();
        $sidebars = $this->sidebar->getHook()->getAreas();
        return $this->theme->baseDashboard("Widget::index", compact('sidebars', 'themeWidgets'));
    }

    public function save()
    {
        $data = $this->request->get('data');
        $options = $this->request->get('options');

        $orders = json_decode($data['widgets_order']);
        $sidebar = $data['sidebar_id'];
        $theme_dir = $data['theme_dir'];
        $id_string = $data['id_string'];

        $this->db->table("sidebar_widget")
            ->where("sidebar", $sidebar)
            ->delete();

        foreach ($orders as $order) {
            $this->db->table("sidebar_widget")->insert([
                'sidebar' => $sidebar,
                'widget' => $order->widget_id,
                'id_string' => $order->id_string,
                'index' => $order->index,
                'dir' => $order->widget_dir,
                'theme' => $theme_dir,
            ]);

            if ($id_string == $order->id_string) {
                $this->db->table("widgets")->insert([
                    'sidebar' => $sidebar,
                    'name' => $order->widget_id,
                    'id_string' => $order->id_string,
                    'options' => base64_encode(serialize($options)),
                    'dir' => $order->widget_dir,
                    'theme' => $theme_dir,
                ]);
            }
        }
    }

    public function delete()
    {
        $data = $this->request->get('data');
        $id_string = $data['id_string'];

        $this->db->table("widgets")
            ->where("id_string", $id_string)
            ->delete();

        $this->db->table("sidebar_widget")
            ->where("id_string", $id_string)
            ->delete();
    }
}