<?php

namespace Widget;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Widget extends Eloquent
{
    public $timestamps = false;
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'body', 'weight', 'page_id'];
}