<h3>Widgets</h3>
<ol class="simple_with_no_drop vertical">
    @foreach($themeWidgets as $widget)
        <li data-widget_dir=""
            data-theme_dir=""
            data-widget_id="{{ $widget->id }}"
            class="highlight">
            <i class="icon-move"></i>
            {{ $widget->name }}
            <form class="widget_form" method="post" action="{{ route('widget.save') }}">
                <?php $widget->form(); ?>
                <div class="widget_buttons" style="width: 100%">
                    <input class="saveMe" type="submit" value="ذخیره"/>
                    <input class="deleteMe" type="submit" value="حذف"/>
                </div>
            </form>
        </li>
    @endforeach
</ol>
