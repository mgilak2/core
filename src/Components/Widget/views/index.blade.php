@extends("Admin::dashboard")

@section("pageTitle")
    ابزارک ها
@stop

@section("content")
    <link rel="stylesheet" href="{{asset("Widget/assets/style.css")}}">

    <div class="row">
        <div class="col-md-6">
            @include("Widget::sidebarWidgets")
        </div>
        <div class="col-md-6">
            @include("Widget::themeWidgets")
        </div>
    </div>
@stop

@section("master.footer")
    @include("Admin::footer")
    <script type="text/javascript" src="{{asset("Widget/assets/sortable.js")}}"></script>
    <script type="text/javascript" src="{{asset("Widget/assets/struct.js")}}"></script>
    <script type="text/javascript" src="{{asset("Widget/assets/app.js")}}"></script>

    <script type="text/javascript" src="{{asset("Widget/assets/struct-app/app.js")}}"></script>
    <script type="text/javascript" src="{{asset("Widget/assets/struct-app/onDemand.js")}}"></script>
    <script type="text/javascript" src="{{asset("Widget/assets/struct-app/plant.js")}}"></script>
    <script type="text/javascript"
            src="{{asset("Widget/assets/struct-app/widget_module.js")}}"></script>
    <script type="text/javascript"
            src="{{asset("Widget/assets/struct-app/sidebar_module.js")}}"></script>
    <script type="text/javascript" src="{{asset("Widget/assets/struct-app/init.js")}}"></script>

    <script>
        <?php
            $language = 'fa';
         ?>
        $(document).ready(function () {
            $("body").attr("data-language", 'fa');
        });
    </script>
@stop
