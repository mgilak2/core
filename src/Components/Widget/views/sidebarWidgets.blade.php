<h3>sidebars</h3>

@foreach($sidebars as $sidebar)
    {{ $sidebar['name'] }}
    <ol data-sidebar_id="{{ $sidebar['id'] }}" class="simple_with_drop vertical">
        <input type="hidden" name="_token" value="{{ csrf_token()  }}" class="_token">
        @foreach(adminSidebarWidgets($sidebar['id']) as $widget)
            <?php $widget_object = widget($widget->id_string, $sidebar['id']);?>
            <li id_string="{{ $widget_object->id_string }}" data-widget_dir="{{$widget_object->dir}}"
                data-theme_dir="{{ $widget_object->theme }}" data-widget_id="{{$widget_object->id}}" class="highlight">
                <i class="icon-move"></i>
                {{$widget_object->name}}
                <form class="widget_form" method="post" action="{{route('widget.save')}}">
                    {{ csrf_field() }}
                    <div style="width: 100%">
                        <?php $widget_object->form($widget_object->id_string); ?>
                    </div>
                    <div class="widget_buttons" style="width: 100%">
                        <input class="saveMe" type="submit" value="ذخیره"/>
                        <input class="deleteMe" type="submit" value="حذف"/>
                    </div>
                </form>
            </li>
        @endforeach
    </ol>
    <hr/>
@endforeach
