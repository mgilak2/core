@extends("Admin::dashboard")

@section("master.ticket.form.add")

    {!! Form::model($file, ['route' => ['file.rename', $file->id]]) !!}
    <div class="row">
        <div class="col-sm-4">
            <label class="control-label">name :</label>
            {!! Form::text('name', null, ['class' => 'form-control']) !!}
        </div>
    </div>
    {!! Form::submit('submit', ['class' => 'btn btn-info']) !!}
    {!! Form::close() !!}

@stop