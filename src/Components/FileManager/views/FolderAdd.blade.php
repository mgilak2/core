@extends("Admin::dashboard")

@section("master.ticket.form.add")

    @if(isset($folder))
        {!! Form::model($folder, ['route' => ['folder.edit', $folder->id]]) !!}
    @else
        {!! Form::open(['route' => 'folder.add']) !!}
    @endif
    <div class="row">
        <div class="col-sm-6">
            <label class="control-label">اسم پوشه :</label>
            {!! Form::text('name', Input::get("name"), ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-6">
            <label class="control-label">مادر :</label>
            {!! Form::text('parent_id', Input::get("parent_id"), ['class' => 'form-control']) !!}
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت مطلب</button>
        </div>
        <div class="col-sm-2">
            <button class="btn btn-warning btn-block" role="button" type="reset">انصراف</button>
        </div>
    </div>
    {!! Form::close() !!}
@stop