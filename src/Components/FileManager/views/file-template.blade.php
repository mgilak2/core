@foreach ($files as $file)
    <div data-id="{{ $file['id']  }}" data-fullname="{{ $file['fullname'] }}" data-size="{{ $file['size'] }}"
         class="file-box col-sm-2">
        <a href="{{ $file['naturalSize'] }}" class="thumbnail image-viewer fancybox-thumbs" data-fancybox-group="thumb">
            <img src="{{ $file['url']  }}" alt="{{ $file['fullname'] }}"/>
        </a>
        <span class="file-name">{{ $file['name']  }}<i class="icon-image2"></i></span>
    </div>
@endforeach