@extends("Admin::dashboard")

@section("master.ticket.form.add")
    {!! Form::model($file, ['route' => ['file.crop', $file->id]]) !!}
    <div class="row">
        <div class="col-sm-2">
            <label class="control-label">width :</label>
            {!! Form::text('width', $file->width, ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-2">
            <label class="control-label">height :</label>
            {!! Form::text('height', $file->height, ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-2">
            <label class="control-label">x :</label>
            {!! Form::text('x', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-2">
            <label class="control-label">y :</label>
            {!! Form::text('y', null, ['class' => 'form-control']) !!}
        </div>
        <div class="col-sm-4">
            <label class="control-label">virtual_path :</label>
            <img style="max-width: 200px;" src="{{ $file->virtual_path }}">
        </div>
    </div>
    {!! Form::submit('submit', ['class' => 'btn btn-info']) !!}
    {!! Form::close() !!}
@stop