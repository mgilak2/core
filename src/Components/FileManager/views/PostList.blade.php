@extends("Admin::dashboard")

@section("master.ticket.form.add")
    <section id="rootwizard" class="tabbable transparent tile wizard-forms">

        <!-- tile header -->
        <div class="tile-header transparent">
            <h1>لسیت مطلب ها</h1>

            <div class="controls">
                <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
                <a href="#" class="remove"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <!-- /tile header -->

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>id</th>
                <th>عنوان</th>
                <th>تصویر</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>n1</td>
                <td>h1</td>
                <td>a1</td>
            </tr>
            @foreach ($posts as $index => $post)
                <td>
                    {{ ++$index }}
                </td>
                <td>{{ $post->id }}</td>
                <td>{{ $post->subject }}</td>
                <td>{{ $post->featured_image }}</td>

                <td>
                    {!! HTML::linkRoute('cat.getEdit', 'ویرایش', array($cat->id)) !!}
                </td>
                <td>
                    {!! HTML::linkRoute('cat.delete', 'حذف', array($cat->id)) !!}</td>
                </tr>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@stop