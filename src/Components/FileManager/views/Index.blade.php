@extends("Admin::filemanager")

@section("filemanager.path")
    <input id="currentPath" data-file-id="" data-folder-id="" data-parent-id="1"
           data-fullname=""
           data-action=""
           data-file-rename="{{ route('file.rename') }}" data-folder-add="{{ route('folder.add') }}"
           data-folder-rename="{{ route('folder.rename') }}"
           data-file-delete="{{ route('file.delete') }}" data-folder-delete="{{ route('folder.delete') }}"
           data-file-index="{{ route('file.all') }}" data-folder-index="{{ route('folder.all') }}"
           type="hidden"/>
    <ul id="filemanagerRoute" class="breadcrumb breadcrumb-sm">
        <li data-id="1" data-name="root">
            <a href="#">
                <i class="icon-home2"></i>
            </a>
            <span class="divider"></span>
        </li>
    </ul>
@stop