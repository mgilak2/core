@extends("Admin::dashboard")

@section("master.ticket.form.add")

    {!! Form::open(['route'=> 'file.add', 'files' => true, 'class' => 'ds']) !!}
    <div class="row">
        <div class="col-sm-6">
            <label class="control-label">عنوان فایل :</label>
            {!! Form::file('files[]', ['class' => 'form-control', 'multiple']) !!}
        </div>
    </div>
    {!! Form::submit('Upload File') !!}
    {!! Form::close() !!}

@stop