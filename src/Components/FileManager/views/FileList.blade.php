@extends("Admin::dashboard")

@section("master.ticket.form.add")

    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>name</th>
            <th>ext</th>
            <th>size</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td>1</td>
            <td>n1</td>
            <td>h1</td>
            <td>a1</td>
        </tr>
        {{$i=0;}}
        @foreach ($files as $file)
            <td>{{++$i;}}</td>
            <td>{{ $file->id }}</td>
            <td>{{ $file->name }}</td>
            <td>{{ $file->size }}</td>
            <td>
                {!! HTML::linkRoute('file.edit', 'ویرایش', array($file->id)) !!}
            </td>
            <td>
                {!! HTML::linkRoute('file.delete', 'حذف', array($file->id)) !!}</td>
            </tr>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop