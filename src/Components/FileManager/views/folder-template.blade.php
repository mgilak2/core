@foreach ($folders as $folder)
    <div data-id="{{ $folder['id'] }}" data-fullname="{{ $folder['fullname'] }}" class="folder-box col-sm-2">
        <div class="type-ico icon-folder2"></div>
        <span class="folder-name">{{ $folder['name'] }}</span>
    </div>
@endforeach