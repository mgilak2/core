@extends("Admin::dashboard")

@section("master.ticket.form.add")
    <section id="rootwizard" class="tabbable transparent tile wizard-forms">

        <!-- tile header -->
        <div class="tile-header transparent">
            <h1>لسیت مطلب ها</h1>

            <div class="controls">
                <a href="#" class="refresh"><i class="fa fa-refresh"></i></a>
                <a href="#" class="remove"><i class="fa fa-times"></i></a>
            </div>
        </div>
        <!-- /tile header -->

        <table class="table">
            <thead>
            <tr>
                <th>#</th>
                <th>name</th>
                <th>ext</th>
                <th>size</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                <td>1</td>
                <td>n1</td>
                <td>h1</td>
                <td>a1</td>
            </tr>
            @foreach ($folders as $index => $folder)
                <td>{{ ++$index }}</td>
                <td>{{ $folder->id }}</td>
                <td>{{ $folder->name }}</td>
                <td>
                    {!! HTML::linkRoute('folder.edit', 'ویرایش', array($folder->id)) !!}
                </td>
                <td>
                    {!! HTML::linkRoute('folder.delete', 'حذف', array($folder->id)) !!}
                </td>
                </tr>
                </tr>
            @endforeach
            </tbody>
        </table>
    </section>
@stop