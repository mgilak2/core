<?php

namespace UserManagement\Database\Seeds;

use Illuminate\Database\Seeder;

class FileManager extends Seeder
{
    public function run()
    {
        DB::table("settings")
            ->insert(['name' => "root", "parent_id" => 1,
                "created_at" => Carbon::now(), "updated_at" => Carbon::now()]);
    }
}
