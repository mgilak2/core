<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Filefolder extends Migration
{
    public function up()
    {
        Schema::create('folder_file', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('folder_id');
            $table->integer('file_id');
        });
    }

    public function down()
    {
        Schema::drop("folder_file");
    }
}
