<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Filemanager extends Migration
{
    public function up()
    {
        Schema::create('files', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('year');
            $table->string('month');
            $table->string('day');
            $table->string('type');
            $table->string('size');
        });
    }

    public function down()
    {
        Schema::drop("files");
    }
}
