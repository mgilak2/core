<?php

namespace FileManager;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Gallery extends Eloquent
{

    public $timestamps = false;
    protected $primaryKey = "id";
    protected $files;
    protected $fillable = ['id', 'name'];


    public function Folders()
    {
        return $this->belongsToMany(Folder::class, 'gallery_file', 'gallery_id', 'file_id');
    }
}