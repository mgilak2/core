<?php

namespace FileManager;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Folder_File_Attached extends Eloquent
{
    public $timestamps = false;
    protected $primaryKey = "id";
    protected $fillable = ['id', 'folder_id', 'file_id'];
}