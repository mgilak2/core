<?php

namespace FileManager;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Gallery_Folder_Attached extends Eloquent
{
    public $timestamps = false;
    protected $primaryKey = "id";
    protected $fillable = ['id', 'gallery_id', 'file_id'];
}