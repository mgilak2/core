<?php

namespace FileManager;

use Illuminate\Database\Eloquent\Model as Eloquent;


class File extends Eloquent
{

    public $timestamps = false;
    public $virtual_path, $x, $y, $width, $height;
    protected $primaryKey = "id";
    protected $files;
    protected $fillable = ['id', 'name', 'type', 'size', 'year', 'month', 'day'];

    //-------------------------------------------------------------------------------------------------------

    public function Folders()
    {
        return $this->belongsToMany(Folder::class, 'folder_file', 'file_id', 'folder_id');
    }

    public function Galleries()
    {
        return $this->belongsToMany(Gallery::class, 'gallery_file', 'file_id', 'gallery_id');
    }
}