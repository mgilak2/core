<?php

namespace FileManager;

use Illuminate\Database\Eloquent\Model as Eloquent;


class Folder extends Eloquent
{
    public $timestamps = false;
    protected $primaryKey = "id";
    protected $fillable = ['id', 'name', 'parent_id'];

    //-------------------------------------------------------------------------------------------------------

    public function Files()
    {
        return $this->belongsToMany(File::class, 'folder_file', 'folder_id', 'file_id');
    }
}