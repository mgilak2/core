<?php

namespace FileManager;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class FileManagerServiceProvider extends ServiceProvider implements CanMigrate, PhonemeComponent, Initializable
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var View
     */
    private $view;

    public function __construct(Application $app, Router $router, View $view)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->router = $router;
        $this->view = $view;
    }

    public function register()
    {
        // do nothing :)
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {
        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('FileManager', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->router->group(['namespace' => "FileManager"], function () {
            $this->router->get('dashboard/filemanager', ['as' => 'filemanager.index', 'uses' => 'FileController@getFileManager']);
        });

        $this->router->group(['prefix' => "dashboard/file", 'namespace' => "FileManager"], function () {
            $this->router->model("file_id", File::class);

            $this->router->get('/', ['as' => 'file.all', 'uses' => 'FileController@getIndex']);

            $this->router->get('/get', ['as' => 'file.getFile', 'uses' => 'FileController@getFile']);

            $this->router->get('/add', ['as' => 'file.index', 'uses' => 'FileController@getFileAdd']);
            $this->router->get('/edit/{file_id}', ['as' => 'file.edit', 'uses' => 'FileController@getEdit']);
            $this->router->get('/manage', ['as' => 'file.manage', 'uses' => 'FileController@getManage']);


            $this->router->post('/edit/{file_id}', 'FileController@fileEdit');
            $this->router->post('/add', ['as' => 'file.add', 'uses' => 'FileController@postFileAdd']);
            $this->router->post('/delete', ['as' => 'file.delete', 'uses' => 'FileController@postDelete']);


            $this->router->get('/rename/{file_id}', ['as' => 'file.rename', 'uses' => 'FileController@getFileRename']);
            $this->router->post('/rename', ['as' => 'file.rename', 'uses' => 'FileController@postFileRename']);

            $this->router->get('/crop/{file_id}', ['as' => 'file.crop', 'uses' => 'FileController@getFileCrop']);
            $this->router->post('/crop/{file_id}', ['as' => 'file.crop', 'uses' => 'FileController@postFileCrop']);

            $this->router->post('/multiPost', ['as' => 'file.multipleFiles', 'uses' => 'FileController@postMultipleFiles']);

            $this->router->get('/list', ['as' => 'file.list', 'uses' => 'FileController@getList']);

        });

        $this->router->group(['prefix' => "dashboard/folder", 'namespace' => "FileManager"], function () {
            $this->router->model("folder_id", Folder::class);

            $this->router->get('/index', ['as' => 'folder.all', 'uses' => 'FolderController@getIndex']);

            $this->router->get('/add', ['as' => 'folder.add', 'uses' => 'FolderController@getFolderAdd']);
            $this->router->get('/edit/{folder_id}', ['as' => 'folder.edit', 'uses' => 'FolderController@getFolderEdit']);
            $this->router->get('/manage', ['as' => 'folder.manage', 'uses' => 'FolderController@getFolderManage']);

            $this->router->post('/delete', ['as' => 'folder.delete', 'uses' => 'FolderController@postDelete']);

            $this->router->post('/add', ['as' => 'folder.add', 'uses' => 'FolderController@postFolderAdd']);
            $this->router->post('/edit', ['as' => 'folder.rename', 'uses' => 'FolderController@postFolderRename']);

            $this->router->get('/list', ['as' => 'folder.list', 'uses' => 'FolderController@getList']);
        });
    }
}