<?php
namespace FileManager;

use Illuminate\Support\Facades\Input;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class FileController extends Controller
{
    protected $view;
    protected $upload_path;

    public function __construct(View $view)
    {
        $this->view = $view;
        $this->upload_path = __DIR__ . "/../../../../../public/uploads/";

    }

    public function getFileAdd()
    {
        $theme = "Admin";

        return $this->view->make("FileManager::FileAdd", compact("theme"));
    }

    public function getManage()
    {
        return $this->view->make("FileManager::FileManage")
            ->with('cats', File::all());
    }

    public function getFile()
    {
        $myfile = new MyFile();
        $myfile->CropImage(50, 50, 0, 5, __DIR__ . '/../../../uploads/m.jpg');

    }

    public function getEdit(File $File)
    {
        $cats = FileToCat::where('File_id', $File->id)->get();
        foreach ($cats as $cat) {
            $File->File_cats .= ',' . $cat->id;
        }
        $File->File_cats = trim($File->File_cats, ",");
        $theme = "Admin";

        return $this->view->make("FileManager::FileEdit", compact("theme", 'File'));
    }

    public function FileAdd()
    {
        $result = File::create(Input::all());

        $cats = explode(',', Input::get("File_cats"));
        foreach ($cats as $cat) {
            FileToCat::create(array(
                'File_id' => $result->id,
                'cat_id' => $cat
            ));
        }
        return Redirect::route('File.list');
    }

    public function FileEdit(File $File)
    {
        $File->subject = Input::get('subject');
        $File->permalink = Input::get('permalink');
        $File->content = Input::get('content');
        $File->featured_image = Input::get('featured_image');
        $File->save();

        $cats = explode(',', Input::get("File_cats"));
        foreach ($cats as $cat) {
            FileToCat::firstOrCreate(array(
                'File_id' => $File->id,
                'cat_id' => $cat
            ));
        }

        return Redirect::route('File.getEdit', array('id' => $File->id));
    }

    public function getList()
    {
        $files = File::all();

        $theme = "Admin";
        return $this->view->make("FileManager::FileList", compact("theme", 'files'));
    }

    public function postMultipleFiles()
    {
        $myFile = new MyFile();

        foreach (\Input::file('files') as $file) {
            $myFile->RenameUpload($file, $this->upload_path);
        }

        $files = $myFile->FileList($this->upload_path);

        $theme = "Admin";
        return $this->view->make("FileManager::FileList", compact("theme", 'files'));
    }

    public function postFileAdd()
    {

        $myFile = new MyFile();

        $year = date("Y");
        $month = date("m");
        $day = date("d");

        $result = $myFile->RenameUpload(\Input::file('file'), $this->upload_path . $year . '/'
            , $this->upload_path . $year . '/' . $month . '/');

        if ($result->success) {
            $file = new File;
            $file->name = basename($result->finalName);
            $file->size = $result->size;
            $file->type = $result->type;
            $file->year = $year;
            $file->month = $month;
            $file->day = $day;
            $file->save();
            $file->Folders()->attach(\Input::get('parent_id'));
        } else {
            return \Response::json('error');
        }

        return \Response::json('ok');
    }

    public function getIndex()
    {
        $folder = Folder::find(\Input::get('id'));

        $dbfiles = $folder->Files;

        $files = [];

        foreach ($dbfiles as $file) {

            $filename = $file->name;

            if (strlen($filename) > 20) {
                $filename = substr($filename, 0, 15) . '...';
            }

            array_push($files,
                array('id' => $file->id, 'name' => $filename,
                    'fullname' => $file->name,
                    'url' => asset('uploads') . '/' . $file->year . '/' . $file->month . '/' . $this->nameHandler($file->name, "180x130"),
                    'naturalSize' => asset('uploads') . '/' . $file->year . '/' . $file->month . '/' . $file->name,
                    'size' => $file->size
                )
            );
        }
        $theme = "Admin";
        return $this->view->make("FileManager::file-template", compact("theme", 'files'));
    }

    public function nameHandler($name, $size)
    {
        $nameParts = explode(".", $name);
        return $nameParts[0] . "-thumb-" . $size . "." . $nameParts[1];
    }


    public function postFileRename()
    {

        $file = File::find(\Input::get('id'));

        $base = $this->upload_path . $file->year . '/' . $file->month . '/';


        \File::copy($base . $file->name, $base . \Input::get('name'));


        $myFile = new MyFile();

        $myFile->FileDelete($this->upload_path . $file->year . '/' . $file->month . '/', $file->name);

        $myFile->CreateThumbsImgUploaded($base, $base . \Input::get('name'));

        $file->name = \Input::get('name');
        $file->save();

        return \Redirect::route('file.list');
    }

    public function getFileCrop(File $file)
    {

        $file->virtual_path = asset('uploads') . '/' . $file->year . '/' . $file->month . '/' . $file->name;

        $imgData = getimagesize($this->upload_path . $file->year . '/' . $file->month . '/' . $file->name);
        $file->width = $imgData[0];
        $file->height = $imgData[1];


        $theme = "Admin";
        return $this->view->make("FileManager::FileCrop", compact("theme", 'file'));
    }

    public function postFileCrop(File $file)
    {
        $width = \Input::get('width');
        $height = \Input::get('height');
        $x = \Input::get('x');
        $y = \Input::get('y');

        $fullPath = $this->upload_path . $file->year . '/' . $file->month . '/' . $file->name;

        $myFile = new MyFile();
        $finalName = $myFile->CropImage($width, $height, $x, $y, $fullPath);
        $myFile->CreateThumbsImgUploaded($this->upload_path . $file->year . '/' . $file->month . '/', $finalName);

        $info = $myFile->getUploadedInfo($finalName);
        $nfile = new File;
        $nfile->name = basename($finalName);
        $nfile->size = $info['size'];
        $nfile->type = $info['type'];
        $nfile->year = $file->year;
        $nfile->month = $file->month;
        $nfile->day = $file->day;
        $nfile->save();

        $theme = "Admin";
        return \Redirect::route('file.list');
    }

    public function postDelete()
    {
        try {
            $File = File::find(\Input::get('id'));
            $File->destroy($File->id);

            Folder_File_Attached::where('file_id', $File->id)->delete();

            $myFile = new MyFile();
            $myFile->FileDelete($this->upload_path . $File->year . '/' . $File->month . '/', $File->name);
        } catch (exception $ex) {
            return \Response::json($ex->getMessage());
        }

        return \Response::json('deleted');
    }

    public function getFileManager()
    {
        $theme = "Admin";
        return $this->view->make("FileManager::Index", compact("theme"));
    }
}