<?php

namespace FileManager;

use Illuminate\View\Factory;
use Phoneme\Http\Controllers\Controller;

class FolderController extends Controller
{
    protected $View;
    protected $upload_path;

    public function __construct(Factory $view)
    {
        $this->View = $view;
        $this->upload_path = __DIR__ . "/../../../uploads/";

    }


    public function getFolderDelete(Folder $Folder)
    {
        $Folder->destroy($Folder->id);

        $theme = "Admin";
        return \Redirect::route('Folder.index');
    }

    public function postDelete()
    {
        $id = \Input::get('id');

        $dbfolders = Folder::where('parent_id', $id)->get();
        $dbfolders->push(Folder::find($id));

        $result = [];
        foreach ($dbfolders as $folder) {
            $files = $folder->Files;
            foreach ($files as $file) {
                $file->destroy($file->id);

                $myFile = new MyFile();
                try {
                    $myFile->FileDelete($this->upload_path . $file->year . '/' . $file->month . '/', $file->name);
                } catch (exception $ex) {
                }
            }
            $folder->destroy($folder->id);
            Folder_File_Attached::where('folder_id', $folder->id)->delete();
        }

        return \Response::json('deleted');
    }

    public function postFolderAdd()
    {
        $Folder = new Folder;
        $Folder->name = \Input::get('name');
        $Folder->parent_id = \Input::get('id');
        $Folder->save();

        return \Response::json('success');
    }

    public function postFolderRename()
    {
        $Folder = Folder::find(\Input::get('id'));
        $Folder->name = \Input::get('name');
        $Folder->save();

        return \Response::json('success');
    }

    public function getList()
    {
        $dbfolders = Folder::where('parent_id', 15)->get();
        $dbfolders->push(Folder::find(15));

        $result = [];
        foreach ($dbfolders as $folder) {
            $files = $folder->Files;
            foreach ($files as $file) {
                $file->destroy($file->id);

                $myFile = new MyFile();
                try {
                    $myFile->FileDelete($this->upload_path . $file->year . '-' . $file->month . '/', $file->name);
                } catch (exception $ex) {
                }
            }
            $folder->destroy($folder->id);
            Folder_File_Attached::where('folder_id', $folder->id)->delete();
        }

        return \Response::json($result);
    }


    public function getIndex()
    {

        $dbfolders = Folder::where('parent_id', \Input::get('id'))->get();

        $folders = [];

        foreach ($dbfolders as $folder) {
            $foldername = $folder->name;

            if (strlen($foldername) > 20) {
                $filename = substr($foldername, 0, 20) . '...';
            }

            array_push($folders, array('id' => $folder->id,
                'name' => $foldername,
                'fullname' => $folder->name,
                'url' => $folder->name));
        }

        $theme = "Admin";
        return $this->View->make("FileManager::folder-template", compact("theme", 'folders'));
    }
}