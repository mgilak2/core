<?php

namespace FileManager;

use Phoneme\Http\Controllers\Controller;

class MyFile extends Controller
{
    public static $thumbs = [
        ['width' => 180, 'height' => 130],  // File manager thumbs !
        ['width' => 45, 'height' => 45],    // Table and list view thumb
        ['width' => 300, 'height' => 300],  // Tour grid thumb
        ['width' => 1400, 'height' => 650]  // Slider images
    ];

    public function UploadFile($file, $path)
    {
        \File::exists($path);
        md5($file);
    }

    public function RenameUpload($file, $pathYear, $pathMonth)
    {
        self::CreateFolder($pathYear);
        self::CreateFolder($pathMonth);

        $path = $pathMonth;

        $log = new UploadLog();

        $info = self::getInfo($file);
        $file_path = $path . $info['name'];
        $file_paths = self::renameFile($file_path, $info['ext'], $path);

        return self::Upload($file, $path, $file_paths);
    }

    public function CreateFolder($path, $mode = 777, $recursive = true, $force = false)
    {
        \File::makeDirectory($path);
    }

    public function getInfo($file)
    {
        $ext = $file->getClientOriginalExtension();
        return array(
            'name' => $file->getClientOriginalName(),
            'ext' => $ext,
            'size' => $file->getClientSize(),
            'type' => self::FileType($ext)
        );
    }

    public function FileType($ext)
    {
        $image_extensions = array("jpg", "jpeg", "gif", "png");
        $word_extensions = array("doc", "dot", "docx", "docm", "dotx", "dotm", "docb");
        $excel_extensions = array("xls", "xlt", "xlsx", "xlsm", "xltx", "xltm", "xlsb", "xla", "xlam", "xll", "xlw");
        $powerpoint_extensions = array("ppt", "pot", "pps", "pptx", "pptm", "potx", "potm", "ppam", "ppsx", "ppsm", "sldx", "sldm");
        $pdf_extensions = array("pdf");
        $txt_extensions = array("txt");

        if (in_array($ext, $image_extensions)) {
            return LogType::image;
        } elseif (in_array($ext, $pdf_extensions)) {
            return LogType::pdf;
        } elseif (in_array($ext, $word_extensions)) {
            return LogType::word;
        } elseif (in_array($ext, $excel_extensions)) {
            return LogType::excel;
        } elseif (in_array($ext, $powerpoint_extensions)) {
            return LogType::powerpoint;
        } elseif (in_array($ext, $txt_extensions)) {
            return LogType::txt;
        } else {
            return LogType::file;
        }
    }

    private function renameFile($file_path, $ext, $path_dir)
    {
        $final_paths = [];
        $thumbs = [];
        $counter = 0;

        $basename = substr_replace(basename($file_path, $ext), "", -1);
        $prefix = $path_dir . $basename;

        $final_paths['filename'] = $file_path;
        $final_paths['thumbs'] = [];

        $thumbs_count = count($this::$thumbs);

        for ($counter = 0; $counter < $thumbs_count; $counter++) {
            array_push($final_paths['thumbs'], $prefix . '-thumb-' . $this::$thumbs[$counter]['width'] . 'x' . $this::$thumbs[$counter]['height'] . '.' . $ext);
        }

        return $final_paths;
    }

    public function Upload($file, $path, $file_paths)
    {
        $info = self::getInfo($file);

        $filename = isset($file_paths) ? $file_paths['filename'] : $info['name'];

        $log = new UploadLog();
        $log->type = $info['type'];
        $log->size = $info['size'];
        $log->finalName = $filename;

        try {
            $file->move($path, $filename);
            $log->success = true;

            if ($log->type != LogType::image || !isset($file_paths))
                return $log;

            $thumbs_count = count($this::$thumbs);

            for ($i = 0; $i < $thumbs_count; $i++) {
                \Image::make($filename)->fit($this::$thumbs[$i]['width'], $this::$thumbs[$i]['height'])->save($file_paths['thumbs'][$i]);
            }

            return $log;
        } catch (\Exception $e) {
            $log->success = false;
            $log->message = $e->getMessage();
            return $log;
        }
    }

    public function FileExists($file, $path)
    {
        $info = self::getInfo($file);

        return \File::exists($path . $info['name']);
    }

    public function FileDelete($base, $name)
    {
        $uploadedinfo = self::getUploadedInfo($base . $name);

        if ($uploadedinfo['type'] != LogType::image) {
            if (\File::exists($base . $name)) {
                \File::delete($base . $name);
                return true;
            }
            return false;
        }


//        dd($uploadedinfo);

        if (\File::exists($base . $name))
            \File::delete($base . $name);

        $basename = substr_replace(basename($name, $uploadedinfo['ext']), '', -1);

        //  $result=[];

        foreach ($this::$thumbs as $thumb) {
            // array_push($result,$base.$basename.'-thumb-'.$thumb['width'] .'x'. $thumb['height'].$uploadedinfo['ext']);

            \File::delete($base . $basename . '-thumb-' . $thumb['width'] . 'x' . $thumb['height'] . '.' . $uploadedinfo['ext']);
        }

    }

    public function getUploadedInfo($path)
    {
        $info = new \SplFileInfo($path);
        $ext = $info->getExtension();
        return array(
            'name' => $info->getFilename(),
            'ext' => $ext,
            'size' => $info->getSize(),
            'type' => self::FileType($ext),
            'dir' => dirname($path) . '/'
        );
    }

    public function CropImage($width, $height, $x, $y, $path)
    {
        $destination = self::copyFile($path, null, '-crop', false);

        $img = \Image::make($path);

        $img->crop($width, $height, $x, $y)->save($destination);

        return $destination;
    }

    public function copyFile($file_path, $newname = null, $suffix = '-copy', $onlynewpath = true)
    {
        $log = new UploadLog();
        $log->action = LogAction::copyfile;

        try {

            $res_path = $file_path;
            $info = self::getUploadedInfo($file_path);

            $ext = $info['ext'];

            if ($newname == null) {
                $newname = substr_replace(basename($info['name'], $ext), "", -1);
            }

            $newname .= $suffix;


            $prefix = $info['dir'] . $newname;

            $file_path = $prefix . '.' . $ext;

            $counter = 0;
            while (\File::exists($file_path)) {
                $prefix = $info['dir'] . $newname . '_item_' . (++$counter);
                $file_path = $prefix . '.' . $ext;
            }

            if ($onlynewpath)
                \File::copy($res_path, $file_path);
            else {
                return $file_path;
            }

            $log->success = true;
            $log->action;
            $log->finalName = $file_path;

            return $log;

        } catch (\Exception $e) {
            $log->success = false;
            $log->message = $e->getMessage();
            return $log;
        }
    }

    public function FileList($path)
    {
        $files = \File::allFiles($path);
        $result = array();
        foreach ($files as $file) {
            $ext = $file->getExtension();
            array_push($result, array(
                'name' => $file->getPath() . '/' . $file->getRelativePathName(),
                'ext' => $ext,
                'type' => self::FileType($ext),// $file->getType(),
                'size' => $file->getSize()
            ));
        }
        return $result;
    }

    public function FolderList($path)
    {
        $folders = \File::directories($path);

        $i = 0;
        $result = [];
        foreach ($folders as $folder) {
            $data = self::dirSize($folder);
            $result[$i++ . '_filecount'] = $data['count'];
            $result[$i . '_filessize'] = $data['size'];
        }

        return $result;
    }

    function dirSize($directory)
    {
        $size = 0;
        $count = 0;
        foreach (new \RecursiveIteratorIterator(new \RecursiveDirectoryIterator($directory)) as $file) {
            $size += $file->getSize();
            $count++;
        }
        return ['size' => $size, 'count' => $count];
    }

    public function rename($file_path, $newname)
    {

    }

    public function CreateThumbsImgUploaded($base, $filename)
    {
        $uploadedinfo = self::getUploadedInfo($filename);

        if ($uploadedinfo['type'] != LogType::image) {
            return false;
        }

        $ext = $uploadedinfo['ext'];

        $thumbs_count = count($this::$thumbs);

        $basename = substr_replace(basename($filename, $ext), '', -1);

        $prefix = $base . $basename;

        for ($counter = 0; $counter < $thumbs_count; $counter++) {
            \Image::make($filename)->
            fit($this::$thumbs[$counter]['width'], $this::$thumbs[$counter]['height'])->
            save($prefix . '-thumb-' . $this::$thumbs[$counter]['width'] . 'x' . $this::$thumbs[$counter]['height'] . '.' . $ext);
        }
    }
}