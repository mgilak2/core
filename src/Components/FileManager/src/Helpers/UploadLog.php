<?php
namespace FileManager;

class UploadLog
{
    public $success;
    public $action;
    public $type;
    public $size;
    public $finalName;
    public $thumbs = [];
    public $message;

    function  __construct($success = null, $action = null, $type = null, $size = null, $finalName = null, $thumbs = null, $message = null)
    {
        $this->success = $success;
        $this->action = $action;
        $this->type = $type;
        $this->size = $size;
        $this->finalName = $finalName;
        $this->thumbs = $thumbs;
        $this->message = $message;
    }
}

class LogType
{
    const file = 0;
    const image = 1;
    const word = 2;
    const excel = 3;
    const powerpoint = 4;
    const pdf = 5;
    const txt = 6;
}

class LogAction
{
    const fetchfile = 0;
    const fetchfolder = 1;

    const uploadfile = 2;
    const createfolder = 3;

    const deltefile = 4;
    const deltefolder = 5;

    const renamefile = 6;
    const renamefolder = 7;

    const copyfile = 8;
    const copyfolder = 9;
}
