@extends("Admin::dashboard")

@section("master.ticket.form.add")

    <table class="table">
        <thead>
        <tr>
            <th>#</th>
            <th>id</th>
            <th>عنوان</th>
            <th>تصویر</th>
        </tr>
        </thead>
        <tbody>
        <tr>
            @foreach ($mainMenus as $index => $menu)
                <td>{{ ++$index }}</td>
                <td>{{ $menu->id }}</td>
                <td>{{ $menu->title }}</td>
        </tr>
        @endforeach
        </tbody>
    </table>
    </section>
@stop