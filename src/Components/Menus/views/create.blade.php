@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن منو
@endsection

@section("master.ticket.form.add")

    {!! Form::open(['route'=>'menu.create.post']) !!}
    <?php
    $message = "";
    if ($errors->has()) {
        $message .= '<ul id="message">';
        foreach ($errors->all('<li class="error">:message</li>') as $m) {
            $message .= $m;
        }
        $message .= '</ul>';
        echo $message;
    }
    ?>
    <div class="row">
        <div class="form-group col-sm-4">
            <label class="control-label">عنوان:</label>
            {!! Form::text('title',Input::get("title"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label">مکان ها :</label>
            {!! Form::select('area',$areas,null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <label class="control-label">&nbsp;</label>
            <button type="submit" class="btn btn-primary btn-block" role="button">ایجاد</button>
        </div>
        <div class="form-group col-sm-2">
            <label class="control-label">&nbsp;</label>
            <a href="/admin/menu" class="btn btn-default btn-block" role="button" type="reset">انصراف</a>
        </div>
    </div>
    {!! Form::close() !!}
@stop