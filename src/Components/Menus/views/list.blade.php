@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-list"></i>
    لیست منوها

    <a href="{{route('menu.create')}}" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>
@endsection

@section("master.ticket.form.add")
    {!! Form::open(['route'=>'menu.create.post']) !!}
    <?php
    $message = "";
    if ($errors->has()) {
        $message .= '<ul id="message">';
        foreach ($errors->all('<li class="error">:message</li>') as $m) {
            $message .= $m;
        }
        $message .= '</ul>';
        echo $message;
    }
    ?>
    <table class="table table-striped">
        <colgroup>
            <col style="width: 80px;"/>
            <col/>
            <col style="width: 60px;"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        @foreach ($menus as $index => $menu)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $menu->title }}</td>
                <td>
                    <a href="{{ route('menu.update',array($menu->id)) }}" class="btn btn-primary btn-sm">
                        <i class="icon-pencil"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    {!! Form::close() !!}
@stop