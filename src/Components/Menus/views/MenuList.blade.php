@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-list"></i>
    لیست منوها

    <a href="/menu/add" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>
@endsection

@section("master.ticket.form.add")
    <table class="table table-striped">
        <colgroup>
            <col style="width: 80px;"/>
            <col/>
            <col/>
            <col style="width: 80px;"/>
        </colgroup>
        <thead>
        <tr class="active">
            <th>ردیف</th>
            <th>عنوان</th>
            <th>آدرس</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>
        @foreach ($menus as $index => $menu)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $menu->title }}</td>
                <td>{{ $menu->url }}</td>
                <td>
                    <a href="{{ route('menu.edit',array($menu->id)) }}" class="btn btn-primary btn-sm">
                        <i class="icon-pencil"></i>
                    </a>
                    <a href="{{ route('menu.delete',array($menu->id)) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')">
                        <i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop
