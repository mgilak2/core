@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن منو

    <a href="/dashboard/menu/" class="btn btn-primary pull-left">
        <i class="fa fa-list"></i>
        لیست منوهای اصلی
    </a>
@endsection

@section("master.ticket.form.add")
    <div class="row">
        {!! Form::open(['route' => ['menu.update.put', $menu->id], 'method' => 'put' ]) !!}
        <input type="hidden" value="{{ csrf_token() }}" class="_token">
        <div class="form-group col-sm-4">
            <label class="control-label">عنوان منو</label>
            {!! Form::text('title', $menu->title, array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label">مکان منو</label>
            {!! Form::select('area',$areas,$menu->area) !!}
        </div>
        <div class="form-group col-sm-2">
            <label class="control-label">&nbsp;</label>
            <button type="submit" class="btn btn-primary btn-block" role="button">بروزرسانی</button>
        </div>
        {!! Form::close() !!}
        <div class="col-sm-6">
            <h4>لیست منوها</h4>
        </div>
    </div>
    <div class="pages-list">

        <div class="row">
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>لیست صفحات</h4>
                    </div>
                    {!! Form::open(['route' => ['menu.update.add-item.post', $menu->id]]) !!}
                    <div class="panel-body">
                        <div class="list-group">
                            @foreach($pages as $page)
                                <div class="list-group-item">
                                    <div class="checkbox">
                                        <label>
                                            <input type="radio" name="page-id" value="{{$page->id}}">
                                            {{$page->subject}}
                                        </label>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary" role="button">افزودن</button>
                    </div>
                    {!! Form::close() !!}
                </div>

                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h4>آیتم ها</h4>
                    </div>
                    {!! Form::open(['route' => ['menu.update.add-item.post', $menu->id]]) !!}
                    <div class="panel-body">
                        <div class="list-group">
                            <div class="list-group-item">
                                <div class="checkbox">
                                    <label>
                                        <input type="radio" name="item-id" value="0">
                                        سربرگ
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <button type="submit" class="btn btn-primary" role="button">افزودن</button>
                    </div>
                    {!! Form::close() !!}
                </div>
            </div>
            <div class="col-sm-6">
                <div class="panel panel-default">
                    <div class="panel-body">
                        <div class="dd" id="nestable">
                            {!!  app()->make(\Menus\MenuOutput::class)
                            ->show_menu_tree($menu->id) !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section("extensionScript")
    @include("Admin::extensions.menu-nested")
@endsection