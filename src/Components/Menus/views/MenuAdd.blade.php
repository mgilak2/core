@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن منو
@endsection

@section("master.ticket.form.add")
    @if(isset($menu))
        {!! Form::model($menu,['route'=>['menu.edit',$menu->id]]) !!}
    @else
        {!! Form::open(['route'=>'menu.add']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-4">
            <label class="control-label">عنوان :</label>
            {!! Form::text('title',\Phoneme\Http\Requests\Request::get("title"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label">مادر</label>
            {!! Form::text('parent_id',\Phoneme\Http\Requests\Request::get("parent_id"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-4">
            <label class="control-label">وزن :</label>
            {!! Form::text('weight',\Phoneme\Http\Requests\Request::get("weight"), array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-3">
            <label class="control-label">آدرس :</label>
            {!! Form::text('url',\Phoneme\Http\Requests\Request::get("url"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-3">
            <label class="control-label">post_id :</label>
            {!! Form::text('post_id',\Phoneme\Http\Requests\Request::get("post_id"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-3">
            <label class="control-label">دسته بندی :</label>
            {!! Form::text('cat_id',\Phoneme\Http\Requests\Request::get("cat_id"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-3">
            <label class="control-label">دسته بندی منوی اصلی:</label>
            {!! Form::text('main_menu_id',\Phoneme\Http\Requests\Request::get("main_menu_id"), array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ایجاد</button>
        </div>
        <div class="form-group col-sm-2">
            <a href="/menu/list" class="btn btn-warning btn-block" role="button" type="reset">انصراف</a>
        </div>
    </div>
    {!! Form::close() !!}
@stop