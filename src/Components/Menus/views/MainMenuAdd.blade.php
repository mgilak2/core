@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن منو
@endsection

@section("master.ticket.form.add")

    @if(isset($mainMenu))
        {!! Form::model($mainMenu,['route'=>['mainMenu.edit',$mainMenu->id]]) !!}
    @else
        {!! Form::open(['route'=>'mainMenu.add']) !!}
    @endif
    <div class="row">
        <div class="form-group col-sm-6">
            <label class="control-label">عنوان :</label>
            {!! Form::text('title',isset($mainMenu)?$mainMenu->title:\Illuminate\Support\Facades\Request::get("title"), array('class' => 'form-control')) !!}
        </div>
        <div class="form-group col-sm-6">
            <label class="control-label">زبان :</label>
            {!! Form::select('language',$languages,null, array('class' => 'form-control')) !!}
        </div>
    </div>
    <div class="row">
        <div class="form-group col-sm-2">
            <button type="submit" class="btn btn-primary btn-block" role="button">ثبت</button>
        </div>
        <div class="form-group col-sm-2">
            <button class="btn btn-warning btn-block" role="button" type="reset">انصراف</button>
        </div>
    </div>
    {!! Form::close() !!}
@stop