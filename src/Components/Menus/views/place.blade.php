@extends("Admin::dashboard")

@section("pageTitle")
    جا گذاری منو ها
@stop

@section("content")
    <link rel="stylesheet" href="{{ asset("Aone/modules/Widget/views/assets/style.css") }}">

    <div class="row">
        <div class="col-md-6">
            @foreach($areas as $area)
                {{ $area->name }}
                @foreach($menus as $menu)
                    {{$menu->title}}
                @endforeach
            @endforeach
        </div>
    </div>
@stop

@section("master.footer")
    @include("Admin::footer")
@stop
