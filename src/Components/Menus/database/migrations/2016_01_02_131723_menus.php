<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Menus extends Migration
{
    public function up()
    {
        Schema::create('menus', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('area');
        });

        Schema::create('menus_item', function (Blueprint $table) {
            $table->increments('id');
            $table->string('title');
            $table->string('url');
            $table->integer('parent_id');
            $table->integer('cat_id');
            $table->integer('post_id');
            $table->integer('weight');
            $table->integer('menu_id');
        });
    }

    public function down()
    {
        Schema::dropTable("menus");
        Schema::dropTable("menus_item");
    }
}
