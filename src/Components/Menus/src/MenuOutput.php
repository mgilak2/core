<?php

namespace Menus;

class MenuOutput
{
    public function show_menu($title, $attributes = [])
    {
        $attr = "";
        $attr_list = [];
        foreach ($attributes as $k => $v) {
            $attr_list[] .= $k . '="' . $v . '"';
        }

        if ($attr_list)
            $attr = " " . implode(' ', $attr_list);

        $menu = \DB::table('menus')->where('title', $title)->first();
        if (!$menu)
            return "";

        $items = \DB::table('menus_item')->where('menu_id', $menu->id)->get();

        return $this->create_menu_tree($items, 0, $attr);
    }

    private function create_menu_tree($items, $parent_id = 0, $attribute = "")
    {
        $childs = $this->get_node_childs($items, $parent_id);
        $output = "";
        $custom_attr = ' role="button" class="navbar-btn dropdown-toggle" data-toggle="dropdown"';

        if (!empty($childs)) {
            $output .= "<ul$attribute>";
            foreach ($childs as $b) {
                $tree = $this->create_menu_tree($items, $b['id']);
                $extra = $tree ? $custom_attr : "";
                $output .= '<li><a href="' . $b['url'] . '"' . $extra . '>' . $b['title'] . '</a>';
                $output .= $tree;
                $output .= '</li>';
            }
            $output .= "</ul>";
        }

        return $output;
    }

    private function get_node_childs($nodes, $node_id)
    {
        $return = [];
        foreach ($nodes as $n) {
            if ($n->parent_id == $node_id)
                $return[] = [
                    'id' => $n->id,
                    'title' => $n->title,
                    'url' => $n->url,
                    'weight' => $n->weight
                ];
        }
        usort($return, function ($a, $b) {
            return $a['weight'] > $b['weight'] ? 1 : ($a['weight'] == $b['weight'] ? 0 : -1);
        });

        return $return;
    }

    // edit menu tree
    public function show_menu_tree($menu_id)
    {
        $menu = \DB::table('menus')->find($menu_id);
        if (!$menu)
            return "";

        $items = \DB::table('menus_item')->where('menu_id', $menu->id)->get();

        return $this->create_menu_tree2($items);
    }

    private function create_menu_tree2($items, $parent_id = 0)
    {
        $childs = $this->get_node_childs($items, $parent_id);
        $output = "";

        if (!empty($childs)) {
            $output .= "<ol class='dd-list'>";
            foreach ($childs as $b) {
                $output .= "<li class='dd-item' data-id='" . $b['id'] . "'>" .
                    "<a target='_parent' class='btn btn-danger btn-xs delete_toggle' rel='" . $b['id'] . "' href='#'><i class='fa fa-trash-o'></i></a>
				            <a target='_parent' class='btn btn-primary btn-xs edit_toggle' rel='" . $b['id'] . "'><i class='fa fa-edit'></i></a>" .
                    "<div class='dd-handle' href='" . $b['url'] . "'>" . $b['title'] . "</div>";
                $output .= $this->create_menu_tree2($items, $b['id']);
                $output .= '</li>';
            }
            $output .= "</ol>";
        }

        return $output;
    }
}
