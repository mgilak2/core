<?php namespace Menus;

use Exception;

class MenuService
{
    public function add($data)
    {
        if (!isset($data['weight'])) {
            $data['weight'] = 0;
        }

        if (!isset($data['parent_id'])) {
            $data['parent_id'] = 0;
        }

        if (!isset($data['fa'])) {
            $data['fa'] = "";
        }

        if (!isset($data['en'])) {
            $data['en'] = "";
        }

        if (!isset($data['icon'])) {
            $data['icon'] = "";
        }

        if (!isset($data['module'])) {
            throw new Exception("module field is required");
        }

        if (!isset($data['route'])) {
            $data['route'] = "";
        }

        $id = \DB::table("menu_backend")->insertGetId($data);

        return new MenuItem(\DB::table("menu_backend")->where("id", $id)->first());
    }

    public function remove($module)
    {
        \DB::table("menu_backend")
            ->where('module', $module)
            ->delete();
    }

    public function present()
    {
        $majors = \DB::table("menu_backend")->where('parent_id', 0)->orderBy('weight', 'asc')->get();

        foreach ($majors as $major) {
            $major->children = \DB::table("menu_backend")->where('parent_id', $major->id)->orderBy('weight', 'asc')->get();
        }

        $menus = $majors;

        return $menus;
    }

    // show menu in theme
    public function show_menu($title, $menu = null, $attributes = [])
    {
        $attr = "";
        $attr_list = [];
        foreach ($attributes as $k => $v) {
            $attr_list[] .= $k . '="' . $v . '"';
        }

        if ($attr_list) {
            $attr = " " . implode(' ', $attr_list);
        }

        if (is_null($menu)) {
            $menu = \DB::table('menus')->where('title', $title)->first();
        }

        if (!$menu) {
            return "";
        }

        $items = \DB::table('menu_item')->where('menu_id', $menu->id)->get();

        return $this->create_menu_tree($items, 0, $attr);
    }

    private function create_menu_tree($items, $parent_id = 0, $attribute = "")
    {
        $childs = $this->get_node_childs($items, $parent_id);
        $output = "";
        $custom_attr = ' role="button" class="navbar-btn dropdown-toggle" data-toggle="dropdown"';

        if (!empty($childs)) {
            $output .= "<ul$attribute>";
            foreach ($childs as $b) {
                $the_url = $b['url'];
                if (!$the_url) {
                    $post = \Post\Post::find($b['post_id']);
                    $the_url = $post ? url($post->permalink) : "";
                }
                $tree = $this->create_menu_tree($items, $b['id']);
                $extra = $tree ? $custom_attr : "";
                $output .= '<li><a href="' . $the_url . '"' . $extra . '>' . $b['title'] . '</a>';
                $output .= $tree;
                $output .= '</li>';
            }
            $output .= "</ul>";
        }

        return $output;
    }

    private function get_node_childs($nodes, $node_id)
    {
        $return = [];
        foreach ($nodes as $n) {
            if ($n->parent_id == $node_id)
                $return[] = (array)$n;
        }
        usort($return, function ($a, $b) {
            return $a['weight'] > $b['weight'] ? 1 : ($a['weight'] == $b['weight'] ? 0 : -1);
        });

        return $return;
    }

    public function show_menu_tree($menu_id)
    {
        $menu = \DB::table('menus')->find($menu_id);
        if (!$menu)
            return "";

        $items = \DB::table('menus_item')->where('menu_id', $menu->id)->get();

        return $this->create_menu_tree2($items);
    }

    private function create_menu_tree2($items, $parent_id = 0)
    {
        $childs = $this->get_node_childs($items, $parent_id);
        $output = "";

        if (!empty($childs)) {
            $output .= "<ol class='dd-list'>";
            foreach ($childs as $b) {
                $the_url = $b['url'];
                if (!$the_url) {
                    $post = \Post\Post::find($b['post_id']);
//                    $the_url = $post ? route('page', [$post->permalink]) : "";
                }

                $output .= "<li class='dd-item' data-id='" . $b['id'] . "'>" .
                    "<a target='_parent' class='btn btn-danger btn-xs delete_toggle' rel='" . $b['id'] . "' href='#'><i class='fa fa-trash-o'></i></a>
				            <a target='_parent' class='btn btn-primary btn-xs edit_toggle' rel='" . $b['id'] . "'><i class='fa fa-edit'></i></a>" .
                    "<div class='dd-handle' data-url='" . $the_url . "'>" . $b['title'] . "</div>";
                $output .= $this->create_menu_tree2($items, $b['id']);
                $output .= '</li>';
            }
            $output .= "</ol>";
        }

        return $output;
    }

    public function show_backend_menu()
    {
        $items = MenuBackend::all();
        return $this->backend_menu_tree($items);
    }

    public function backend_menu_tree($items, $parent_id = 0)
    {
        $childs = $this->get_node_childs($items, $parent_id);
        $output = "";

        if (!empty($childs)) {
            $output .= "<ol class='dd-list'>";
            foreach ($childs as $b) {
                $the_url = $b['url'];
                if (!$the_url) {
                    $post = \Post\Post::find($b['post_id']);
                    $the_url = $post ? url($post->permalink) : "";
                }

                $output .= "<li class='dd-item' data-id='" . $b['id'] . "'>" .
                    "<a target='_parent' class='btn btn-danger btn-xs delete_toggle' rel='" . $b['id'] . "' href='#'><i class='fa fa-trash-o'></i></a>
				            <a target='_parent' class='btn btn-primary btn-xs edit_toggle' rel='" . $b['id'] . "'><i class='fa fa-edit'></i></a>" .
                    "<div class='dd-handle' data-url='" . $the_url . "'>" . $b['title'] . "</div>";
                $output .= $this->backend_menu_tree($items, $b['id']);
                $output .= '</li>';
            }
            $output .= "</ol>";
        }

        return $output;
    }
}