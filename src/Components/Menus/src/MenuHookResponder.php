<?php

namespace Menus;

use Menus\Contracts\MenuArea;
use Phoneme\Contracts\CanManageHooks;
use Phoneme\Contracts\Hook;

class MenuHookResponder implements CanManageHooks
{
    protected $hook;

    public function add(Hook $hook)
    {
        $this->hook = $hook;
    }

    /**
     * @return MenuArea
     */
    public function getHook()
    {
        return $this->hook;
    }
}