<?php

namespace Menus\Contracts;

interface MenuArea
{
    public function getAreas();
}