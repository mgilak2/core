<?php

namespace Menus;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class MenusServiceProvider extends ServiceProvider
    implements CanMigrate, Initializable,
    PhonemeComponent
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Application $app, View $view, Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
    }

    public function register()
    {
        $this->app->bind(MenuService::class, function ($app) {
            return new MenuService();
        });

        $this->app->singleton(MenuHookResponder::class, function ($app) {
            return new MenuHookResponder();
        });
    }

    public function getMigrationArguments()
    {
        return [];
    }

    public function boot()
    {
        $this->defineRoutes();
        $this->viewsBaseFolder();
    }

    private function defineRoutes()
    {

        $this->router->get("hook", ["uses" => "Menus\\MenuController@showMeTheAreas"]);

        $this->router->group(['prefix' => "dashboard/menu", 'namespace' => "Menus"], function () {
            $this->router->get('/place', ['as' => 'menu.place', 'uses' => 'MenuController@getPlace']);
            $this->router->get('/create', ['as' => 'menu.create', 'uses' => 'MenuController@getCreate']);
            $this->router->post('/create', ['as' => 'menu.create.post', 'uses' => 'MenuController@postCreate']);
            $this->router->get('/{_id}', ['as' => 'menu.update', 'uses' => 'MenuController@getUpdate']);
            $this->router->put('/update/{_id}', ['as' => 'menu.update.put', 'uses' => 'MenuController@putUpdate']);
            $this->router->get('/', ['as' => 'menu.list', 'uses' => 'MenuController@getList']);
            $this->router->post('/update/{_id}/order', ['as' => 'menu.update.order.put', 'uses' => 'MenuController@postMenuOrder']);
            $this->router->put('/update/{_id}/item', ['as' => 'menu.update.item.put', 'uses' => 'MenuController@putMenuItem']);
            $this->router->post('/update/{_id}/add-item', ['as' => 'menu.update.add-item.post', 'uses' => 'MenuController@postAddItem']);
            $this->router->delete('/delete/{_id}/item', ['as' => 'menu.delete.item.delete', 'uses' => 'MenuController@deleteItem']);
        });
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Menu', __DIR__ . "/../views");
    }
}