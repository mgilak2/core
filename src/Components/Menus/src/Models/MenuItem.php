<?php namespace Menus;

class MenuItem
{
    protected $id;
    protected $module;

    public function __construct($menuItem)
    {
        $this->id = $menuItem->id;
        $this->module = $menuItem->module;
    }

    public function add($data)
    {
        if (!isset($data['weight'])) {
            $data['weight'] = 0;
        }

        $data['parent_id'] = $this->id;

        if (!isset($data['fa'])) {
            $data['fa'] = "";
        }

        if (!isset($data['en'])) {
            $data['en'] = "";
        }

        if (!isset($data['icon'])) {
            $data['icon'] = "";
        }

        if (!isset($data['route'])) {
            $data['route'] = "";
        }

        $data['module'] = $this->module;

//        \DB::table("cms.menu_backend")->insert($data);

        $id = \DB::table("menu_backend")->insertGetId($data);

        return new MenuItem(\DB::table("menu_backend")->where("id", $id)->first());
    }
}