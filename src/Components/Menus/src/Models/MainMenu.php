<?php

namespace Menus;

use Illuminate\Database\Eloquent\Model as Eloquent;

class MainMenu extends Eloquent
{
    public $timestamps = false;
    protected $table = "menus_item";
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title'];

    public function menus()
    {
        return $this->hasMany(Menu::class);
    }
}