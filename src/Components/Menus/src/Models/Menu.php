<?php

namespace Menus;

use Illuminate\Database\Eloquent\Model as Eloquent;

class Menu extends Eloquent
{
    public $timestamps = false;
    protected $primaryKey = "id";
    protected $fillable = ['id', 'title', 'area', 'parent_id',
        'url', 'post_id', 'cat_id', 'weight', 'main_menu_id'];

    public function menu()
    {
        return $this->belongsTo(MainMenu::class, 'id', 'main_menu_id');
    }
}