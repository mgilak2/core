<?php
namespace Menus;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class MainMenuController extends Controller
{
    protected $view;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Redirector
     */
    private $redirector;


    public function __construct(View $view, AdminTheme $theme,
                                Request $request, Redirector $redirector)
    {
        $this->view = $view;
        $this->theme = $theme;
        $this->request = $request;
        $this->redirector = $redirector;
    }

    public function getManage()
    {
        return $this->theme->baseDashboard("Menu::MainMenuManage");
    }

    public function getAdd()
    {
        return $this->theme
            ->baseDashboard("Menu::MainMenuAdd");
    }

    public function getEdit(MainMenu $mainMenu)
    {
        return $this->theme
            ->baseDashboard("Menu::MainMenuAdd");
    }

    public function getDelete(MainMenu $MainMenu)
    {
        $MainMenu->destroy($MainMenu->id);

        return $this->redirector->route('mainMenu.add');
    }

    public function postAdd()
    {
        MainMenu::create(
            $this->request->all()
        );

        return $this->redirector->route('mainMenu.list');
    }

    public function postEdit(MainMenu $MainMenu)
    {
        $MainMenu->title = $this->request->get('title');
        $MainMenu->save();

        return $this->redirector->route('mainMenu.edit', ['id' => $MainMenu->id]);
    }

    public function getList()
    {
        $mainMenus = MainMenu::all();
        return $this->theme->baseDashboard("Menu::MainMenuList",
            compact('mainMenus'));
    }
}