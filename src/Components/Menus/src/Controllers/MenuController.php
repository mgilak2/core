<?php
namespace Menus;

use AdminTheme\AdminTheme;
use Illuminate\Contracts\Routing\ResponseFactory;
use Illuminate\Database\DatabaseManager as DB;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\Factory as Validator;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;
use Post\Post;

class MenuController extends Controller
{
    protected $environment;
    protected $view;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var DB
     */
    private $db;
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var ResponseFactory
     */
    private $response;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var MenuHookResponder
     */
    private $hooks;

    public function __construct(View $view, AdminTheme $theme,
                                Request $request, DB $db,
                                Redirector $redirector, ResponseFactory $response,
                                Validator $validator, MenuHookResponder $hooks)
    {
        $this->view = $view;
        $this->theme = $theme;
        $this->request = $request;
        $this->db = $db;
        $this->redirector = $redirector;
        $this->response = $response;
        $this->validator = $validator;
        $this->hooks = $hooks;
    }

    public function showMeTheAreas()
    {
        $hook = $this->hooks->getHook();
        return $hook->getAreas();
    }

    public function getList()
    {
        $menus = $this->db->table('menus')->get();
        return $this->theme->baseDashboard("Menu::list", ['menus' => $menus]);
    }

    public function getCreate()
    {
        return $this->theme->baseDashboard("Menu::create", ['areas' => $this->hooks->getHook()->getAreas()]);
    }

    public function postCreate()
    {
        $inputs = $this->request->only('title', 'area');
        $rules = array(
            'title' => 'required'
        );

        $validator = $this->validator->make($inputs, $rules);

        if ($validator->fails())
            return $this->redirector->route('menu.create')
                ->withErrors($validator)
                ->withInput($inputs);

        $this->db->table('menus')->insert($inputs);

        return $this->redirector->route('menu.list');
    }

    public function getUpdate($menu_id = "")
    {
        $menu = $this->db->table('menus')->find($menu_id);
        $items = $this->db->table('menus_item')->where('menu_id', $menu_id)->get();
        $pages = $this->db->table('posts')->where('type', '2')->get();
        return $this->view->make("Menu::update")->with([
            'menu' => $menu,
            'items' => $items,
            'pages' => $pages,
            'areas' => []
        ]);
    }

    public function putUpdate($menu_id)
    {
        Menu::where('id', $menu_id)->update([
            'title' => $this->request->get('title'),
            'area' => $this->request->get('area'),
        ]);

        return $this->redirector->route('menu.update', [$menu_id]);
    }

    public function getUpdateItem()
    {
        $inputs = $this->request->only('action', 'info');
        $rules = [
            'action' => ['required', 'in:update,delete'],
            'info' => ['array']
        ];
        $validator = $this->validator->make($inputs, $rules);
        $response = [];

        if ($validator->fails()) {
            $response['success'] = 0;
            return $this->response->json($response);
        }

        extract($inputs);

        if ($inputs['action'] == "delete") {
            $this->db->table('menus_item')->delete($inputs['info']['id']);
        } elseif ($inputs['action'] == "update") {
            $id = $inputs['info']['id'];
            unset($inputs['info']['id']);
            $this->db->table('menus_item')
                ->where('id', $id)->update($inputs['info']);
        }

        $response['success'] = 1;
        return $this->response->json($response);
    }

    public function postMenuOrder($menu_id)
    {
        $source = e($this->request->get('source'));
        $destination = e($this->request->get('destination', 0));

        $this->db->table('menus_item')
            ->where('menu_id', $menu_id)
            ->where('id', $source)
            ->update(['parent_id' => $destination ? $destination : 0]);

        $ordering = json_decode($this->request->get('order'));
        $rootOrdering = json_decode($this->request->get('rootOrder'));

        if ($ordering) {
            foreach ($ordering as $order => $item_id) {
                $this->db->table('menus_item')
                    ->where('menu_id', $menu_id)
                    ->where('id', $item_id)
                    ->update(['weight' => $order]);
            }
        } else {
            foreach ($rootOrdering as $order => $item_id) {
                $this->db->table('menus_item')
                    ->where('menu_id', $menu_id)
                    ->where('id', $item_id)
                    ->update(['weight' => $order]);
            }
        }
    }

    public function putMenuItem($id)
    {
        $inputs = $this->request->only('title', 'url');

        $item = MainMenu::find($id);
        if ($inputs['title'])
            $item->title = $inputs['title'];
        if ($inputs['url'])
            $item->url = $inputs['url'];
        $item->save();
    }

    public function postAddItem($menu_id)
    {
        if (!is_null($this->request->get('page-id'))) {
            $page_id = $this->request->get('page-id');

            $max = MainMenu::max('weight');
            $post = Post::find($page_id);
            MainMenu::insert([
                'title' => $post->subject,
                'parent_id' => '0',
                'url' => '',
                'post_id' => $post->id,
                'menu_id' => $menu_id,
                'weight' => $max + 1,
            ]);

            return $this->redirector->route('menu.update', [$menu_id]);
        }

        $max = MainMenu::max('weight');
        MainMenu::insert([
            'title' => "عنوان",
            'parent_id' => '0',
            'url' => '',
            'menu_id' => $menu_id,
            'weight' => $max + 1,
        ]);

        return $this->redirector->route('menu.update', [$menu_id]);
    }

    public function deleteItem($id)
    {
        MainMenu::destroy($id);
    }

    public function getPlace()
    {
        $menus = $this->db->table('menus')->get();
        return $this->view->make("Menu::place", ["menus", $menus]);
    }
}