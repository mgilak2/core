<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Permissions extends Migration
{
    public function up()
    {
        Schema::create('permissions', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('components');
        });

        Schema::table('users', function ($table) {
            $table->string('roles_permissions');
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('roles_permissions');
        });
    }
}
