<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class Roles extends Migration
{
    public function up()
    {
        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name');
            $table->string('permissions');
        });

        Schema::table('users', function ($table) {
            $table->string('roles');
        });
    }

    public function down()
    {
        Schema::table('users', function ($table) {
            $table->dropColumn('roles');
        });

        Schema::dropTable("roles");
    }
}
