<?php

namespace UserManagement\Contracts;

interface PermissionGroupContract
{
    public function getPermissionGroup();

    public function setPermissions();

    public function getId();

    public function getName();
}