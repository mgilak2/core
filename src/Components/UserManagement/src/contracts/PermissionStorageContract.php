<?php

namespace UserManagement\Contracts;

interface PermissionStorageContract
{
    public function save();

    public function relateSubGroupsWithThisGroup();
}