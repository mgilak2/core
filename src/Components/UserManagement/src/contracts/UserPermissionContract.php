<?php

namespace UserManagement\Contracts;

interface UserPermissionContract
{
    public function hasAccess($name);

    public function getPermission();
}