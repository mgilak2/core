<?php

namespace UserManagement;

use Illuminate\Database\Eloquent\Model as Eloquent;

class PermissionModel extends Eloquent
{
    protected $primaryKey = "id";
    protected $table = "permissions";
    public $timestamps = false;
}
