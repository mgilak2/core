<?php

namespace UserManagement;

use Illuminate\Database\Eloquent\Model as Eloqeunt;

class Role extends Eloqeunt
{
    protected $primaryKey = "id";
    public $timestamps = false;

    public function users()
    {
        return $this->belongsToMany("UserManagement\\User");
    }
}
