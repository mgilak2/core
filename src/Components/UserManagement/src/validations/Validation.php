<?php namespace UserManagement;

use Validator;

class Validation
{
    public function validateLogin($inputs, $rules = array(), $messages = array())
    {
        if (count($rules) == 0) {
            $rules = array(
                'email' => 'required|email',
                "password" => 'required|min:6'
            );
        }

        if (count($messages) == 0) {
            $messages = [
                "email" => "",
                "password.required" => "",
            ];
        }

        return Validator::make($inputs, $rules, $messages);
    }

    public function validateRegister($inputs, $rules = array(), $messages = array())
    {
        if (count($rules) == 0) {
            $rules = array(
                'first_name' => 'required|min:3',
                'last_name' => 'required|min:3',
                'email' => 'required|email',
                "password" => 'required|min:6|confirmed',
                "password_confirmation" => 'required|min:6'
            );
        }

        if (count($messages) == 0) {
            $messages = [
                "email" => "",
                "password.required" => "",
            ];
        }

        return Validator::make($inputs, $rules, $messages);
    }

    public function validateReset($inputs, $rules = array(), $messages = array())
    {
        if (count($rules) == 0) {
            $rules = array(
                'email' => 'required|email',
                "password" => 'required|min:6'
            );
        }

        if (count($messages) == 0) {
            $messages = [
                "email" => "",
                "password.required" => "",
            ];
        }

        return Validator::make($inputs, $rules, $messages);
    }

    public function validateRemind($inputs, $rules = array(), $messages = array())
    {
        if (count($rules) == 0) {
            $rules = array(
                'email' => 'required|email',
                "password" => 'required|min:6'
            );
        }

        if (count($messages) == 0) {
            $messages = [
                "email" => "",
                "password.required" => "",
            ];
        }

        return Validator::make($inputs, $rules, $messages);
    }
}