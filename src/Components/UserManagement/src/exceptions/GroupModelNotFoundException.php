<?php

namespace UserManagement\Exceptions;

use Exception;

class GroupModelNotFoundException extends Exception
{
}
