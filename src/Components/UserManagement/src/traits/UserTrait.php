<?php namespace UserManagement\Traits;

use Illuminate\Support\Facades\App;
use UserManagement\Contracts\PermissionGroupContract;
use UserManagement\Exceptions\GroupModelNotFoundException;
use UserManagement\PermissionGroup;
use UserManagement\PermissionModel;

trait UserTrait
{
    /**
     * @var PermissionGroup
     */
    protected $permissionGroup = null;

    /**
     * tailored for eloquent
     * @param $name
     * @return bool
     */
    public function hasAccess($name)
    {
        return $this->getPermissionGroup()->has(new PermissionModel($name, 1));
    }

    /**
     * @return PermissionGroup
     */
    public function getPermissionGroup()
    {
        if (is_null($this->permissionGroup)) {
            $this->permissionGroup = new PermissionGroup();
            $this->setPermissions();
        }

        return $this->permissionGroup;
    }

    public function setPermissions()
    {
        $permissionFiledName = $this->getPermission();
        $permissions = json_decode($this->{$permissionFiledName});
        if ($permissions === null) {
            return $this->permissionGroup;
        }

        if (isset($permissions->permissions) && is_array($permissions->permissions)) {
            foreach ($permissions->permissions as $permission) {
                $this->permissionGroup->add(new PermissionModel($permission->name, $permission->status));
            }
        }

        if (isset($permissions->groups) && is_array($permissions->groups)) {
            $model = App::make($this->groupModel);
            if ($model instanceof PermissionGroupContract === false) {
                throw new GroupModelNotFoundException("you must define your Group model full class name in your User model with the help of protected property named groupModel :)");
            }
            foreach ($permissions->groups as $group) {
                $group = $model->find($group->id)->getPermissionGroup();
                $this->permissionGroup->addGroup($group);
            }
        }

        return $this;
    }
}