<?php namespace UserManagement\Traits;

use UserManagement\PermissionGroup;

trait StorageTrait
{
    /**
     * tailored for eloquent
     * @param PermissionGroup $group
     */
    public function save()
    {
    }

    public function relateSubGroupsWithThisGroup()
    {
    }
}