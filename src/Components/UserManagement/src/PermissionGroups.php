<?php

namespace UserManagement;

use UserManagement\Contracts\PermissionGroupContract;
use UserManagement\Contracts\UserPermissionContract;
use UserManagement\Traits\UtilsTrait;

class PermissionGroup
{
    use UtilsTrait;
    public $id;
    public $name;
    protected $is_baned = false;
    /**
     * collection of NoSchemaPermission\Permission
     * @var array
     */
    protected $permissions = [];

    /**
     * collection of NoSchemaPermission\PermissionGroup
     * @var array
     */
    protected $groups = [];

    public function __construct()
    {
        $this->id = $this->randomString();
    }

    public function add(Permission $permission)
    {
        $this->permissions[$permission->getPermissionForeignId()] = $permission;
    }

    public function remove(Permission $permission)
    {
        if (isset($this->permissions[$permission->getPermissionForeignId()]))
            unset($this->permissions[$permission->getPermissionForeignId()]);
    }

    public function addGroup(PermissionGroup $group)
    {
        $this->groups[$group->getId()] = $group;
    }

    public function getId()
    {
        return $this->id;
    }

    public function removeGroup(PermissionGroup $group)
    {
        if (isset($this->groups[$group->getId()]))
            unset($this->groups[$group->getId()]);
    }

    public function has(Permission $permission)
    {
        if ($this->findInPermissions($permission) && !$this->is_baned) return true;
        if ($this->findPermissionInGroups($permission) && !$this->is_baned) return true;
        return false;
    }

    public function findInPermissions(Permission $permission)
    {
        $this->is_baned = false;
        $simplified = $permission->simplifyPermission();
        foreach ($this->permissions as $stored_permission) {
            $stored_permission_simplified = $stored_permission->simplifyPermission();
            if ($simplified['name'] == $stored_permission_simplified['name']) {
                $this->is_baned = true;
                return true;
            }
        }

        return false;
    }

    protected function findPermissionInGroups(Permission $permission)
    {
        foreach ($this->groups as $group) {
            if ($group->findInPermissions($permission)) {
                return true;
            }
        }
        return false;
    }

    public function getAllPermissions()
    {
        return $this->permissions;
    }

    public function getAllGroups()
    {
        return $this->groups;
    }

    public function saveUser(UserPermissionContract $model)
    {
        $model->permissions = $this->intoJson();
        $model->save();
    }

    public function intoJson()
    {
        $std = new \stdClass();
        $std->permissions = [];
        foreach ($this->permissions as $permission) {
            $permissionStd = new \stdClass();
            $permissionStd->name = $permission->getName();
            $permissionStd->status = $permission->getStatus();
            $std->permissions[] = $permissionStd;
        }
        $std->groups = [];
        foreach ($this->groups as $group) {
            $groupStd = new \stdClass();
            $groupStd->name = $group->name;
            $groupStd->id = $group->id;
            $std->groups[] = $groupStd;
        }

        return json_encode($std);
    }

    public function saveGroup(PermissionGroupContract $model)
    {
        $model->permissions = $this->intoJson();
        $model->save();
    }

    public function assignUser(UserPermissionContract $model)
    {
        $model->permissions = $this->intoJson();
    }

    public function assignGroup(PermissionGroupContract $model)
    {
        $model->permissions = $this->intoJson();
    }
}