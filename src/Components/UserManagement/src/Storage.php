<?php

namespace UserManagement;

use UserManagement\Contracts\PermissionStorageContract;
use UserManagement\Traits\StorageTrait;

class Storage implements PermissionStorageContract
{
    use StorageTrait;
}