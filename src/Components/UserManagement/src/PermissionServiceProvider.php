<?php

namespace UserManagement;

use Illuminate\Contracts\Auth\Access\Gate as GateContract;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;

class PermissionServiceProvider extends ServiceProvider
    implements PhonemeComponent, Initializable, CanMigrate
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;
    /**
     * @var GateContract
     */
    private $gate;

    public function __construct(Application $app,
                                View $view,
                                GateContract $gate,
                                Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
        $this->gate = $gate;
    }

    public function register()
    {
        // noting to do :)
    }

    public function boot()
    {
        $this->defineRules();
        $this->defineRoutes();
        $this->viewsBaseFolder();
    }

    private function defineRules()
    {
        $this->gate->define('userManagement', function ($user, $post) {
            dd($user, $post);
        });
    }

    private function defineRoutes()
    {
        $this->router->model("role_id", "UserManagement\\Role");
        $this->router->group(["prefix" => "/dashboard/user", "namespace" => "UserManagement"], function () {
            $this->router->get('role', ['as' => 'role.index', 'uses' => 'RoleController@index']);

            $this->router->get('role/create', ['as' => 'role.create', 'uses' => 'RoleController@create']);
            $this->router->post('role/create', ['as' => 'role.create.post', 'uses' => 'RoleController@postCreate']);

            $this->router->get('role/update/{role_id}', ['as' => 'role.update', 'uses' => 'RoleController@update']);
            $this->router->post('role/update/{role_id}', ['as' => 'role.update.post', 'uses' => 'RoleController@postUpdate']);

            $this->router->get('role/delete/{role_id}', ['as' => 'role.delete', 'uses' => 'RoleController@delete']);

            $this->router->get('permission/', ['as' => 'permission.index', 'uses' => 'PermissionController@index']);

            $this->router->model("user_id", "UserManagement\\UserModel");
            $this->router->get("/", ['as' => 'user.index', 'uses' => 'UserController@index']);

            $this->router->get("/create", ['as' => 'user.create', 'uses' => 'UserController@create']);
            $this->router->post("/create", ['as' => 'user.create.post', 'uses' => 'UserController@postCreate']);

            $this->router->get("/update/{user_id}", ['as' => 'user.update', 'uses' => 'UserController@update']);
            $this->router->post("/update/{user_id}", ['as' => 'user.update.post', 'uses' => 'UserController@postUpdate']);

            $this->router->get("/delete/{user_id}", ['as' => 'user.delete', 'uses' => 'UserController@delete']);
        });

        $this->router->group(["namespace" => "UserManagement"], function () {
            $this->router->get("/login", ['as' => 'login', 'uses' => 'UserController@login']);
            $this->router->post("/login", ['as' => 'login.post', 'uses' => 'UserController@postLogin']);

            $this->router->get("/logout", ['as' => 'logout', 'uses' => 'UserController@logout']);

            $this->router->get("/register", ['as' => 'register', 'uses' => 'UserController@register']);
            $this->router->post("/register", ['as' => 'register.post', 'uses' => 'UserController@postRegister']);
        });
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('UserManagement', __DIR__ . "/../views");
    }

    public function getMigrationArguments()
    {
        return [];
    }
}