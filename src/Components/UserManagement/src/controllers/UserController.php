<?php

namespace UserManagement;

use AdminTheme\AdminTheme;
use Illuminate\Auth\AuthManager;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\Factory as Validator;
use Phoneme\Http\Controllers\Controller;
use Phoneme\User;

class UserController extends Controller
{
    /**
     * @var Validator
     */
    protected $validator;
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var AuthManager
     */
    private $auth;
    /**
     * @var Hasher
     */
    private $hasher;
    /**
     * @var User
     */
    private $user;

    public function __construct(Validator $validator, Redirector $redirector,
                                Request $request, AdminTheme $theme,
                                AuthManager $auth, Hasher $hasher,User $user)
    {
        $this->validator = $validator;
        $this->redirector = $redirector;
        $this->request = $request;
        $this->theme = $theme;
        $this->auth = $auth;
        $this->hasher = $hasher;
        $this->user = $user;
    }

    public function index()
    {
        $users = $this->user->where("id", '>', 1)->get();

        return $this->theme
            ->baseDashboard('UserManagement::user.index', ["users" => $users]);
    }

    public function create()
    {
        return $this->theme->baseDashboard('UserManagement::user.create',
            ['roles' => Role::all(),
                'permissions' => PermissionModel::all(),
                "rolesArray" => [],
                "permissionsArray" => []]);
    }

    public function postCreate()
    {
        $inputs = $this->request->only('email', 'password', 'first_name',
            'last_name', 'permissions', 'roles');

        $validator = $this->validatePostCreateRequest($inputs);
        if ($validator->fails()) {
            return $this->redirector->route('user.create')
                ->withErrors($validator)
                ->withInput($inputs);
        }

        $user = new User;
        $user->saveme($this->request->all());

        return $this->redirector->route('user.index');
    }

    private function validatePostCreateRequest($inputs)
    {
        $rules = array(
            'email' => ['required', 'email'],
            'password' => ['required', 'min:6'],
            'first_name' => ['required', 'alpha'],
            'last_name' => ['required', 'alpha'],
            'permissions' => ['array'],
            'roles' => ['array']
        );
        $validator = $this->validator->make($inputs, $rules);

        return $validator;
    }

    public function update(User $user)
    {
        if ($user->id != 1) {
            $roles = Role::all();
            $permissions = PermissionModel::all();

            $rolesArray = unserialize($user->roles);
            if (!is_array($rolesArray)) {
                $rolesArray = [];
            }

            $permissionsArray = unserialize($user->permissions);
            if (!is_array($permissionsArray)) {
                $permissionsArray = [];
            }

            return $this->theme->baseDashboard('UserManagement::user.create')
                ->with("User", $user)
                ->with('roles', $roles)
                ->with('permissions', $permissions)
                ->with("rolesArray", $rolesArray)
                ->with("permissionsArray", $permissionsArray);
        }
    }

    public function postUpdate(User $user)
    {
        if ($user->id == 1) {
            return $this->redirector->route("user.index");
        }

        $inputs = $this->request->only('email', 'password', 'first_name', 'last_name', 'permissions', 'groups');

        $rules = array(
            'email' => array('email'),
            'first_name' => array('required', 'alpha'),
            'last_name' => array('required', 'alpha'),
            'permissions' => array('array'),
            'groups' => array('array')
        );

        $validator = $this->validator->make($inputs, $rules);

        if ($validator->fails()) {
            return $this->redirector->route('user.update', [$user->id])
                ->withErrors($validator)
                ->withInput($inputs);
        }

        $user->updateme($this->request->all());

        if (strlen($inputs['password']) >= 6) {
            $user->password = $this->hasher->make($inputs["password"]);
            $user->save();
        }


        return $this->redirector->route('user.index');
    }

    public function delete(User $user)
    {
        if ($user->id != 1) {
            $user->delete();

            return $this->redirector->route('user.index');
        }
    }

    public function login()
    {
        if ($this->auth->check() && $this->auth->user()->can(['Dashboard'])) {
            return $this->redirector->route("dashboard");
        }

        if ($this->auth->check() && !$this->auth->user()->can(['Dashboard'])) {
            return $this->redirector->route("home");
        }

        $url = $this->request->get("url");

        return $this->theme->baseDashboard("Admin::login")->with("url", $url);
    }

    public function postLogin()
    {
        if ($this->auth->attempt($this->request->only('email', 'password'))) {
            $url = $this->request->get("url");
            if ($url == null) {
                return $this->redirector->route("dashboard");
            }

            return $this->redirector->to($this->request->get("url"));
        }

        return $this->redirector->route('home');

    }

    public function register()
    {
        return $this->theme->baseDashboard("Admin::register");
    }

    public function logout()
    {
        $this->auth->logout();

        return $this->redirector->back();
    }
}