<?php

namespace UserManagement;

use Illuminate\Foundation\Auth\AuthenticatesAndRegistersUsers;
use Illuminate\Foundation\Auth\ThrottlesLogins;
use Illuminate\Validation\Factory as Validator;
use Phoneme\Http\Controllers\Controller;
use Phoneme\User;

class AuthController extends Controller
{
    use AuthenticatesAndRegistersUsers, ThrottlesLogins;

    /**
     * @var User
     */
    private $user;
    /**
     * @var Validator
     */
    private $validator;

    public function __construct(User $user, Validator $validator)
    {
        $this->middleware('guest', ['except' => 'getLogout']);
        $this->user = $user;
        $this->validator = $validator;
    }

    protected function validator(array $data)
    {
        return $this->validator->make($data, [
            'name' => 'required|max:255',
            'email' => 'required|email|max:255|unique:users',
            'password' => 'required|confirmed|min:6',
        ]);
    }

    protected function create(array $data)
    {
        return $this->user->create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => bcrypt($data['password']),
        ]);
    }
}
