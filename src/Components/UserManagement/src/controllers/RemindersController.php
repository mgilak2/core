<?php

namespace UserManagement;

use Illuminate\Auth\Passwords\PasswordBroker;
use Illuminate\Contracts\Hashing\Hasher;
use Illuminate\Foundation\Application;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Translation\Translator;
use Illuminate\View\Factory as View;
use Phoneme\Http\Controllers\Controller;

class RemindersController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var View
     */
    private $view;
    /**
     * @var PasswordBroker
     */
    private $password;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Translator
     */
    private $translator;
    /**
     * @var Hasher
     */
    private $hasher;
    /**
     * @var Application
     */
    private $application;

    public function __construct(Redirector $redirector, View $view,
                                PasswordBroker $password, Request $request,
                                Translator $translator, Hasher $hasher, Application $application)
    {
        $this->redirector = $redirector;
        $this->view = $view;
        $this->password = $password;
        $this->request = $request;
        $this->translator = $translator;
        $this->hasher = $hasher;
        $this->application = $application;
    }

    public function getRemind()
    {
        return $this->view->make('password.remind');
    }

    public function postRemind()
    {
        switch ($response = $this->password->remind($this->request->only('email'))) {
            case $this->password->INVALID_USER:
                return $this->redirector->back()->with('error', $this->translator->get($response));

            case $this->password->REMINDER_SENT:
                return $this->redirector->back()->with('status', $this->translator->get($response));
        }
    }

    public function getReset($token = null)
    {
        if (is_null($token)) $this->application->abort(404);

        return $this->view->make('UserManagement::password.reset')->with('token', $token);
    }

    public function postReset()
    {
        $credentials = $this->request->only(
            'email', 'password', 'password_confirmation', 'token'
        );

        $response = $this->password->reset($credentials, function ($user, $password) {
            $user->password = $this->hasher->make($password);

            $user->save();
        });

        switch ($response) {
            case $this->password->INVALID_PASSWORD:
            case $this->password->INVALID_TOKEN:
            case $this->password->INVALID_USER:
                return $this->redirector->back()->with('error', $this->translator->get($response));

            case $this->password->PASSWORD_RESET:
                return $this->redirector->to('/');
        }
    }
}
