<?php

namespace UserManagement;

use AdminTheme\AdminTheme;
use Illuminate\Http\Request;
use Illuminate\Routing\Redirector;
use Illuminate\Validation\Factory as Validator;
use Phoneme\Http\Controllers\Controller;

class RoleController extends Controller
{
    /**
     * @var Redirector
     */
    private $redirector;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Validator
     */
    private $validator;
    /**
     * @var $this ->theme->baseDashboardTheme
     */
    private $theme;

    public function __construct(Redirector $redirector, Request $request,
                                Validator $validator, AdminTheme $theme)
    {
        $this->redirector = $redirector;
        $this->request = $request;
        $this->validator = $validator;
        $this->theme = $theme;
    }

    public function index(Role $roll)
    {
        $roles = $roll->all();

        return $this->theme->baseDashboard("UserManagement::role.index")
            ->with("roles", $roles);
    }

    public function create(PermissionModel $permission)
    {
        return $this->theme->baseDashboard('UserManagement::role.create')
            ->with('permissions', $permission->all())
            ->with("permissionsArray", []);
    }

    public function postCreate()
    {
        $inputs = $this->request->only('name', 'permissions');

        $rules = [
            'name' => 'required',
            'permissions' => 'array'
        ];

        $validator = $this->validator->make($inputs, $rules);

        if ($validator->fails()) {
            return $this->redirector->route('role.create')
                ->withErrors($validator)
                ->withInput($inputs);
        }

        $role = new Role;
        $role->name = $this->request->get("name");
        $role->permissions = serialize($this->request->get("permissions"));
        $role->save();

        return $this->redirector->route('role.index');
    }

    public function update(Role $role)
    {
        $permissions = PermissionModel::all();

        $permissionsArray = unserialize($role->permissions);
        if (!is_array($permissionsArray)) {
            $permissionsArray = [];
        }

        return $this->theme->baseDashboard('UserManagement::role.create')
            ->with('role', $role)
            ->with('permissions', $permissions)
            ->with('permissionsArray', $permissionsArray);
    }

    public function postUpdate(Role $role)
    {
        $inputs = $this->request->only('name', 'permissions');

        $rules = array(
            'name' => 'required',
            'permissions' => 'array'
        );

        $validator = $this->validator->make($inputs, $rules);

        if ($validator->fails()) {
            return $this->redirector->route('role.update', $role->id)
                ->withErrors($validator)
                ->withInput($inputs);
        }

        $role->name = $this->request->get("name");
        $role->permissions = serialize($this->request->get("permissions"));
        $role->save();

        return $this->redirector->route('role.index');
    }

    public function delete(Role $id)
    {
        return $this->redirector->route('permission.index');
    }

}
