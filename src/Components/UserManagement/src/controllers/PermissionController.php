<?php

namespace UserManagement;

use AdminTheme\AdminTheme;
use Phoneme\Http\Controllers\Controller;

class PermissionController extends Controller
{
    /**
     * @var AdminTheme
     */
    private $theme;
    /**
     * @var PermissionModel
     */
    private $permissions;

    public function __construct(AdminTheme $theme, PermissionModel $permissions)
    {
        $this->theme = $theme;
        $this->permissions = $permissions;
    }

    public function index()
    {
        $permissions = $this->permissions->all();

        return $this->theme
            ->baseDashboard("UserManagement::permission.index",
                ["permissions" => $permissions]);
    }
}
