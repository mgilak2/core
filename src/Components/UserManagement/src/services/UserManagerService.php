<?php namespace UserManagement;

use Sentry;

class UserManagementService
{
    protected $repo = null;

    public function add($en, $fa, $module)
    {
        $inputs = [
            'fa' => $fa,
            'en' => $en,
            'module' => $module,
        ];

        \DB::table('permissions')
            ->insert($inputs);
    }

    public function remove($module)
    {
        \DB::table('permissions')
            ->where('module', $module)
            ->delete();
    }

    public function repo()
    {
        if (is_null($this->repo)) {
            $this->repo = \App::make("UserManagement\\User");
        }

        return $this->repo;
    }

    public function db()
    {
        return \DB::table("cms_users");
    }
}