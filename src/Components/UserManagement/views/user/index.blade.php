@extends("Admin::dashboard")

@section("pageTitle")
    <i class="fa fa-list"></i>
    لیست کاربران

    <a href="<?= route('user.create') ?>" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>
@endsection

@section('content')


    <table class="table table-striped">
        <colgroup>
            <col style="width: 50px"/>
            <col/>
            <col/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>نام</th>
            <th>نام خانوادگی</th>
            <th>#</th>
        </tr>
        </thead>
        <tbody>

        @foreach($users as $index => $user)
            <tr>
                <td>{{++$index}}</td>
                <td>{{ $user->first_name }}</td>
                <td>{{ $user->last_name }}</td>
                <td>
                    <a href="{{ route('user.update', $user->id) }}" class="btn btn-primary btn-sm">
                        <i class="icon-pencil"></i>
                    </a>
                    <a href="{{ route('user.delete', $user->id) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')">
                        <i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop