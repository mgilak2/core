@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن کابر

    <a href="{{ route('user.index') }}" class="btn btn-primary pull-left">
        <i class="fa fa-list"></i>
        لیست کاربران
    </a>
@endsection

@section('content')

    <?php
    $message = "";
    if ($errors->has()) {
        $message .= '<ul id="message">';
        foreach ($errors->all('<li class="error">:message</li>') as $m) {
            $message .= $m;
        }
        $message .= '</ul>';
        echo $message;
    }
    ?>

    @if(isset($User))
        {!! Form::model($User,array('route' => ['user.update.post',$User->id])) !!}
    @else
        {!! Form::open(array('route' => 'user.create.post')) !!}
    @endif

    <div class="row">
        <div class="col-sm-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h5>مشخصات حساب کاربری</h5>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="form-group col-sm-6">
                            {!! Form::label('first_name', 'نام',array("class"=>"control-label")) !!}
                            {!! Form::text('first_name', null,array("class"=>"form-control")) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('last_name', 'نام خانوادگی',array("class"=>"control-label")) !!}
                            {!! Form::text('last_name', null,array("class"=>"form-control")) !!}
                        </div>
                    </div>
                    <div class="row">
                        <div class="form-group col-sm-6">
                            {!! Form::label('email', 'ایمیل',array("class"=>"control-label")) !!}
                            {!! Form::text('email', null,array("class"=>"form-control")) !!}
                        </div>
                        <div class="form-group col-sm-6">
                            {!! Form::label('password', 'رمز عبور',array("class"=>"control-label")) !!}
                            {!! Form::password('password', array("class"=>"form-control")) !!}
                        </div>
                    </div>

                    <div class="row"></div>
                </div>
            </div>
            <div class="panel panel-success">
                <div class="panel-heading"><h5>لیست گروه ها</h5></div>
                <div class="panel-body">
                    <ul class="user-permission-list">
                        @foreach($roles as $role)
                            <li>
                                <div class="checkbox">
                                    <label>
                                        {!! Form::checkbox("roles[$role->name]",$role->id,array_key_exists($role->name,$rolesArray)) !!}{{ $role->name }}
                                    </label>
                                </div>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
        <div class="col-sm-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h5>لیست مجوز ها</h5>
                </div>
                <div class="panel-body">
                    <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                        <div class="panel panel-warning">
                            <div class="panel-heading" role="tab" id="headingOne">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                       aria-expanded="true" aria-controls="collapseOne">
                                        لیست مجوز ها
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse in" role="tabpanel"
                                 aria-labelledby="headingOne">
                                <div class="panel-body">
                                    <ul class="user-permission-list">
                                        @foreach($permissions as $permission)
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        {!! Form::checkbox("permissions[$permission->en]", $permission->id,array_key_exists($permission->en,$permissionsArray)) !!} {{$permission->fa}}
                                                    </label>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="row">
        <div class="col-sm-2">
            {!! Form::submit('ایجاد',array("class"=>"btn btn-primary btn-block")) !!}
        </div>
    </div>

    {!! Form::close() !!}
@stop