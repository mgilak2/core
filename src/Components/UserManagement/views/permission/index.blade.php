@extends("Admin::dashboard")

@section("pageTitle")
    <i class="fa fa-list"></i>
    لیست مجوزها
@endsection

@section('content')
    <table class="table table-striped">
        <colgroup>
            <col style="width: 80px"/>
            <col/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        @foreach($permissions as $index => $perm)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $perm->fa }}</td>
            </tr>
        @endforeach
        </tbody>
    </table>

@stop