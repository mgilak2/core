@extends("Admin::dashboard")

@section("pageTitle")
    <i class="fa fa-list"></i>
    لیست گروه ها

    <a href="{{ route('role.create') }}" class="btn btn-danger pull-left">
        <i class="icon-plus"></i>
        جدید
    </a>
@endsection

@section('content')
    <table class="table table-striped">
        <colgroup>
            <col style="width: 80px"/>
            <col/>
            <col/>
            <col style="width: 90px"/>
        </colgroup>
        <thead>
        <tr>
            <th>ردیف</th>
            <th>عنوان</th>
            <th></th>
        </tr>
        </thead>
        <tbody>

        <?php $i = 0; ?>
        @foreach($roles as $index => $role)
            <tr>
                <td>{{ ++$index }}</td>
                <td>{{ $role->name }}</td>
                <td>
                    <a href="{{ route('role.update' , $role->id) }}" class="btn btn-primary btn-sm">
                        <i class="icon-pencil"></i>
                    </a>
                    <a href="{{ route('role.delete' , $role->id) }}" class="btn btn-danger btn-sm"
                       onclick="return confirm('آیا مایلید گزینه مورد نظر را حذف نمایید؟')">
                        <i class="icon-trash"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
@stop