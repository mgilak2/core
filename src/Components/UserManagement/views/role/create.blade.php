@extends("Admin::dashboard")

@section("pageTitle")
    <i class="icon-plus"></i>
    افزودن مجوز جدید
@endsection

@section('content')
    <?php
    $message = "";
    if ($errors->has()) {
        $message .= '<ul id="message">';
        foreach ($errors->all('<li class="error">:message</li>') as $m) {
            $message .= $m;
        }
        $message .= '</ul>';
        echo $message;
    }
    ?>

    @if(isset($role))
        {!! Form::model($role,['route'=>['role.update.post',$role->id]]) !!}
    @else
        {!! Form::open(['route'=>'role.create']) !!}
    @endif

    <div class="row">
        <div class="col-sm-3 form-group">
            {!! Form::label('name', 'نام رول') !!}
            {!! Form::text('name',Input::get("name"),array('class'=>'form-control')) !!}
        </div>
    </div>

    <h4>لیست مجوز ها</h4>
    <hr>
    <div class="row">
        <div class="col-sm-12 form-group">
            <div class="checkbox checkbox-inline"></div>
            @foreach($permissions as $permission)
                <div class="checkbox checkbox-inline">
                    <label>
                        {!! Form::checkbox("permissions[$permission->en]", $permission->id,array_key_exists($permission->en,$permissionsArray)) !!}
                        {{ $permission->fa }}
                    </label>
                </div>
            @endforeach
        </div>
    </div>
    <div class="row">
        <div class="col-sm-2">
            {!! Form::submit('ایجاد', array('class' => 'btn btn-primary btn-block')) !!}
        </div>
    </div>
    {!! Form::close() !!}
@stop