<?php

namespace Settings;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Contracts\CanSeed;
use Phoneme\Contracts\Composerable;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeComponent;
use SettingsDatabaseSeeder;
use Symfony\Component\Process\Process;

class SettingsServiceProvider extends ServiceProvider
    implements PhonemeComponent, Initializable,
    CanMigrate, CanSeed, Composerable
{
    /**
     * @var Router
     */
    private $router;
    /**
     * @var View
     */
    private $view;

    public function __construct(Application $app, Router $router, View $view)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->router = $router;
        $this->view = $view;
    }

    public function register()
    {
        // nothing to do :)
    }

    public function boot()
    {
        $this->viewsBaseFolder();
        $this->defineRoutes();
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Setting', __DIR__ . "/../views");
    }

    private function defineRoutes()
    {
        $this->router->group(['prefix' => '/dashboard/setting', 'namespace' => 'Settings'], function () {
            $this->router->get("/", ['as' => 'setting.index', 'uses' => 'SettingsController@index']);
            $this->router->post("/", ['as' => 'setting.post', 'uses' => 'SettingsController@postSetting']);
        });
    }

    public function getMigrationArguments()
    {
        return ["--path" => "app/Components/Settings/database/migrations"];
    }

    public function getSeedArguments()
    {
        return ['--class' => SettingsDatabaseSeeder::class];
    }

    public function getComposerCommand()
    {
        return new Process("cd Settings
        && composer dump-autoload -o
        && composer update");
    }
}