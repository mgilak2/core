<?php namespace Captcha;

use Captcha\Core\Captcha;
use Captcha\Core\Headers;
use Captcha\Core\Image;
use Captcha\Core\Util;
use Phoneme\Http\Requests\Request;
use Symfony\Component\HttpFoundation\Session\Session;

class CaptchaService
{
    private $conf;
    /**
     * @var Request
     */
    private $request;
    /**
     * @var Session
     */
    private $session;

    public function __construct(Request $request, Session $session)
    {
        $this->request = $request;
        $this->session = $session;
    }

    public function check()
    {
        if (strtolower($this->request->get("captcha")) != strtolower($this->session->get("captcha")))
            return true;
        else
            return false;
    }

    public function captcha()
    {
        (new Captcha(new Image(), new Headers(), new Util()))->create();
    }
}
