<?php namespace Captcha\Core;


use Captcha\Interfaces\iHeaders;

class Headers implements iHeaders
{
    public $conf = array(
        "headers" => true
    );

    /**
     * sets many headers for images
     *
     */
    public function setHeaders()
    {
        header("Expires: Mon, 26 Jul 1997 05:00:00 GMT");
        header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
        header("Cache-Control: no-store, no-cache, must-revalidate");
        header("Cache-Control: post-check=0, pre-check=0", false);
        header("Pragma: no-cache");

        $this->setPngOrJpegHeader();
    }

    /**
     * sets header for png or jpeg
     *
     */
    public function setPngOrJpegHeader()
    {
        if ($this->conf['headers'])
            header('Content-Type: image/png');
        else
            header('Content-Type: image/jpeg');
    }

}