<?php namespace Captcha\Core;


use Captcha\Interfaces\iImage;

class Image implements iImage
{

    /**
     *
     * configuration for default needs of create function
     *
     * @param array config
     */
    public $conf = array(
        "font" => "arial.ttf",
        "captchaSize" => array("w" => 140, "h" => 50),
        "bgColor" => array("r" => 0, "g" => 0, "b" => 0),
        "fontColor" => array("r" => 255, "g" => 255, "b" => 255),
        "fontPosition" => array(25, 2, 6, 40)
    );

    /**
     * creates a img recources
     *
     * @return this
     *
     */
    public function createImage()
    {
        $img = @imagecreate(
            $this->conf['captchaSize']['w'],
            $this->conf['captchaSize']['h']
        ) or die("couldnt initial g2 stream");

        $bgColor = imagecolorallocate(
            $img,
            $this->conf['bgColor']['r'],
            $this->conf['bgColor']['b'],
            $this->conf['bgColor']['g']
        );

        $fontColor = imagecolorallocate(
            $img,
            $this->conf['fontColor']['r'],
            $this->conf['fontColor']['b'],
            $this->conf['fontColor']['g']
        );

        imagettftext(
            $img,
            $this->conf['fontPosition'][0],
            $this->conf['fontPosition'][1],
            $this->conf['fontPosition'][2],
            $this->conf['fontPosition'][3],
            $fontColor,
            __DIR__ . "/../Fonts/" . $this->conf['font'],
            $this->conf['string']
        );

        $this->img = $img;

        return $this;
    }

    /**
     * makes image source into png format
     *
     */
    public function intoPng()
    {
        imagepng($this->img, NULL, 0);
        imagedestroy($this->img);
    }

    public function intoJpeg()
    {
        imagejpeg($this->img, NULL, 0);
        imagedestroy($this->img);
    }

    public function setCaptchaSize(array $size)
    {
        $this->conf['w'] = $size['w'];
        $this->conf['h'] = $size['h'];
    }

    public function setBgColor(array $bgColor)
    {
        $this->conf['bgColor']['r'] = $bgColor['r'];
        $this->conf['bgColor']['b'] = $bgColor['b'];
        $this->conf['bgColor']['g'] = $bgColor['g'];
    }

    public function setFontColor(array $color)
    {
        $this->conf['fontColor']['r'] = $color['r'];
        $this->conf['fontColor']['b'] = $color['b'];
        $this->conf['fontColor']['g'] = $color['g'];
    }

    public function setImagePosition(array $position)
    {
        $this->conf['fontPosition'] = $position;
    }
}