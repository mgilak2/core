<?php namespace Captcha\Core;

use Captcha\Interfaces\iHeaders;
use Captcha\Interfaces\iImage;
use Captcha\Interfaces\iUtil;

class Captcha
{

    /**
     * holds captcha code - private
     *
     * @param string $code
     */
    private $code = "";

    /**
     *
     * configuration for default needs of create function
     *
     * @param array config
     */
    private $conf = array();

    public function __construct(iImage $Image, iHeaders $Headers, iUtil $Util)
    {
        $this->Image = $Image;
        $this->Headers = $Headers;
        $this->Util = $Util;
        $this->services = array(
            "util" => $this->Util,
            "image" => $this->Image,
            "headers" => $this->Headers
        );
        $this->mergeConfs();
    }

    private function mergeConfs()
    {
        foreach ($this->services as $service => $conf)
            foreach ($conf->conf as $option => $value)
                $this->conf[$option] = $value;

        foreach ($this->services as $service => $conf)
            $conf->conf = $this->conf;
    }

    /**
     * this function merge the $this->conf with the given $conf
     */
    public function setConf(array $conf)
    {
        $this->conf = array_merge($this->conf, $conf);
        $this->mergeConfs();
    }

    /**
     * returns code
     *
     * @return string code
     */
    public function code()
    {
        return $this->code;
    }

    /**
     * handle the whole process
     *
     */
    public function create()
    {
        $this->Headers->setHeaders();
        $this->setCode();
        $this->Image->createImage()->intoPng();
        $this->Util->setSession();
    }

    /**
     * creates a random text from Util
     * then asinges it to the $this->code
     * and $this->conf['string']
     */
    private function setCode()
    {
        $this->code = $this->Util->randomText();
        $this->Image->conf['string'] = $this->code;
        $this->Util->conf['string'] = $this->code;
        $this->conf['string'] = $this->code;
    }
}






