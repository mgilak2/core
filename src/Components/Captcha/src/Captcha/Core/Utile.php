<?php namespace Captcha\Core;

use Captcha\Interfaces\iUtil;
use Session;

class Util implements iUtil
{

    public $conf = array(

        "length" => 6,
        "captcha" => "captcha"

    );

    /**
     * creates a random text
     *
     * @return string
     *
     */
    public function randomText()
    {
        $string = "";
        $letters = array(
            "a", "b", "c", "d", "e", "f", "g", "h", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z", "A", "B", "C", "D", "E", "F", "G", "H", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
            "1", "2", "3", "4", "5", "6", "7", "8", "9", "0"
        );
        for ($i = 0; $i < $this->conf['length']; $i++)
            $string .= $letters[rand(0, count($letters) - 1)];

        return $string;
    }

    /**
     * this function put the code into session
     */
    public function setSession()
    {
        Session::put($this->conf['captcha'], $this->conf['string']);
    }

    /**
     * you may have some other session index named 'captcha'
     * for escaping conflict this function can rename the default
     * setup for captcha .
     *
     */
    public function changeCaptchaName($name)
    {
        $this->conf['captcha'] = $name;
    }
}