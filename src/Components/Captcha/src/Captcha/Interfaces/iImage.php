<?php namespace Captcha\Interfaces;

interface iImage
{

    public function createImage();

    public function intoPng();

    public function intoJpeg();

}