<?php namespace Captcha\Interfaces;

interface iHeaders
{
    public function setHeaders();

    public function setPngOrJpegHeader();
}