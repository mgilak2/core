<?php namespace Captcha\Interfaces;

interface iCrossUsage
{
    public static function check();

    public static function captcha();
}
