<?php namespace Captcha;

use Illuminate\Foundation\AliasLoader;
use Illuminate\Support\ServiceProvider;

class CaptchaServiceProvider extends ServiceProvider
{
    public function register()
    {
        $this->app->bind('Captcha', function ($app) {
            return new CaptchaService($app['request'], $app['session']);
        });
    }

    public function boot()
    {
        $this->package("gilak/captcha");
        AliasLoader::getInstance()->alias('Captcha', 'Gilak\Captcha\Captcha');
    }
}
