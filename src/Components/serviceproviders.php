<?php

use Backup\BackUpServiceProvider;
use Captcha\CaptchaServiceProvider;
use Comments\CommentsServiceProvider;
use Contact\ContactServiceProvider;
use Dashboard\DashboardServiceProvider;
use FileManager\FileManagerServiceProvider;
use Gallery\GalleryServiceProvider;
use Mailer\MailerServiceProvider;
use Menus\MenusServiceProvider;
use NewsLetter\NewsLetterServiceProvider;
use Notify\NotifyServiceProvider;
use OverallStatus\OverallStatusServiceProvider;
use Polls\PollsServiceProvider;
use Post\PostServiceProvider;
use Rates\RatesServiceProvider;
use Settings\SettingsServiceProvider;
use Sidebars\SidebarsServiceProvider;
use Sliders\SliderServiceProvider;
use Subscribe\SubscribeServiceProvider;
use UserManagement\PermissionServiceProvider;
use Widget\WidgetServiceProvider;

return [
    "Backup" => ['serviceProvider' => BackUpServiceProvider::class,
        "autoLoader" => __DIR__ . "/Backup/vendor/autoload.php"],

    "Captcha" => ['serviceProvider' => CaptchaServiceProvider::class,
        "autoLoader" => __DIR__ . "/Captcha/vendor/autoload.php"],

    "Comments" => ['serviceProvider' => CommentsServiceProvider::class,
        "autoLoader" => __DIR__ . "/Comments/vendor/autoload.php"],

    "Contact" => ['serviceProvider' => ContactServiceProvider::class,
        "autoLoader" => __DIR__ . "/Contact/vendor/autoload.php"],

    "Dashboard" => ['serviceProvider' => DashboardServiceProvider::class,
        "autoLoader" => __DIR__ . "/Dashboard/vendor/autoload.php", "init" => true],

    "FileManager" => ['serviceProvider' => FileManagerServiceProvider::class,
        "autoLoader" => __DIR__ . "/FileManager/vendor/autoload.php"],

    "Gallery" => ['serviceProvider' => GalleryServiceProvider::class,
        "autoLoader" => __DIR__ . "/Gallery/vendor/autoload.php"],

    "Mailer" => ['serviceProvider' => MailerServiceProvider::class,
        "autoLoader" => __DIR__ . "/Mailer/vendor/autoload.php"],

    "Menus" => ['serviceProvider' => MenusServiceProvider::class,
        "autoLoader" => __DIR__ . "/Menus/vendor/autoload.php"],

    "NewsLetter" => ['serviceProvider' => NewsLetterServiceProvider::class,
        "autoLoader" => __DIR__ . "/NewsLetter/vendor/autoload.php"],

    "Notify" => ['serviceProvider' => NotifyServiceProvider::class,
        "autoLoader" => __DIR__ . "/Notify/vendor/autoload.php"],

    "OverallStatus" => ['serviceProvider' => OverallStatusServiceProvider::class,
        "autoLoader" => __DIR__ . "/OverallStatus/vendor/autoload.php"],

    "Polls" => ['serviceProvider' => PollsServiceProvider::class,
        "autoLoader" => __DIR__ . "/Polls/vendor/autoload.php"],

    "Post" => ['serviceProvider' => PostServiceProvider::class,
        "autoLoader" => __DIR__ . "/Post/vendor/autoload.php"],

    "Rates" => ['serviceProvider' => RatesServiceProvider::class,
        "autoLoader" => __DIR__ . "/Rates/vendor/autoload.php"],

    "Settings" => ['serviceProvider' => SettingsServiceProvider::class,
        "autoLoader" => __DIR__ . "/Settings/vendor/autoload.php", "init" => true],

    "Sidebars" => ['serviceProvider' => SidebarsServiceProvider::class,
        "autoLoader" => __DIR__ . "/Sidebars/vendor/autoload.php"],

    "Sliders" => ['serviceProvider' => SliderServiceProvider::class,
        "autoLoader" => __DIR__ . "/Sliders/vendor/autoload.php"],

    "Subscribe" => ['serviceProvider' => SubscribeServiceProvider::class,
        "autoLoader" => __DIR__ . "/Subscribe/vendor/autoload.php"],

    "UserManagement" => ['serviceProvider' => PermissionServiceProvider::class,
        "autoLoader" => __DIR__ . "/UserManagement/vendor/autoload.php"],

    "Widget" => ['serviceProvider' => WidgetServiceProvider::class,
        "autoLoader" => __DIR__ . "/Widget/vendor/autoload.php"],
];