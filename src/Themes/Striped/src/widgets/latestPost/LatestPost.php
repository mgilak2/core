<?php

namespace Striped;

use Post\Post;

class LatestPost
{
    public $name = "latest post";
    public $id = LatestPost::class;


    public function form($idstring = "")
    {
        if ($idstring != "")
            $options = phoneme_unserialize((widgetOptions($idstring)->options));

        include __DIR__ . "/form.php";
    }

    public function display($idstring = "")
    {
        if ($idstring != "") {
            $options = phoneme_unserialize((widgetOptions($idstring)->options));
        }

        $post_numbers = $options['sometext'];

        $posts = Post::orderBy("id", "desc")
            ->where('type', 1)->take($post_numbers)->get();

        include __DIR__ . "/display.php";
    }

    public function routes()
    {
        Route::post("/text-widget", function () {
            die($_POST);
        });
    }
}