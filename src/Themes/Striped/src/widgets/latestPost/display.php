<nav id="nav">
    <ul>
        <li class="current"><a href="#">Latest Post</a></li>
        <?php foreach($posts as $post):?>
            <li><a href="<?= $post->url(); ?>"><?= $post->subject ?></a></li>
        <?php endforeach; ?>
    </ul>
</nav>