<?php

namespace Striped;

class TextWidget
{
    public $id = TextWidget::class;
    public $name = "Text";

    public function form($idstring = "")
    {
        if ($idstring != "")
            $options = phoneme_unserialize(widgetOptions($idstring)->options);

        include __DIR__ . "/form.php";
    }

    public function display($idstring = "")
    {
        if ($idstring != "")
            $options = phoneme_unserialize((widgetOptions($idstring)
                ->options));

        include __DIR__ . "/display.php";
    }

    public function routes()
    {
        Route::post("/text-widget", function () {
            die($_POST);
        });
    }
}