<?php

namespace Striped;

use Illuminate\Contracts\Foundation\Application;
use Illuminate\Routing\Router;
use Illuminate\Support\ServiceProvider;
use Illuminate\View\Factory as View;
use Phoneme\Contracts\Initializable;
use Phoneme\Contracts\PhonemeTheme;

class StripedServiceProvider extends ServiceProvider implements PhonemeTheme, Initializable
{
    /**
     * @var View
     */
    private $view;
    /**
     * @var Router
     */
    private $router;

    public function __construct(Application $app, View $view,
                                Router $router)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->view = $view;
        $this->router = $router;
    }

    public function register()
    {
    }

    public function boot()
    {
        $this->defineRoutes();
        $this->viewsBaseFolder();
    }

    private function defineRoutes()
    {
        $this->router->group(['prefix' => '/', 'namespace' => 'Striped\Controllers'], function () {
            $this->router->get("/", ['as' => 'home', 'uses' => 'StripedController@home']);
            $this->router->get("/article/{post_title}", ['as' => 'article', 'uses' => 'SingleController@single']);
            $this->router->get("/page/{page_title}", ['as' => 'page', 'uses' => 'PageController@page']);
            $this->router->post("/user/register", ['as' => 'register', 'uses' => 'UsersController@register']);
        });
    }

    private function viewsBaseFolder()
    {
        $this->view
            ->addNamespace('Striped', __DIR__ . "/../views");
    }

}