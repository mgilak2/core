<?php

namespace Striped\Controllers;

use Phoneme\Http\Controllers\Controller;
use Post\Post;
use Redirect;
use View;

class SingleController extends Controller
{
    public function single($url_parameter)
    {
        if (is_numeric($url_parameter))
            $post = $this->single_by_id($url_parameter);
        else
            $post = $this->single_by_permalink($url_parameter);

        if ($post)
            return View::make('Striped::views.single', compact("post"));

        return Redirect::route("home");
    }

    public function single_by_id($id)
    {
        $post = Post::find($id);
        if ($post)
            return $post;

        return false;
    }

    public function single_by_permalink($permalink)
    {
        $post = Post::where("permalink", $permalink)->where("type", 1)->first();
        if ($post)
            return $post;

        return false;
    }
}