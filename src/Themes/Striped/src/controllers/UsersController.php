<?php

namespace Striped\Controllers;

use Phoneme\Http\Controllers\Controller;

class UsersController extends Controller
{
    public function register()
    {
        $user = module("UserManagement");
        $inputs = \Input::only('email', 'password');
        $inputs['password'] = \Hash::make($inputs['password']);
        $user->db()->insert($inputs);

        return \Redirect::route("home");
    }
}