<?php

namespace Striped\Controllers;

use Phoneme\Http\Controllers\Controller;
use Post\Post;
use Route;
use Striped\Services\Slider;
use Illuminate\View\Factory as View;

class StripedController extends Controller
{
    protected $slider;
    /**
     * @var View
     */
    private $view;

    public function __construct(Slider $slider,View $view)
    {
        $this->slider = $slider;
        $this->view = $view;
    }

    public function home()
    {
        $posts = Post::where('type', 1)
            ->orderBy("id", 'desc')
            ->take(5)
            ->get();
        $slides = $this->slider->get_images(5);

        return $this->view->make("Striped::index", compact('posts', 'slides'));
    }
}