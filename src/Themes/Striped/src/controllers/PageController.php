<?php namespace Striped\Controllers;

use Phoneme\Http\Controllers\Controller;
use Post\Post;
use Redirect;
use Route;
use View;

class PageController extends Controller
{
    public function page($url_parameter)
    {
        if (is_numeric($url_parameter))
            $post = $this->page_by_id($url_parameter);
        else
            $post = $this->page_by_permalink($url_parameter);

        if ($post)
            return View::make('Striped::views.single', compact("post"));

        return Redirect::route("home");
    }

    public function page_by_id($id)
    {
        $post = Post::find($id);
        if ($post)
            return $post;

        return false;
    }

    public function page_by_permalink($permalink)
    {
        $post = Post::where("permalink", $permalink)
            ->where("type", 2)->first();
        if ($post)
            return $post;

        return false;
    }
}