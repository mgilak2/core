<?php

namespace Striped\Services;

use Sliders\Slider as repo;

class Slider
{
    public function get_images($count)
    {
        return repo::orderBy("id", "desc")->take($count)->get();
    }
}