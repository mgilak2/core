<?php

namespace Striped;

class WidgetService
{
    public function getWidgets()
    {
        return ["calendar" => new Calendar(),
            "latest" => new LatestPost(),
            "Text" => new TextWidget()];
    }
}