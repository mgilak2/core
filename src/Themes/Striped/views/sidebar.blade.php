<div id="sidebar">
	<!-- Logo -->
	<h1 id="logo"><a href="{{ route("home") }}">STRIPED</a></h1>

	<?php widgetsOfSidebar("left sidebar"); ?>

	<!-- Copyright -->
	<ul id="copyright">
		<li>&copy; Untitled.</li>
		<li>Design: <a href="http://html5up.net">HTML5 UP</a></li>
	</ul>
</div>