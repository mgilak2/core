<!DOCTYPE HTML>
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>@yield('title', "Striped")</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    @section('scripts')
		<!--[if lte IE 8]>
    <script src="{{ asset('Striped/assets/css/ie/html5shiv.js') }}"></script><![endif]-->
    <script src="{{ asset('Striped/assets/js/jquery.min.js') }}"></script>
    <script src="{{ asset('Striped/assets/js/skel.min.js') }}"></script>
    <script src="{{ asset('Striped/assets/js/skel-layers.min.js') }}"></script>
    <script src="{{ asset('Striped/assets/js/init.js') }}"></script>
    <link rel="stylesheet" href="{{ asset('Striped/assets/css/skel.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Striped/assets/css/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Striped/assets/css/style-desktop.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Striped/assets/css/style-wide.css') }}"/>
    <!--[if lte IE 8]>
    <link rel="stylesheet" href="{{ asset('Striped/assets/css/ie/v8.css') }}"/><![endif]-->
    <link href="{{ asset("Striped/assets/bxslider/jquery.bxslider.css") }}" rel="stylesheet"/>

    <link href="{{ asset("Striped/assets/prism/prism.css") }}" rel="stylesheet"/>
    <script src="{{ asset("Striped/assets/prism/prism.js") }}" type="text/javascript"></script>
    @show
    <style>
        h2 a, h4 a, h5 a, h6 a {
            direction: rtl;
            float: right;
        }

        div.content {
            direction: rtl;
        }
    </style>
</head>
<body class="left-sidebar">

<div id="wrapper">

    @yield("content")

    @include("Striped::sidebar")

</div>

<script type="text/javascript" src="{{ asset("Striped/assets/jquery.js") }}"></script>
<script type="text/javascript"
        src="{{ asset("Striped/assets/bxslider/plugins/jquery.fitvids.js") }}"></script>
<script type="text/javascript" src="{{ asset("Striped/assets/bxslider/jquery.bxslider.js") }}"></script>
<script>
    $(document).ready(function () {
        console.log('alireza');
        $('.bxslider').bxSlider();
    });
</script>

@yield("footer")

</body>
</html>