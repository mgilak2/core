@extends("Striped::views.layout")
@section("content")
    <!-- Content -->
    <div id="content">
        <div class="inner">

            <!-- Post -->
            <article class="box post post-excerpt">
                <header>
                    <h2><a href="{{ $post->url() }}">{{ $post->subject }}</a></h2>
                </header>
                <div class="info">
                    <span class="date"><span class="month">Jul<span>y</span></span> <span class="day">14</span><span class="year">, 2014</span></span>
                    <ul class="stats">
                        <li><a href="#" class="icon fa-comment">16</a></li>
                        <li><a href="#" class="icon fa-heart">32</a></li>
                        <li><a href="#" class="icon fa-twitter">64</a></li>
                        <li><a href="#" class="icon fa-facebook">128</a></li>
                    </ul>
                </div>
                <a href="#" class="image featured"><img src="{{ $post->featured_image }}" alt="" /></a>
                <div class='content'>
                    {{ $post->content }}
                </div>
            </article>


            <!-- Pagination -->
            <div class="pagination">
                <!--<a href="#" class="button previous">Previous Page</a>-->
                <div class="pages">
                    <a href="#" class="active">1</a>
                    <a href="#">2</a>
                    <a href="#">3</a>
                    <a href="#">4</a>
                    <span>&hellip;</span>
                    <a href="#">20</a>
                </div>
                <a href="#" class="button next">Next Page</a>
            </div>

        </div>
    </div>
@stop

