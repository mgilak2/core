<?php
return [
    'name'            => "Admin",
    'serviceprovider' => 'AdminTheme\\AdminThemeServiceProvider',
    'dir'             => 'Admin',
    'theme'           => 'Admin',
    "admin"           => true,
    'hooks'            => true
];