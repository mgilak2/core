<?php namespace AdminTheme;

use Illuminate\Auth\Guard as Auth;
use Illuminate\View\Factory as View;

class AdminTheme
{
    /**
     * @var Auth
     */
    private $auth;
    /**
     * @var View
     */
    private $view;

    public function __construct(Auth $auth, View $view)
    {
        $this->auth = $auth;
        $this->view = $view;
    }

    public function baseDashboard($view, $dependencies = [])
    {
        return $this->view->make($view,
            ["user" => $this->auth->user()] + $dependencies);
    }

    public function filemanager($view, $dependencies = [])
    {
        return $this->view->make($view,
            ["user" => $this->auth->user()] + $dependencies);
    }

    public function dashboard($view, $dependencies = [])
    {
        return $this->view->make($view,
            ["user" => $this->auth->user(), 'adminpage' => true] + $dependencies);
    }
}