<?php namespace AdminTheme;

use Illuminate\Support\ServiceProvider;
use Phoneme\Contracts\PhonemeTheme;

class AdminThemeServiceProvider extends ServiceProvider implements PhonemeTheme
{
    public function register()
    {
        $this->app->bind("\\AdminTheme\\AdminTheme", function ($app) {
            return new AdminTheme($app->make("Illuminate\\Auth\\Guard"), $app['view']);
        });
    }
}