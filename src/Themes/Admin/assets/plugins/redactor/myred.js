﻿$(document).ready(function () {
    $('.redactor').redactor({
        imageUpload: "http://" + window.location.host + "/admin/upload/imageupload",
        imageGetJson: "http://" + window.location.host + "/admin/upload/getimages",
         autoformat: false,
         convertDivs: false,
         focus: true,
         plugins: ['fontcolor', 'imagemanager'],
         lang : 'fa',
         direction: 'rtl'
    });
});