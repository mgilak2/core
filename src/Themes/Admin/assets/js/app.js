﻿/**
 * Created by saeed on 11/27/2014.
 */

$(document).ready(function(){
    //$(".select2").select2();

    // Google styled selects
    $('select').not('.input-lg').not('.input-sm').each(function () {
        var e = $(this)
        e.select2({
            minimumResultsForSearch: 20
        })
    });

    // tooltip

    $(function() {
        $('[data-toggle="tooltip"]').tooltip();
    });
    

    $(".select-multi").select2({
        placeholder: "انتخاب گروه"
    });
    //$(".select-norm").select2();
});
