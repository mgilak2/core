jQuery.route = {
    nextFolder:function(id,name){
        $.route.unActive();
        $.route.addActive(id,name);
        //console.log(id+' - '+name);
    },
    selectFolder: function (id,name) {
        $('#currentPath').attr('data-parent-id',id);
        $('#fileParent').val(id);
        $.filemanager.databind();
        //console.log(id+' - '+name);
    }
    ,
    unActive: function () {
        var active =$('#filemanagerRoute .active');
        if(active.length<=0)
        return;
        active.removeClass('active');
        var item ='<a href="#">'+active.attr('data-name')+'</a>';
        var divider ='<span class="divider"></span>';
        active.html(item+divider);
    }
    ,
    addActive: function (id,name) {
        var item='<li class="active" data-id="'+id+'" data-name="'+name+'" >'+name+'</li>';
        $('#filemanagerRoute').append(item);
    }
    ,
    toActive: function (id,name) {

    }
}