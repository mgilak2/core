﻿@yield("master.head")
<header class="header nav navbar-inverse">
    @yield("master.header")
</header>

<div class="map-shortkey bottom-box-shadow">
    <div class="container">
        <div class="row">
            <div class="col-sm-12 col-md-12">
                @yield("master.navigation")
            </div>
        </div>
    </div>
</div>

<div class="page-content">
    <div class="content">
        <div class="row">

            <div class="container">
                @if(isset($adminpage))
                    @yield('dashboardContent')
                @else

                @section('mainContent')
                    <div class="grid grid-max center-box-shadow">
                        <div class="grid-title">
                            <!-- Title -->
                            <h4>
                                @yield("pageTitle")
                            </h4>
                            <!-- End Title -->
                        </div>

                        <!-- Page main body -->
                        <div class="grid-body">
                            <!-- Page content ---------------------------->
                            @yield("master.ticket.form.add")
                            @yield("content")
                                    <!-- End Page content ------------------------>
                        </div>
                    </div>
                @show
                @endif
                @yield("fileManager")

            </div>
        </div>
    </div>
</div>
@yield("master.footer")