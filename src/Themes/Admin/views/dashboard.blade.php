@extends("Admin::master")

@section("master.head")
    @include("Admin::head")
@stop

@section("master.header")
    @include("Admin::header")
@stop

@section("master.navigation")
    @include("Admin::navigation")
@stop

@section("dashboardContent")

    <div class="row ds-sm">
        <div class="col-sm-3">
            <div class="grid center-box-shadow">
                <div class="grid-body grid-blue">
                    <div class="my-users my-ds-sm-box">
                        <div class="progress-list">
                            <div class="details">
                                <i class="icon-users4"></i>

                                <h2 class="media-heading animate-number" data-value="3659"
                                    data-animation-duration="1500">3,659</h2>
                            </div>
                            <div class="title">تعداد بازدید این ماه %</div>
                            <div class="status pull-right">
                                <span class="animate-number" data-value="83" data-animation-duration="1500">83</span>%
                            </div>
                            <div class="clearfix"></div>
                            <div class="progress progress-little">
                                <div class="progress-bar animate-progress-bar" data-percentage="83%"
                                     style="width: 83%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="grid center-box-shadow">
                <div class="grid-body grid-red">
                    <div class="my-users my-ds-sm-box">
                        <div class="progress-list">
                            <div class="details">
                                <i class="icon-earth"></i>

                                <h2 class="media-heading animate-number" data-value="3659"
                                    data-animation-duration="1500">3,659</h2>
                            </div>
                            <div class="title">میزان فروش این ماه %</div>
                            <div class="status pull-right">
                                <span class="animate-number" data-value="45" data-animation-duration="1500">45</span>%
                            </div>
                            <div class="clearfix"></div>
                            <div class="progress progress-little">
                                <div class="progress-bar animate-progress-bar" data-percentage="45%"
                                     style="width: 45%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="grid center-box-shadow">
                <div class="grid-body grid-yellow">
                    <div class="my-users my-ds-sm-box">
                        <div class="progress-list">
                            <div class="details">
                                <i class="icon-eye"></i>

                                <h2 class="media-heading animate-number" data-value="3659"
                                    data-animation-duration="1500">3,659</h2>
                            </div>
                            <div class="title">تعداد بازدید این ماه %</div>
                            <div class="status pull-right">
                                <span class="animate-number" data-value="83" data-animation-duration="1500">74</span>%
                            </div>
                            <div class="clearfix"></div>
                            <div class="progress progress-little">
                                <div class="progress-bar animate-progress-bar" data-percentage="74%"
                                     style="width: 74%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-sm-3">
            <div class="grid center-box-shadow">
                <div class="grid-body grid-green">
                    <div class="my-users my-ds-sm-box">
                        <div class="progress-list">
                            <div class="details">
                                <i class="icon-cart"></i>

                                <h2 class="media-heading animate-number" data-value="3659"
                                    data-animation-duration="1500">3,659</h2>
                            </div>
                            <div class="title">تعداد بازدید این ماه %</div>
                            <div class="status pull-right">
                                <span class="animate-number" data-value="83" data-animation-duration="1500">83</span>%
                            </div>
                            <div class="clearfix"></div>
                            <div class="progress progress-little">
                                <div class="progress-bar animate-progress-bar" data-percentage="83%"
                                     style="width: 83%;"></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="grid center-box-shadow grid-red">
                <div class="grid-body">
                    <img src="{{asset('Admin/assets/img/bg2.jpg')}}" alt="lool" style="width: 100%"/>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="grid center-box-shadow">
                <div class="grid-body grid-skyblue">
                    <div id="weather" style="min-height: 221px;"></div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-sm-8">
            <div class="grid center-box-shadow grid-red">
                <div class="grid-body">
                    <div id="visitors" style="height: 230px;"></div>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <div class="grid center-box-shadow" style="background-color: #87CEEB">
                <div class="grid-body">
                    <div id="browser-usage" style="height: 230px;" class="morris-chart"></div>
                </div>
            </div>
        </div>
    </div>
@stop

@section("master.footer")
    @include("Admin::extensions.weather")
    @include("Admin::extensions.morris")
    @include("Admin::footer")
@stop

