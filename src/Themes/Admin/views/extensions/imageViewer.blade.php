<link rel="stylesheet" href="{{ asset('Admin/assets/plugins/fancybox/jquery.fancybox.css') }}"/>
<script src="{{ asset('Admin/assets/plugins/fancybox/jquery.fancybox.pack.js') }}" type="text/javascript" ></script>

<script>
		/*
			 *  Thumbnail helper. Disable animations, hide close button, arrows and slide to next gallery item if clicked
			 */

			$('.image-viewer').fancybox({
				prevEffect : 'none',
				nextEffect : 'none',

				closeBtn  : false,
				arrows    : false,
				nextClick : true,

				helpers : {
					thumbs : {
						width  : 50,
						height : 50
					}
				}
			});
</script>