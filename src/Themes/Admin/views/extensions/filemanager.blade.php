
<script src="{{ asset('Admin/assets/plugins/fancybox/source/jquery.fancybox.pack.js')}}"></script>
<script type="text/javascript">
    $(document).ready(function () {
        $("#newUpload").fancybox({
            width: '75%',
            height: '75%',
            autoSize: false,
            fitToView: false,
            autoScale: false,
            transitionIn: 'elastic',
            transitionOut: 'elastic',
            type: 'iframe',
            'afterClose': function () {
                var inputVal = $('#image').val();
                if (inputVal != '') {
                    var imgSrc = inputVal.replace('/uploads/', '/thumbs/');
                    $('#imgUpload').attr('src', imgSrc);
                    $('#thumbs').val(imgSrc);
                    $('#imgUpload,#deleteImg').show();
                    $('#newUpload').remove();
                }

            }
        });
        function close_window() {
            parent.$.fancybox.close();
        }
        $('#deleteImg').live('click', function () {
            if(confirm("آیا مایلید تصویر را حذف کنید؟")){
            $('#imgUpload').attr('src', '').hide();
            $('#image').val(null);
            $(this).hide();
            if($('#newUpload').length==0)
                $('#updateImage').append('<a id="newUpload" href="{{URL::to("Aone/modules/addFilePop/dialog.php?type=1&field_id=image&lang=fa")}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> تصویر </a> ');
            }
            return false;
        });

        if($('#imgUpload').attr('src')==''){
            $('#updateImage').append('<a id="newUpload" href="{{URL::to("Aone/modules/addFilePop/dialog.php?type=1&field_id=image&lang=fa")}}" class="btn btn-primary"><i class="fa fa-plus-circle"></i> تصویر </a>  ');
            $('#imgUpload,#deleteImg').hide();}
    });
</script>

