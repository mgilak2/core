<link href="{{ asset('Admin/assets/plugins/Jcrop/jquery.Jcrop.css') }}" rel="stylesheet"/>
<script src="{{ asset('Admin/assets/plugins/Jcrop/jquery.Jcrop.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('Admin/assets/plugins/Jcrop/jquery.color.js') }}" type="text/javascript"></script>

<script type="text/javascript">
    function jcropHandler() {
        var jcrop_api;

        $('#target').Jcrop({
            bgFade: true,
            bgOpacity: .2,
            setSelect: [20, 20, 500, 250],
            boxWidth: 850
        }, function () {
            jcrop_api = this;
        });

        $('#fadetog').change(function () {
            jcrop_api.setOptions({
                bgFade: this.checked
            });
        }).attr('checked', 'checked');

        $('#shadetog').change(function () {
            jcrop_api.setOptions({
                shade: this.checked
            });
        }).attr('checked', false);
    }
</script>