<link rel="stylesheet" href="{{ asset('Admin/assets/plugins/morris/css/morris.css') }}"/>
<script src="{{ asset('Admin/assets/plugins/morris/raphael-min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('Admin/assets/plugins/morris/morris.min.js') }}" type="text/javascript" ></script>


    <script>
           // Morris donut chart
              Morris.Donut({
                element: 'browser-usage',
                data: [
                  {label: "Chrome", value: 25},
                  {label: "Safari", value: 20},
                  {label: "Firefox", value: 15},
                  {label: "Opera", value: 5},
                  {label: "Internet Explorer", value: 10},
                  {label: "Other", value: 25}
                ],
                colors: ['#00a3d8', '#2fbbe8', '#72cae7', '#d9544f', '#ffc100', '#1693A5']
              });

              $('#browser-usage').find("path[stroke='#ffffff']").attr('stroke', 'rgba(0,0,0,0)');

              Morris.Line({
                element: 'visitors',
                data: [
                  { y: '1387', a: 100, b: 90 },
                  { y: '1388', a: 75,  b: 65 },
                  { y: '1389', a: 50,  b: 80 },
                  { y: '1390', a: 75,  b: 65 },
                  { y: '1391', a: 50,  b: 40 },
                  { y: '1392', a: 75,  b: 65 },
                  { y: '1393', a: 100, b: 90 }
                ],
                xkey: 'y',
                ykeys: ['a', 'b'],
                labels: ['آبان', 'مهر '],
                colors: ['#00a3d8', '#2fbbe8']
              });
    </script>