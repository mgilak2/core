<link rel="stylesheet" href="{{ asset('Admin/assets/plugins/jquery-nestable/jquery.nestable.css') }}"/>
<script src="{{ asset('Admin/assets/plugins/jquery-nestable/jquery.nestable.js') }}" type="text/javascript"></script>
<script type="text/javascript">
    $(function () {
        $('.dd').nestable({
            dropCallback: function (details) {

                // p!wvH
                $('.dd li.edit').remove();
                $('.dd li a.edit_toggle[disabled=disabled]').removeAttr('disabled');

                var order = new Array();
                $("li[data-id='" + details.destId + "']").find('ol:first').children().each(function (index, elem) {
                    order[index] = $(elem).attr('data-id');
                });

                if (order.length === 0) {
                    var rootOrder = new Array();
                    $("#nestable > ol > li").each(function (index, elem) {
                        rootOrder[index] = $(elem).attr('data-id');
                    });
                }

                $.post('{{route("menu.update.order.put", [$menu->id])}}',
                        {
                            source: details.sourceId,
                            destination: details.destId,
                            order: JSON.stringify(order),
                            rootOrder: JSON.stringify(rootOrder),
                            _token: $('._token').val()
                        },
                        function (data) {
                        }
                        )
                        .
                        done(function (data) {
                            $("#success-indicator").fadeIn(100).delay(1000).fadeOut();
                        })
                        .fail(function (e) {
                        })
                        .always(function () {
                        });
            }
        });

        $('.dd .delete_toggle').each(function (index, elem) {
            var This = $(this);

            $(elem).click(function (e) {
                if (confirm('آیا مایلید آیتم مورد نظر حذف شود؟')) {
                    var id = This.closest('li').data().id;
                    $.post('/dashboard/menu/delete/' + id + '/item', {
                        _method: 'delete'
                    }).done(function (data) {
                        This.closest('li').fadeTo(400, 0, function () {
                            $(this).remove();
                        })
                    }).fail(function (e) {
                        console.log(e.responseText);
                    });
                }
                e.preventDefault();
            });
        });


        // p!wvH

        // edit
        $('.dd li a.edit_toggle').click(function () {
            $(this).attr('disabled', 'disabled');
            var id = $(this).attr('rel'),
                    title = $(this).closest('li').find('.dd-handle').text(),
                    url = $(this).closest('li').find('.dd-handle').data().url;

            var html = '<li class="edit" data-id="' + id + '"><div class="row"><div class="col-sm-5"><input type="text" class="form-control" name="title" value="' + title + '"></div>' +
                    '<div class="col-sm-5"><input type="text" name="url" class="form-control" value="' + url + '"></div>' +
                    '<button class="btn btn-info btn-sm save">ثبت</button></li></div>';
            $(this).closest('li').after(html);
        });

        $('body').on('click', '.dd li.edit .save', function () {
            var li = $(this).closest('li'),
                    id = li.data().id,
                    title = li.find('input[name=title]').val(),
                    url = li.find('input[name=url]').val();
            var _token = $('._token').val();

            $.post('/dashboard/menu/update/' + id + '/item', {
                _method: 'put',
                title: title,
                url: url,
                _token: _token
            }).done(function (data) {
                var item = $('.dd li[data-id=' + id + ']')
                item.find('.dd-handle').text(title);
                item.find('.dd-handle').data('url', url);
                li.remove();
                item.find('a.edit_toggle').removeAttr('disabled');
            }).fail(function (e) {
                //console.log(e.responseText);
            });
        });

    });
</script>


<script>
    var DoAfter = {
        time: 1000,
        type: 'text',
        callback: function () {
        },

        run: function (element, type, time, callback) {
            this.type = type || this.type;
            this.time = time || this.time;
            this.callback = callback || this.callback;

            var _time = this.time, _type = this.type, _callback = this.callback;

            element.each(function () {
                var t;
                if (_type == 'text') {
                    $(this).keydown(function () {
                        clearTimeout(t);
                        var This = $(this);
                        t = setTimeout(function () {
                            _callback(This);
                        }, _time);
                    });
                }
                else {
                    $(this).change(function () {
                        clearTimeout(t);
                        var This = $(this);
                        t = setTimeout(function () {
                            _callback(This);
                        }, _time);
                    });
                }
            });
        }
    };

    $(document).ready(function () {
        var el = $('.row input[type=text]').not('[name=title]');
        DoAfter.run(el, 'text', 1000, function (el) {
            alert('Updaintg... [' + el.val() + ']');
            var _token = $('._token').val();
            $.ajax({
                type: 'post',
                data: {action: 'update'},
                url: null,
                _token: _token
            }).done(function () {

            }).fail(function () {

            })
        });
    });
</script>