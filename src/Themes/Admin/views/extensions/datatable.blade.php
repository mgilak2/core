<link rel="stylesheet" href="{{ asset('Admin/assets/plugins/jquery-datatable/media/css/jquery.dataTables.css') }}"/>
<link rel="stylesheet" href="{{ asset('Admin/assets/plugins/jquery-datatable/extensions/bootstrap/dataTables.bootstrap.css') }}"/>
<script src="{{ asset('Admin/assets/plugins/jquery-datatable/media/js/jquery.dataTables.min.js') }}" type="text/javascript" ></script>
<script src="{{ asset('Admin/assets/plugins/jquery-datatable/extensions/bootstrap/dataTables.bootstrap.js') }}" type="text/javascript" ></script>

<script src="{{ asset('Admin/assets/plugins/datatables-responsive/js/datatables.responsive.js') }}" type="text/javascript"></script>
<script src="{{ asset('Admin/assets/plugins/datatables-responsive/js/lodash.min.js') }}" type="text/javascript"></script>

<!-- END PAGE LEVEL PLUGINS -->
<script type="text/javascript" src="{{ asset('Admin/assets/js/datatables.js') }}"></script>
