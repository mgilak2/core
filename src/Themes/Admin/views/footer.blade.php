    <!-- Scripts -->
    <script src="{{ asset('Admin/assets/plugins/boostrapv3/js/bootstrap.min.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/todc-select/select2.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/jquery-nicescroll/jquery.nicescroll.min.js') }}"></script>

    <script src="{{ asset('Admin/assets/plugins/redactor/redactor.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/redactor/fontcolor.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/redactor/fa.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/redactor/myred.js') }}"></script>


    <!-- CALENDAR CONVERTER -->
    <script src="{{ asset('Admin/assets/plugins/calendar/scripts/jquery.plugin.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/calendar/scripts/jquery.calendars.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/calendar/scripts/jquery.calendars.plus.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/calendar/scripts/jquery.calendars.picker.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/calendar/scripts/calendar.js') }}"></script>
    <!-- CALENDAR CONVERTER -->

    <script src="{{ asset('Admin/assets/js/app.js') }}"></script>

    <script>
        $('html').niceScroll({
            cursorcolor: "#000",
            cursorborder: "0px solid #fff",
            railpadding: {
                top: 0,
                right: 0,
                left: 0,
                bottom: 0
            },
            cursorwidth: "10px",
            cursorborderradius: "0px",
            cursoropacitymin: 0.2,
            cursoropacitymax: 0.8,
            boxzoom: true,
            horizrailenabled: false,
            zindex: 9999
        });

        /*$(function () {
          $('[data-toggle="popover"]').popover()
        });*/

        $('#appListIco').popover({
            placement: "bottom",
            trigger: "click",
            html: true,
            content: $("#appList").html()
        });
    </script>
@yield("extensionScript")
</body>
</html>