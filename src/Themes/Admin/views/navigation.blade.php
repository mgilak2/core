<div class="header-nav-box">
    <div class="navbar-header">
        <a href="{{ route("setting.index") }}" class="navbar-btn btn-options">
            <i class="icon-cog3"></i>
        </a>
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar-collapse2">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
    </div>
    <div class="collapse navbar-collapse" id="navbar-collapse2">
        <ul class="nav navbar-nav navbar-right nav-tabs-google">
            <li class="active">
                <a href="{{ route("dashboard") }}">داشبورد</a>
            </li>

            <li><a href="javascriptVoid:;" role="button" class="navbar-btn dropdown-toggle" data-toggle="dropdown">کاربران</a>
                <ul class="nav dropdown-menu">
                    <li><a href="{{ route('user.create') }}"><i class="icon-user"></i>ایجاد حساب کاربری</a></li>
                    <li><a href="{{ route('user.index') }}"><i class="fa fa-list"></i>لیست کاربران</a></li>
                    <li><a href="{{ route('role.index') }}"><i class="fa fa-lock"></i>گروه های کاربری</a></li>
                </ul>
            </li>
            <li><a href="javascriptVoid:;" role="button" class="navbar-btn dropdown-toggle"
                   data-toggle="dropdown">منوها</a>
                <ul class="nav dropdown-menu">
                    <li><a href="{{ route('menu.create') }}"><i class="icon-plus"></i>ایجاد منو</a></li>
                    <li><a href="{{ route('menu.list') }}"><i class="fa fa-list"></i>لیست منو</a></li>
                </ul>
            </li>
            <li><a href="javascriptVoid:;" role="button" class="navbar-btn dropdown-toggle"
                   data-toggle="dropdown">صفحات</a>
                <ul class="nav dropdown-menu">
                    <li><a href="{{ route('page.add.get') }}"><i class="icon-plus"></i>ایجاد صفحه</a></li>
                    <li><a href="{{ route('page.list') }}"><i class="fa fa-list"></i>لیست صفحات</a></li>
                </ul>
            </li>
            <li><a href="javascriptVoid:;" role="button" class="navbar-btn dropdown-toggle"
                   data-toggle="dropdown">مطالب</a>
                <ul class="nav dropdown-menu">
                    <li><a href="{{ route('post.add.get') }}"><i class="icon-plus"></i>افزودن مطلب</a></li>
                    <li><a href="{{ route('post.list') }}"><i class="fa fa-list"></i>لیست مطالب</a></li>
                    <li><a href="{{ route('cat.add.get') }}"><i class="fa fa-sitemap"></i>دسته بندی ها</a></li>
                </ul>
            </li>

            <li>
                <a href="javascriptVoid:;" role="button" class="navbar-btn dropdown-toggle" data-toggle="dropdown">اسلایدشو</a>
                <ul class="nav dropdown-menu">
                    <li><a href="{{ route('slider.add.get') }}"><i class="icon-plus"></i>افزودن اسلاید</a></li>
                    <li><a href="{{ route('slider.list') }}"><i class="fa fa-list"></i>لیست اسلاید</a></li>
                </ul>
            </li>

            <li><a href="{{ route('filemanager.index') }}">مدیریت رسانه</a></li>
            <li><a href="{{ route('widget.index') }}">ابزارک ها</a></li>
        </ul>
    </div>
</div>
