﻿<div class="logo">
    <img style="padding: 14px 10px;height: 90px;" src="{{ asset('Admin/assets/img/logo.png')}}" alt=""/>
</div>
<div class="pull-right">
    <div class="user-account-info">

        <div class="user-apps">
            <div class="navbar-header">
                <a href="#" class="navbar-btn dropdown-toggle" data-toggle="dropdown">
                    <i class="icon-users4" style="font-size: 18px;"></i>
                </a>
                <ul class="nav dropdown-menu">
                    <li><a href="#"><i class="glyphicon glyphicon-user"></i>پروفایل</a></li>
                    <li><a href="#"><i class="glyphicon glyphicon-cog"></i>تنظیمات کاربری</a></li>
                    <li><a href="{{-- route('logout') --}}"><i class="glyphicon glyphicon-log-out"></i>خروج</a></li>
                </ul>
            </div>
        </div>

        <div class="user-name">
            <a href="javascript:">{{-- Auth::user()->first_name --}} </a>
        </div>
    </div>
    <div id="appList">
        <ul class="nav">
            <li><a href="#"><i class="icon-user"></i>پروفایل</a></li>
            <li class="nav-divider"></li>
            <li><a href="#"><i class="icon-cog"></i>تنظیمات</a></li>
            <li><a href="{{-- route('logout') --}}"><i class="glyphicon glyphicon-log-out"></i>خروج</a></li>
        </ul>
    </div>
</div>