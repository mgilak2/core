@extends("Admin::dashboard")
@section("fileManager")

    <link href="{{ asset('Admin/assets/css/filemanager.css') }}" rel="stylesheet"/>
    <!-- file uploader -->
    <link href="{{ asset('Admin/assets/plugins/dropzone/css/basic.css') }}" rel="stylesheet"/>
    <link href="{{ asset('Admin/assets/plugins/dropzone/css/dropzone.css') }}" rel="stylesheet"/>
@section('mainContent') @stop

<div class="grid grid-max center-box-shadow">
    <div class="grid-title fm-options">
        <a href="#addFile" class="btn btn-default" role="button" data-toggle="modal"><i
                    class="icon-upload3"></i>آپلود</a>
        <a href="#addFolder" id="popAddFolder" class="btn btn-default" role="button" data-toggle="modal"><i
                    class="icon-plus"></i>ایجاد پوشه</a>
    </div>
    <div class="site-map">
        @yield("filemanager.path")
    </div>
    <div class="grid-body fm-body" id="fileManagerBody">
        <div class="row">
            <div class="col-sm-12">
                <div class="file-explore">
                    <div id=":Folders" class="row">
                        <div class="col-sm-12" id="folder-container">
                        </div>
                    </div>
                    <div id=":Files" class="row">
                        <div class="col-sm-12" id="file-container">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<!-- Add Folder -->
<div class="modal fade" id="addFolder">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h4 id="modal-title" class="modal-title">ایجاد پوشه</h4>
            </div>
            <div class="modal-body">
                <input type="text" id="modal-text" class="form-control" placeholder=""/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-primary" id="btnModal" data-action="" data-dismiss="modal">ایجاد
                    پوشه
                </button>
                <button type="button" class="btn btn-default" data-dismiss="modal">بستن</button>
            </div>
        </div>
    </div>
</div>
<!-- Add File -->
<div class="modal fade" id="addFile">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title"><i class="icon-plus" style="font-size: 13px; color: forestgreen"></i> آپلود
                    تصویر </h4>
            </div>
            <div class="modal-body" style="position: relative">
                <div class="row">
                    <div class="col-sm-12">
                        <div style="max-width: 600px; position: relative; overflow: hidden;">
                            {!! Form::open(['route'=> 'file.add', 'files' => true, 'class'=> 'dropzone']) !!}
                            <div class="fallback" id="dropzone">
                                <input name="file" type="file" multiple/>
                            </div>
                            <input id="fileParent" type="hidden" name="parent_id" value="1"/>
                            {!! Form::close() !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="resetDzone">بستن</button>
            </div>
        </div>
    </div>
</div>

<!-- Context Menu Folder -->
<div id="contextMenu_folder" class="context-menu">
    <ul class="dropdown-menu" role="menu">
        <li><a id="fm_delete" tabindex="-1"><i class="fa fa-trash"></i>حذف</a></li>
        <li><a id="fm_rename" tabindex="-1"><i class="fa fa-pencil"></i>تغییر نام</a></li>
        <li><a id="fm_link" tabindex="-1"><i class="fa fa-link"></i>نمایش آدرس</a></li>
        <li class="divider"></li>
        <li><a id="fm_size" tabindex="-1"><i class="fa fa-stop"></i>0 بیت</a></li>
    </ul>
</div>
<!-- Context Menu File -->
<div id="contextMenu_file" class="context-menu">
    <ul class="dropdown-menu" role="menu">
        <li><a id="fl_crop" tabindex="-1" data-image=""><i class="fa fa-crop"></i>برش تصویر</a></li>
        <li><a id="fl_delete" tabindex="-1"><i class="fa fa-trash"></i>حذف</a></li>
        <li><a id="fl_rename" tabindex="-1"><i class="fa fa-edit"></i>تغییر نام</a></li>
        <li><a id="fl_url" tabindex="-1"><i class="fa fa-link"></i>نمایش آدرس</a></li>
        <li><a id="fl_download" tabindex="-1"><i class="fa fa-download"></i>دانلود</a></li>
        <li class="divider"></li>
        <li><a tabindex="-1"><i class="fa fa-stop"></i><span id="filesize"></span>&nbsp;بیت</a></li>
    </ul>
</div>

<!-- Crop Image -->
<div class="modal fade" id="cropImage">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span
                            class="sr-only">Close</span></button>
                <h4 class="modal-title"><i class="icon-crop" style="font-size: 13px; color: forestgreen"></i>برش تصویر
                </h4>
            </div>
            <div class="modal-body crop-image">
                <div class="row">
                    <div class="col-sm-12" id="imageZone">
                        <img src="" id="target" alt="Jcrop Image" style="width: 100%"/>
                    </div>
                    <div class="col-sm-12" id="interface">
                        <label class="checkbox">
                            <input type="checkbox" id="fadetog"/> Enable fading (bgFade: true)
                        </label>
                        <label class="checkbox">
                            <input type="checkbox" id="shadetog"/> Use experimental shader (shade: true)
                        </label>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" data-dismiss="modal" id="cropImageClose">بستن</button>
            </div>
        </div>
    </div>
</div>

@stop
@section("extensionScript")
    <script src="{{ asset('Admin/assets/filemanager/js/underscore-min.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/bootstrap-contextmenu/bootstrap-contextmenu.js') }}"></script>
    <script src="{{ asset('Admin/assets/plugins/dropzone/dropzone.min.js') }}"></script>
    <script src="{{ asset('Admin/assets/filemanager/js/modal-providers.js') }}"></script>
    <script src="{{ asset('Admin/assets/filemanager/js/route.js') }}"></script>
    <script src="{{ asset('Admin/assets/filemanager/js/filemanager.js') }}"></script>
    <script src="{{ asset('Admin/assets/filemanager/js/events.js') }}"></script>
    <script>
        // $("#dropzone").dropzone({ url: "file/post"});
        $('#addFile').on('hidden.bs.modal', function (e) {
            $.filemanager.databind();
        });

        $('#addFile').on('show.bs.modal', function (e) {
            $('div.dz-preview').remove();
            if ($('div.dz-preview').length == 0)
                $(".dropzone.dz-started .dz-message").css({"opacity": "1"});
            else
                $(".dropzone.dz-started").css({"opacity": "0"});
        });


        $('#resetDzone').click(function () {
            $('div.dz-preview').remove();
            if ($('div.dz-preview').length != 0)
                $(".dropzone.dz-started").css({"opacity": "1"});
            else
                $(".dropzone.dz-started").css({"opacity": "0"});
        });

        $('#cropImage').on('hidden.bs.modal', function () {
            $(".jcrop-holder").remove();
            $("#interface").children("fieldset").remove();
            $("#imageZone img").remove();
            $("#imageZone").append('<img src="" id="target" alt="Jcrop Image" style="width: 100%" />');
        });
    </script>
    @include("Admin::extensions.imageViewer")
    @include("Admin::extensions.croper")
@endsection
