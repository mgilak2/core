<link rel="stylesheet" href="{{ asset('Aone/themes/Admin/assets/plugins/boostrapv3/css/bootstrap.min.css') }}" />
<link rel="stylesheet" href="{{ asset('Aone/themes/Admin/assets/plugins/font-awesome/css/font-awesome.min.css') }}" />
<link rel="stylesheet" href="{{ asset('Aone/themes/Admin/assets/plugins/boostrapv3/css/todc-bootstrap.css') }}" />
<link rel="stylesheet" href="{{ asset('Aone/themes/Admin/assets/plugins/boostrapv3/css/bootstrap-rtl.css') }}" />
<link rel="stylesheet" href="{{ asset('Aone/themes/Admin/assets/css/font.css') }}"/>
<link rel="stylesheet" href="{{ asset('Aone/themes/Admin/assets/css/login.css') }}"/>

<div class="container">
    <div class="row login-container column-seperation">
        <div class="alert-box"></div>
        <div id="login-box">
            <h3><img src="{{asset('Aone/themes/Admin/assets/img/aone.png')}}" alt="" /></h3>

            {{Form::open(['route'=>'login.post'],array('class' => 'login-form', 'id' => 'login_form'))}}

                <!-- Name -->
                <div class="form-group {{{ $errors->has('username') ? 'has-error' : '' }}}">
                    {{ Form::label('username', 'نام کاربری', array('class' => 'control-label')) }}

                    <div class="controls">
                        {{Form::text('email',Input::get("email"),array('class' => 'form-control'))}}
                        {{ $errors->first('username') }}
                    </div>
                </div>

                <!-- Password -->
                <div class="form-group {{{ $errors->has('password') ? 'has-error' : '' }}}">
                    {{ Form::label('password', 'کلمه عبور', array('class' => 'control-label')) }}

                    <div class="controls">
                       {{Form::password("password",array('class' => 'form-control'))}}
                        {{ $errors->first('password') }}
                        <div class="checkbox check-success pull-left">
                            <input type="checkbox" id="checkbox1" value="1" />
                            <label for="checkbox1">مرا به خاطر بسپار </label>
                        </div>
                    </div>
                </div>

                @if(!is_null($url))
                    {{Form::hidden('url',$url)}}
                @endif

                <!-- Login button -->
                <div class="form-group">
                    <div class="controls">
                        {{Form::submit("ورود به مدیریت سایت", array('class' => 'btn btn-primary btn-cons'))}}
                    </div>
                </div>
                {{ Form::close() }}
        </div>
    </div>
</div>
<div id="bg">
    <img src="{{ asset('Aone/themes/Admin/assets/img/bg.jpg')}}" alt="" />
</div>

<script src="{{ asset('Aone/themes/Admin/assets/plugins/jquery-1.8.3.min.js') }}"></script>
<script src="{{ asset('Aone/themes/Admin/assets/plugins/jquery-validation/js/jquery.validation.min.js') }}"></script>
<script src="{{ asset('Aone/themes/Admin/assets/plugins/jquery-validation/js/messages_fa.js') }}"></script>
<script type="text/javascript" src="{{ asset('Aone/themes/Admin/assets/js/login.js')}}"></script>