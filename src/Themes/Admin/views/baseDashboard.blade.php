@extends("Admin::master")

@section("master.head")
    @include("Admin::head")
@stop

@section("master.navigation")
    @include("Admin::navigation")
@stop

@section("master.header")
    @include("Admin::header")
@stop

@section("extensionScript")
    @include("Admin::extensions.weather")
    @include("Admin::extensions.morris")
@endsection

@section("master.footer")
    @include("Admin::footer")
@stop

