jQuery.filemanager = {
    dataunbind:function(){
        $('#contextMenu_folder,#contextMenu_file,#fileManagerBody, #contextMenu_folder, #contextMenu_file' +
        ',.folder-box,.file-box,#popAddFolder,#btnModal,#filemanagerRoute li').unbind();
    },
        databind: function () {

            $.filemanager.dataunbind();

        var id = $('#currentPath').attr('data-parent-id');
        var folderaction = $('#currentPath').attr('data-folder-index') + '?id=' + id;
        var fileaction = $('#currentPath').attr('data-file-index') + '?id=' + id;

        $.get(folderaction)
            .done(function (data) {
                $("#folder-container").html(data);

                $.get(fileaction)
                    .done(function (data) {
                        $("#file-container").html(data);

                        // Right click Contex Menu
                        $('#contextMenu_folder').on('show.bs.context', function () {
                            $("#contextMenu_file").hide();
                        });
                        $('#contextMenu_file').on('show.bs.context', function () {
                            $("#contextMenu_folder").hide();
                        });
                        $('#fileManagerBody, #contextMenu_folder, #contextMenu_file').bind('contextmenu', function (e) {
                            return false;
                        });


                        $('.folder-box').contextmenu({
                            target: '#contextMenu_folder',
                            onItem: function (context, e) {
                                var eventType = $(e.target).attr("id");
                                switch (eventType) {
                                    case "fm_delete":
                                        removeFolder();
                                        break;
                                    case "fm_rename":
                                        provideFolderRename();
                                        $('#addFolder').modal('show');
                                        break;
                                    case "fm_link":

                                        $('#addFolder').modal('show');
                                        break;
                                    default:
                                }
                            }
                            //,
                            //before: function (e, element, target) {
                            //e.preventDefault();
                            //alert(e.target.innerText);
                            //}
                        });
                        $('.file-box').contextmenu({
                            target: '#contextMenu_file',
                            onItem: function (context, e) {
                                var eventType = $(e.target).attr("id");
                                switch (eventType) {
                                    case "fl_crop":
                                        $("img#target").attr("src",$(e.target).attr("data-image"));
                                        //$(".jcrop-holder img").attr("src",$(e.target).attr("data-image"));
                                        $("#cropImage").modal("show");
                                        $("#cropImage").on('shown.bs.modal',function(){jcropHandler()});
                                        break;
                                    case "fl_delete":
                                        removeFile();
                                        break;
                                    case "fl_rename":
                                            provideFileRename();
                                            $('#addFolder').modal('show');
                                        break;
                                    case "fl_url":
                                        $('#addFolder').modal('show');
                                        //provideFileUrl();
                                        break;
                                    case "fl_download":

                                        $('#addFolder').modal('show');
                                        break;
                                    default:
                                }
                            }
                        });


                        //-----------------------pop up-----------------------------------
                        $('#popAddFolder').click(function () {
                            provideFolderAdd();
                        });
                        //----------------------------------------------------------------

                        $('#btnModal').click(function () {
                            var action = $(this).attr('data-action');
                            var id = $(this).attr('data-id');
                            $.ajax({
                                type: "POST",
                                url: action,
                                data: {name: $('#modal-text').val(), id: id}
                            })
                                .done(function (msg) {
                                    $.filemanager.databind();
                                });
                        });

                        $('#btnFolder').click(function () {
                            alert($('#currentPath').attr('data-parent-id'));
                        });
                        //------------------file------------------------------------
                        $('.file-box').mousedown(function (event) {
                            $('#currentPath').attr('data-file-id', $(this).attr('data-id'));
                            $('#currentPath').attr('data-fullname', $(this).attr('data-fullname'));
                            $('#filesize').html($(this).attr('data-size'));
                            $('#fl_crop').attr("data-image",$(this).children().attr('href'));
                            switch (event.which) {
                                case 3:
                                    break;
                                default:
                                    //alert($('#currentPath').attr('data-file-id')+'-'+$('#currentPath').attr('data-fullname'));
                            }
                        });
                        ;
                        //------------------folder------------------------------------
                        $('.folder-box').mousedown(function (event) {
                            $('#currentPath').attr('data-folder-id', $(this).attr('data-id'));
                            $('#currentPath').attr('data-fullname', $(this).attr('data-fullname'));
                            switch (event.which) {
                                case 3:
                                    break;
                                default:
                                {
                                    $('#currentPath').attr('data-parent-id',$('#currentPath').attr('data-folder-id'));
                                    $('#fileParent').val($('#currentPath').attr('data-folder-id'));

                                    $.route.nextFolder($('#currentPath').attr('data-folder-id')
                                        ,$('#currentPath').attr('data-fullname'));
                                    $.filemanager.databind();
                                }
                            }
                        });
                        //-----------------------route-click-----------------------------------
                        $('#filemanagerRoute li').click(function(){

                                var clicked=$(this);
                                if(clicked.attr('data-id')!=1)
                                clicked.html(clicked.attr('data-name'));
                                clicked.nextAll('li').remove();

                                $.route.selectFolder(clicked.attr('data-id')
                                    ,clicked.attr('data-name'));

                            }
                        );


                    });
            });
    }
}