function provideFolderAdd()
{
    var current =$('#currentPath');

    $('#modal-title').html('ایجاد پوشه');

    $('#btnModal').attr('data-action',current.attr('data-folder-add'));

    $('#btnModal').attr('data-id',current.attr('data-parent-id'));


    $('#modal-text').val('');

    $('#modal-text').attr('placeholder',"نام پوشه ..." );

    $('#btnModal').text('ایجاد پوشه');

}
function provideFolderRename()
{
    var current =$('#currentPath');

    $('#modal-title').html('تغییر نام پوشه');


    $('#btnModal').attr('data-action',current.attr('data-folder-rename'));

    $('#btnModal').attr('data-id',current.attr('data-folder-id'));


    $('#modal-text').val(current.attr('data-fullname'));

    $('#modal-text').attr('placeholder','');

    $('#btnModal').text('تغییر نام');
}
function provideFileRename()
{
    var current =$('#currentPath');

    $('#modal-title').html('تغییر نام فایل');


    $('#btnModal').attr('data-action',current.attr('data-file-rename'));

    $('#btnModal').attr('data-id',current.attr('data-file-id'));



    $('#modal-text').val(current.attr('data-fullname'));

    $('#modal-text').attr('placeholder','');

    $('#btnModal').text('تغییر نام');
}
function removeFile()
{
    var current =$('#currentPath');
    $.ajax({
        type: "POST",
        url:current.attr('data-file-delete') ,
        data: {id:current.attr('data-file-id') }
    })
        .done(function( msg ) {
            $.filemanager.databind();
        });
}
function removeFolder()
{
    var current =$('#currentPath');
    $.ajax({
        type: "POST",
        url:current.attr('data-folder-delete') ,
        data: { id:current.attr('data-folder-id') }
    })
        .done(function( msg ) {
            $.filemanager.databind();
        });
}

function previewImage(){
    //$(this).fancybox();
    var current =$('#currentPath');
    alert(current.attr('data-id'));
}


function provideFileUrl(){

    $("#modal-title").text("آدرس فایل");
    //$("#modal-text").val($(e.target).attr("data-image"));
    //alert($(e.target).attr("data-image"));

    $('#btnModal').text('');
}