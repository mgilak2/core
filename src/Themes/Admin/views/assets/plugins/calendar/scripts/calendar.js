﻿(function ($) {
    function PersianCalendar(language) {
        this.local = this.regional[language || ''] || this.regional[''];
    }
    PersianCalendar.prototype = new $.calendars.baseCalendar;
    $.extend(PersianCalendar.prototype,
					{
					    name: 'Persian',
					    jdEpoch: 1948320.5,
					    daysPerMonth: [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29],
					    hasYearZero: false,
					    minMonth: 1,
					    firstMonth: 1,
					    minDay: 1,
					    regional: {
					        '': {
					            name: 'Persian',
					            epochs: ['BP', 'AP'],
					            monthNames: ['فروردين', 'ارديبهشت', 'خرداد', 'تير', 'مرداد', 'شهريور', 'مهر', 'آبان', 'آذر', 'دي', 'بهمن', 'اسفند'],
					            monthNamesShort: ['Far', 'Ord', 'Kho', 'Tir', 'Mor', 'Sha', 'Meh', 'Aba', 'Aza', 'Day', 'Bah', 'Esf'],
					            dayNames: ['یکشنبه', 'دوشنبه', 'سه شنبه', 'چهارشنبه', 'پنجشنبه', 'جمعه', 'شنبه'],
					            dayNamesShort: ['ی', 'د', 'س', 'چ', 'پ', 'ج', 'ش'],
					            dayNamesMin: ['ی', 'د', 'س', 'چ', 'پ', 'ج', 'ش'],
					            dateFormat: 'yyyy/mm/dd',
					            firstDay: 6,
					            isRTL: true
					        }
					    },
					    leapYear: function (year) {
					        var date = this._validate(year, this.minMonth,
									this.minDay, $.calendars.local.invalidYear);
					        return (((((date.year() - (date.year() > 0 ? 474
									: 473)) % 2820) + 474 + 38) * 682) % 2816) < 682;
					    },
					    weekOfYear: function (year, month, day) {
					        var checkDate = this.newDate(year, month, day);
					        checkDate.add(-((checkDate.dayOfWeek() + 1) % 7), 'd');
					        return Math.floor((checkDate.dayOfYear() - 1) / 7) + 1;
					    },
					    daysInMonth: function (year, month) {
					        var date = this._validate(year, month, this.minDay,
									$.calendars.local.invalidMonth);
					        return this.daysPerMonth[date.month() - 1]
									+ (date.month() == 12 && this.leapYear(date.year()) ? 1 : 0);
					    },
					    weekDay: function (year, month, day) {
					        return this.dayOfWeek(year, month, day) != 5;
					    },
					    toJD: function (year, month, day) {
					        var date = this._validate(year, month, day,
									$.calendars.local.invalidDate);
					        year = date.year();
					        month = date.month();
					        day = date.day();
					        var epBase = year - (year >= 0 ? 474 : 473);
					        var epYear = 474 + mod(epBase, 2820);
					        return day
									+ (month <= 7 ? (month - 1) * 31 : (month - 1) * 30 + 6)
									+ Math.floor((epYear * 682 - 110) / 2816)
									+ (epYear - 1) * 365
									+ Math.floor(epBase / 2820) * 1029983
									+ this.jdEpoch - 1;
					    },
					    fromJD: function (jd) {
					        jd = Math.floor(jd) + 0.5;
					        var depoch = jd - this.toJD(475, 1, 1);
					        var cycle = Math.floor(depoch / 1029983);
					        var cyear = mod(depoch, 1029983);
					        var ycycle = 2820;
					        if (cyear != 1029982) {
					            var aux1 = Math.floor(cyear / 366);
					            var aux2 = mod(cyear, 366);
					            ycycle = Math.floor(((2134 * aux1)
										+ (2816 * aux2) + 2815) / 1028522)
										+ aux1 + 1;
					        }
					        var year = ycycle + (2820 * cycle) + 474;
					        year = (year <= 0 ? year - 1 : year);
					        var yday = jd - this.toJD(year, 1, 1) + 1;
					        var month = (yday <= 186 ? Math.ceil(yday / 31) : Math.ceil((yday - 6) / 30));
					        var day = jd - this.toJD(year, month, 1) + 1;
					        return this.newDate(year, month, day);
					    }
					});
    function mod(a, b) {
        return a - (b * Math.floor(a / b));
    }
    $.calendars.calendars.persian = PersianCalendar;
    $.calendars.calendars.jalali = PersianCalendar;
})(jQuery);


(function ($) {
    $.calendarsPicker.regionalOptions['fa'] = {
        renderer: $.calendarsPicker.defaultRenderer,
        prevText: '<i class="fa fa-angle-right"></i>', prevStatus: 'نمايش ماه قبل',
        prevJumpText: '&#x3c;&#x3c;', prevJumpStatus: '',
        nextText: '<i class="fa fa-angle-left"></i>', nextStatus: 'نمايش ماه بعد',
        nextJumpText: '&#x3e;&#x3e;', nextJumpStatus: '',
        currentText: 'امروز', currentStatus: 'نمايش ماه جاري',
        todayText: 'امروز', todayStatus: 'نمايش ماه جاري',
        clearText: '<i class="fa fa-minus-circle"></i>', clearStatus: 'پاک کردن تاريخ جاري',
        closeText: '<i class="fa fa-times"></i>', closeStatus: 'بستن بدون اعمال تغييرات',
        yearStatus: 'نمايش سال متفاوت', monthStatus: 'نمايش ماه متفاوت',
        weekText: 'هف', weekStatus: 'هفتهِ سال',
        dayStatus: 'انتخاب D, M d', defaultStatus: 'انتخاب تاريخ',
        isRTL: true
    };
    $.calendarsPicker.setDefaults($.calendarsPicker.regionalOptions['fa']);
})(jQuery);




$(document).ready(function () {
    var jalaliDate = new Array();
    var hijriDate = new Array();
    var gregorianDate = new Array();
    var fixd;
    $('.gregorianDate').calendarsPicker({
        dateFormat: 'yyyy/m/d', yearRange: '1970:2020',
    });
    $('.jalaliDate , .jalaliDatePicker').calendarsPicker({
        calendar: $.calendars.instance('persian'), dateFormat: 'yyyy/m/d', yearRange: '1330:1400',
    });
    $('#dateConverter input').not('.hijriDate').click(function () {
        $('.gregorianDate,.jalaliDate,.hijriDate').val('');
    });

    $('#dateConverter input').blur(function () {
        $('.gregorianDate').val('میلادی');
        $('.jalaliDate').val('شمسی');
        $('.hijriDate').val('قمری');
    });
    $('#doConvert').click(function () {

        if ($('.gregorianDate').val() != 'میلادی') {
            gregorianDate = $('.gregorianDate').val().split('/');
            gregorianDate = JalaliDate.gregorianToJalali(gregorianDate[0], gregorianDate[1], gregorianDate[2]);
            $('.jalaliDate').val(gregorianDate[0] + '/' + gregorianDate[1] + '/' + gregorianDate[2]);
        }

        if ($('.jalaliDate').val() != 'شمسی') {
            jalaliDate = $('.jalaliDate').val().split('/');
            jalaliDate = JalaliDate.jalaliToGregorian(jalaliDate[0], jalaliDate[1], jalaliDate[2]);
            $('.gregorianDate').val(jalaliDate[0] + '/' + jalaliDate[1] + '/' + jalaliDate[2]);
        }

        gregorianDate = $('.gregorianDate').val().split('/');
        hijriDate = gregorianToHijri(parseInt(gregorianDate[0]), parseInt(gregorianDate[1]), parseInt(gregorianDate[2]));
        $('.hijriDate').val(hijriDate[0] + '/' + hijriDate[1] + '/' + hijriDate[2]);

    });

    JalaliDate = {
        g_days_in_month: [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31],
        j_days_in_month: [31, 31, 31, 31, 31, 31, 30, 30, 30, 30, 30, 29]
    };

    JalaliDate.jalaliToGregorian = function (j_y, j_m, j_d) {
        j_y = parseInt(j_y);
        j_m = parseInt(j_m);
        j_d = parseInt(j_d);
        var jy = j_y - 979;
        var jm = j_m - 1;
        var jd = j_d - 1;

        var j_day_no = 365 * jy + parseInt(jy / 33) * 8 + parseInt((jy % 33 + 3) / 4);
        for (var i = 0; i < jm; ++i) j_day_no += JalaliDate.j_days_in_month[i];

        j_day_no += jd;

        var g_day_no = j_day_no + 79;

        var gy = 1600 + 400 * parseInt(g_day_no / 146097); /* 146097 = 365*400 + 400/4 - 400/100 + 400/400 */
        g_day_no = g_day_no % 146097;

        var leap = true;
        if (g_day_no >= 36525) /* 36525 = 365*100 + 100/4 */ {
            g_day_no--;
            gy += 100 * parseInt(g_day_no / 36524); /* 36524 = 365*100 + 100/4 - 100/100 */
            g_day_no = g_day_no % 36524;

            if (g_day_no >= 365)
                g_day_no++;
            else
                leap = false;
        }

        gy += 4 * parseInt(g_day_no / 1461); /* 1461 = 365*4 + 4/4 */
        g_day_no %= 1461;

        if (g_day_no >= 366) {
            leap = false;

            g_day_no--;
            gy += parseInt(g_day_no / 365);
            g_day_no = g_day_no % 365;
        }

        for (var i = 0; g_day_no >= JalaliDate.g_days_in_month[i] + (i == 1 && leap) ; i++)
            g_day_no -= JalaliDate.g_days_in_month[i] + (i == 1 && leap);
        var gm = i + 1;
        var gd = g_day_no + 1;

        return [gy, gm, gd];
    };

    JalaliDate.checkDate = function (j_y, j_m, j_d) {
        return !(j_y < 0 || j_y > 32767 || j_m < 1 || j_m > 12 || j_d < 1 || j_d >
            (JalaliDate.j_days_in_month[j_m - 1] + (j_m == 12 && !((j_y - 979) % 33 % 4))));
    };

    JalaliDate.gregorianToJalali = function (g_y, g_m, g_d) {
        g_y = parseInt(g_y);
        g_m = parseInt(g_m);
        g_d = parseInt(g_d);
        var gy = g_y - 1600;
        var gm = g_m - 1;
        var gd = g_d - 1;

        var g_day_no = 365 * gy + parseInt((gy + 3) / 4) - parseInt((gy + 99) / 100) + parseInt((gy + 399) / 400);

        for (var i = 0; i < gm; ++i)
            g_day_no += JalaliDate.g_days_in_month[i];
        if (gm > 1 && ((gy % 4 == 0 && gy % 100 != 0) || (gy % 400 == 0)))
            /* leap and after Feb */
            ++g_day_no;
        g_day_no += gd;

        var j_day_no = g_day_no - 79;

        var j_np = parseInt(j_day_no / 12053);
        j_day_no %= 12053;

        var jy = 979 + 33 * j_np + 4 * parseInt(j_day_no / 1461);

        j_day_no %= 1461;

        if (j_day_no >= 366) {
            jy += parseInt((j_day_no - 1) / 365);
            j_day_no = (j_day_no - 1) % 365;
        }

        for (var i = 0; i < 11 && j_day_no >= JalaliDate.j_days_in_month[i]; ++i) {
            j_day_no -= JalaliDate.j_days_in_month[i];
        }
        var jm = i + 1;
        var jd = j_day_no + 1;

        return [jy, jm, jd];
    };
    function isGregLeapYear(year) {
        return year % 4 == 0 && year % 100 != 0 || year % 400 == 0;
    }


    function gregToFixed(year, month, day) {
        var a = Math.floor((year - 1) / 4);
        var b = Math.floor((year - 1) / 100);
        var c = Math.floor((year - 1) / 400);
        var d = Math.floor((367 * month - 362) / 12);

        if (month <= 2)
            e = 0;
        else if (month > 2 && isGregLeapYear(year))
            e = -1;
        else
            e = -2;

        return 1 - 1 + 365 * (year - 1) + a - b + c + d + e + day;
    }

    function Hijri(year, month, day) {
        this.year = year;
        this.month = month;
        this.day = day;
        this.toFixed = hijriToFixed;
        this.toString = hijriToString;
    }

    function hijriToFixed() {
        return this.day + Math.ceil(29.5 * (this.month - 1)) + (this.year - 1) * 354 +
            Math.floor((3 + 11 * this.year) / 30) + 227015 - 1;
    }

    function hijriToString() {

        return [this.year, this.month, this.day];
    }

    function fixedToHijri(f) {
        var i = new Hijri(1100, 1, 1);
        i.year = Math.floor((30 * (f - 227015) + 10646) / 10631);
        var i2 = new Hijri(i.year, 1, 1);
        var m = Math.ceil((f - 29 - i2.toFixed()) / 29.5) + 1;
        i.month = Math.min(m, 12);
        i2.year = i.year;
        i2.month = i.month;
        i2.day = 1;
        i.day = f - i2.toFixed() + 1;
        return i;
    }

    function gregorianToHijri(y, m, d) {
        fixd = gregToFixed(y, m, d);
        var h = new Hijri(1421, 11, 28);
        h = fixedToHijri(fixd);
        return h.toString();
    }

});

