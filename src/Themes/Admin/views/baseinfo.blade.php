@extends(extendAdmin("dashboard"))

@section("master.ticket.form.add")
@section("pageTitle")
<i class="icon-office"></i>
اطلاعات پایه

@endsection
<div class="navbar navbar-toolbar navbar-default">
    <ul class="nav navbar-nav">
        <li><a href="/admin/continent/list">قاره</a></li>
        <li class="active"><a href="/admin/country/list">کشور</a></li>
        <li><a href="/admin/city/list">شهر</a></li>
        <li><a href="/admin/place_category/list">دسته بندی اماکن</a></li>
        <li><a href="/admin/place_sub_category/list">انواع اماکن</a></li>
        <li><a href="/admin/place/list">اماکن</a></li>
        <li><a href="/admin/airline/list">ایرلاین</a></li>
    </ul>
</div>
@stop