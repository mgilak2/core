<!DOCTYPE html>
<html>
<head>
    <title>Phoneme</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta charset="UTF-8"/>
    <link rel="icon" type="image/ico" href="{{ asset('Admin/assets/images/favicon.ico') }}"/>
    <!-- Bootstrap -->
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/boostrapv3/css/bootstrap.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/font-awesome/css/font-awesome.min.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/boostrapv3/css/todc-bootstrap.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/boostrapv3/css/bootstrap-rtl.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/todc-select/select2.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/iconmoon/style.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/calendar/ariasun.j.c.picker.css') }}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/redactor/redactor.css') }}">
    <link rel="stylesheet" href="{{ asset('Admin/assets/plugins/fancybox/source/jquery.fancybox.css')}}"/>
    <link rel="stylesheet" href="{{ asset('Admin/assets/css/RnbCss.css') }}"/>
    <script src="{{ asset('Admin/assets/plugins/jquery-1.8.3.min.js') }}"></script>

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
    <![endif]-->
</head>
<body>


