<?php

use AdminTheme\AdminThemeServiceProvider;
use Striped\StripedServiceProvider;


return [
    "Admin" => ['serviceProvider' => AdminThemeServiceProvider::class,
        "autoLoader" => __DIR__ . "/Admin/vendor/autoload.php"],

    "Striped" => ['serviceProvider' => StripedServiceProvider::class,
        "autoLoader" => __DIR__ . "/Striped/vendor/autoload.php"],
];