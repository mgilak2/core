<?php

namespace Phoneme;

use Illuminate\Contracts\Foundation\Application;

class BlogInitializer
{
    /**
     * @var ComponentManager
     */
    private $componentManager;
    /**
     * @var ThemeManager
     */
    private $themeManager;
    /**
     * @var Application
     */
    private $app;
    /**
     * @var HookManager
     */
    private $hook;

    public function __construct(
        Application $app,
        HookManager $hook,
        ComponentManager $componentManager,
        ThemeManager $themeManager)
    {
        $this->componentManager = $componentManager;
        $this->themeManager = $themeManager;
        $this->app = $app;
        $this->hook = $hook;
    }

    public function load()
    {
        $this->loadComponents();
        $this->loadHooks();
        $this->loadThemes();
    }

    private function loadComponents()
    {
        $this->componentManager->setAllServiceProvidersFrom(__DIR__ . "/../Components");
        $this->componentManager->registerServiceProviders();
        $this->componentManager->callBootMethodOfServiceProviders();
    }

    private function loadHooks()
    {
        $this->hook->initialsAllResponders();
    }

    private function loadThemes()
    {
        $this->themeManager->setAllServiceProvidersFrom(__DIR__ . "/../Themes");
        $this->themeManager->registerServiceProviders();
        $this->themeManager->callBootMethodOfServiceProviders();
    }
}