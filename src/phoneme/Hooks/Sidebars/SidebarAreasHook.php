<?php

namespace Phoneme\Hooks\Menus;

use Phoneme\Contracts\Hook;
use Sidebars\Contracts\SidebarArea;
use Sidebars\SidebarHookResponder;

class SidebarAreasHook implements SidebarArea, Hook
{

    public function getAreas()
    {
        return [
            ["name" => "left sidebar", 'id' => 'sfkjsdhf']
        ];
    }

    public function getResponder()
    {
        return SidebarHookResponder::class;
    }
}