<?php

namespace Phoneme;

use Illuminate\Contracts\Foundation\Application;

class HookManager
{
    private $baseFolder;
    private $implementations;
    /**
     * @var Application
     */
    private $app;

    public function __construct(Application $app, $baseFolder)
    {
        $this->baseFolder = $baseFolder;
        $this->app = $app;
    }

    public function initialsAllResponders()
    {
        foreach ($this->getAllHooksImplementations() as $hook){
            $responder = $this->app->make($hook->getResponder());
            $responder->add($hook);
        }
    }

    private function getAllHooksImplementations()
    {
        $this->implementations = [];
        foreach ($this->getAllHooks() as $hookClass)
            $this->implementations[] = $this->app->make($hookClass);

        return $this->implementations;
    }

    private function getAllHooks()
    {
        return include($this->baseFolder . "/hooks.php");
    }

}