<?php

use Phoneme\Hooks\Menus\MenuAreasHook;
use Phoneme\Hooks\Menus\SidebarAreasHook;

return [
    MenuAreasHook::class,
    SidebarAreasHook::class,
];