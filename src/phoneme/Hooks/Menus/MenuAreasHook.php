<?php

namespace Phoneme\Hooks\Menus;

use Menus\Contracts\MenuArea;
use Menus\MenuHookResponder;
use Phoneme\Contracts\Hook;

class MenuAreasHook implements MenuArea, Hook
{

    public function getAreas()
    {
        return [
            "header menu",
            "footer menu",
        ];
    }

    public function getResponder()
    {
        return MenuHookResponder::class;
    }
}