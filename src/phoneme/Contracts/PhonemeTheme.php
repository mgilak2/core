<?php

namespace Phoneme\Contracts;

interface PhonemeTheme
{
    public function register();
}