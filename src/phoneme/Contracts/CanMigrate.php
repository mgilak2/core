<?php

namespace Phoneme\Contracts;

interface CanMigrate
{
    public function getMigrationArguments();
}