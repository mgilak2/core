<?php

namespace Phoneme\Contracts;

interface Initializable
{
    public function boot();
}