<?php

namespace Phoneme\Contracts;

interface CanSeed
{
    public function getSeedArguments();
}