<?php

namespace Phoneme\Contracts;

use Symfony\Component\Process\Process;

interface Composerable
{
    /**
     * @return Process
     */
    public function getComposerCommand();
}