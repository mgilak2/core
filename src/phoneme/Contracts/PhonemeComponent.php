<?php

namespace Phoneme\Contracts;

interface PhonemeComponent
{
    public function register();
}