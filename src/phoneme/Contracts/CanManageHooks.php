<?php

namespace Phoneme\Contracts;

interface CanManageHooks
{
    public function add(Hook $hook);
}