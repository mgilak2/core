<?php

namespace Phoneme\Contracts;

interface Hook
{
    public function getResponder();
}