<?php

function settings($key, $default = "")
{
    $serializedSettings = \DB::table("settings")
        ->where("key", "cms")->first();
    $settings = unserialize($serializedSettings->value);
    if (isset($settings[$key]))
        return $settings[$key];

    return $default;
}

function adminSidebarWidgets($sidebar)
{
    return \DB::table("sidebar_widget")
        ->where("sidebar", $sidebar)
        ->get();
}

function adminSidebarWidget($widgetIdString, $sidebar)
{
    return \DB::table("widgets")
        ->where("sidebar", $sidebar)
        ->where('id_string', $widgetIdString)
        ->orderBy('id', 'desc')
        ->first();
}

function widget($id_string, $sidebar_id)
{
    $widget = \DB::table("widgets")
        ->where('id_string', $id_string)
        ->where('sidebar', $sidebar_id)
        ->orderBy('id', 'desc')
        ->first();

    $widgetService = app()->make($widget->name);
    $widgetService->id_string = $id_string;
    $widgetService->theme = "";
    $widgetService->dir = "";

    return $widgetService;
}

function widgetOptions($idString)
{
    return \DB::table("widgets")
        ->where('id_string', $idString)
        ->orderBy('id', 'desc')
        ->first();
}

function widgetsOfSidebar($sidebar)
{
    $widgets = \DB::table("sidebar_widget")
        ->where("sidebar", $sidebar)
        ->get();

    foreach ($widgets as $widget) {
        $widget_object = widget($widget->id_string, $sidebar);
        $widget_object->display($widget->id_string);
    }
}

function phoneme_serialize($data)
{
    return base64_encode(serialize($data));
}

function phoneme_unserialize($data)
{
    return unserialize(base64_decode($data));
}