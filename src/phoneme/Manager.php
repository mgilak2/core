<?php

namespace Phoneme;

use Illuminate\Contracts\Foundation\Application;
use Phoneme\Contracts\Initializable;

abstract class Manager
{
    protected $environment;
    protected $serviceProviders;
    protected $components;
    protected $baseFolder;
    /**
     * @var Application
     */
    protected $app;

    public function __construct(Application $app)
    {
        $this->app = $app;
    }

    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    public function setAllServiceProvidersFrom($baseDirectory)
    {
        $this->serviceProviders = require $baseDirectory
            . "/serviceproviders.php";
    }

    public function registerServiceProviders()
    {
        foreach ($this->getAllComponents() as $component)
            $component->register();
    }

    protected function getAllComponents()
    {
        if (is_null($this->components))
            foreach ($this->serviceProviders as $name => $serviceProvider)
                if ($this->isAutoloadExists($serviceProvider['autoLoader'])) {
                    $this->requireServiceAutoLoaderClassFile($serviceProvider['autoLoader']);
                    $this->components[] = $this->createAnInstanceOfServiceProvider($serviceProvider['serviceProvider']);
                }

        return $this->components;
    }

    protected function isAutoloadExists($autoLoader)
    {
        if (is_file($autoLoader))
            return true;
        return false;
    }

    protected function requireServiceAutoLoaderClassFile($autoLoader)
    {
        include_once($autoLoader);
    }

    protected function createAnInstanceOfServiceProvider($serviceProviderClass)
    {
        return $this->app->make($serviceProviderClass, [$this->app]);
    }

    public function callBootMethodOfServiceProviders()
    {
        foreach ($this->getAllComponents() as $component)
            $this->callBootOnServiceProviderIfIsInstanceOfInitializable($component);
    }

    /**
     * @param Initializable $component
     */
    protected function callBootOnServiceProviderIfIsInstanceOfInitializable($component)
    {
        if ($this->isInitializable($component)) {
            $component->boot();
        }
    }

    protected function isInitializable($component)
    {
        return $component instanceof Initializable;
    }

    protected function displayMessageIntoConsole($message)
    {
        if ($this->environment == "tinker")
            echo $message . "\n";
    }
}