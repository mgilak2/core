<?php

namespace Phoneme\Providers;

use Illuminate\Support\ServiceProvider;
use Phoneme\BlogInitializer;
use Phoneme\ComponentManager;
use Phoneme\HookManager;
use Phoneme\ThemeManager;

class PhonePlatformServiceProvider extends ServiceProvider
{
    public function boot(BlogInitializer $blogInitializer)
    {
        $blogInitializer->load();
    }

    public function register()
    {
        $this->app->bind('\Phoneme\ComponentLoader', function ($app) {
            return new ComponentManager($app, __DIR__ . "/../Components");
        });

        $this->app->bind('\Phoneme\ThemeLoader', function ($app) {
            return new ThemeManager($app, __DIR__ . "/../Themes");
        });

        $this->app->bind('\Phoneme\HookManager', function ($app) {
            return new HookManager($app, __DIR__ . "/../Phoneme/Hooks");
        });

        $this->app->bind('\Phoneme\BlogInitializer', function ($app) {
            return new BlogInitializer(
                $app,
                $app['\Phoneme\HookManager'],
                $app['\Phoneme\ComponentLoader'],
                $app['\Phoneme\ThemeLoader']
            );
        });
    }
}