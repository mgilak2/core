<?php

namespace Phoneme\Installation;

use Illuminate\Contracts\Console\Kernel as Artisan;
use Illuminate\Contracts\Foundation\Application;
use Phoneme\Contracts\CanMigrate;
use Phoneme\Manager;

class Migrator extends Manager
{
    protected $serviceProviders;
    protected $components;
    protected $arguments;
    /**
     * @var Artisan
     */
    private $artisan;

    public function __construct(Application $app, Artisan $artisan)
    {
        parent::__construct($app);
        $this->app = $app;
        $this->artisan = $artisan;
    }

    public function migrateAll()
    {
        foreach ($this->getAllMigrationCommandsArguments() as $commandHolder)
            $this->migrate($commandHolder);
    }

    private function getAllMigrationCommandsArguments()
    {
        foreach ($this->getAllComponents() as $component)
            if ($component instanceof CanMigrate)
                $this->arguments[] = $component->getMigrationArguments();

        return $this->arguments;
    }

    private function migrate($arguemnts)
    {
        $this->artisan->call('migrate', $arguemnts);
    }
}