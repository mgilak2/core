<?php

namespace Phoneme\Installation;

use Illuminate\Contracts\Console\Kernel as Artisan;
use Illuminate\Contracts\Foundation\Application;
use Phoneme\Contracts\CanSeed;

class Seeder
{
    protected $serviceProviders;
    protected $components;
    protected $arguments;
    /**
     * @var Application
     */
    private $app;
    /**
     * @var Artisan
     */
    private $artisan;

    public function __construct(Application $app, Artisan $artisan)
    {
        $this->app = $app;
        $this->artisan = $artisan;
    }

    public function seedAll()
    {
        foreach ($this->getAllSeedCommandsArguments() as $commandHolder)
            $this->seed($commandHolder);
    }

    private function getAllSeedCommandsArguments()
    {
        foreach ($this->getAllComponents() as $component)
            if ($component instanceof CanSeed)
                $this->arguments[] = $component->getSeedArguments();

        return $this->arguments;
    }

    private function getAllComponents()
    {
        foreach ($this->serviceProviders as $name => $attributes)
            $this->components[] = $this->createAnInstanceOfServiceProvider($attributes['serviceProvider']);
        return $this->components;
    }

    private function createAnInstanceOfServiceProvider($serviceProviderClass)
    {
        return $this->app->make($serviceProviderClass, [$this->app]);
    }

    private function seed($arguemnts)
    {
        $this->artisan->call('db:seed', $arguemnts);
    }

    public function setAllServiceProvidersFrom($baseFolder)
    {
        $this->serviceProviders = require $baseFolder . "/serviceproviders.php";
    }
}