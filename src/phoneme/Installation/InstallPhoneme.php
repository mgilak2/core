<?php

namespace Phoneme\Installation;

class InstallPhoneme
{
    protected $componentsBaseFolder = __DIR__ . "/../../Components/";
    protected $themesBaseFolder = __DIR__ . "/../../Themes/";
    protected $environment = "";

    /**
     * @var Migrator
     */
    private $migrator;
    /**
     * @var Seeder
     */
    private $seeder;
    /**
     * @var ComposerManager
     */
    private $composer;

    public function __construct(ComposerManager $composer, Migrator $migrator, Seeder $seeder)
    {
        $this->migrator = $migrator;
        $this->seeder = $seeder;
        $this->composer = $composer;
    }

    public function setEnvironment($environment)
    {
        $this->environment = $environment;
    }

    public function compose()
    {
        $this->composer->setAllServiceProvidersFrom($this->componentsBaseFolder);
        $this->composer->setEnvironment($this->environment);

        $this->composer->setBaseFolder($this->componentsBaseFolder);
        $this->composer->dumpAutoloadAll();
        $this->composer->composeAll();

        $this->composer->setBaseFolder($this->themesBaseFolder);
        $this->composer->dumpAutoloadAll();
        $this->composer->composeAll();
    }

    public function migrate()
    {
        $this->migrator->setAllServiceProvidersFrom($this->componentsBaseFolder);
        $this->composer->setEnvironment($this->environment);
        $this->migrator->migrateAll();
    }

    public function seed()
    {
        $this->seeder->setAllServiceProvidersFrom($this->componentsBaseFolder);
        $this->composer->setEnvironment($this->environment);
        $this->seeder->seedAll();
    }
}