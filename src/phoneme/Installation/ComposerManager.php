<?php

namespace Phoneme\Installation;

use Phoneme\Contracts\Composerable;
use Phoneme\Manager;
use Symfony\Component\Process\Process;

class ComposerManager extends Manager
{
    protected $commands;

    public function dumpAutoloadAll()
    {
        $dumpCommand = "composer dump-autoload -o";
        $command = new Process("composer dump-autoload -o");
        $command->setTimeout(null);

        foreach ($this->serviceProviders as $direcotryName => $attributes) {
            $command->setCommandLine($dumpCommand);
            $this->cdIntoADirectory($command, $direcotryName);
            $this->displayMessageIntoConsole("dump-autoload -o in " . $this->baseFolder . $direcotryName);
            $command->run();
            $this->displayResultMessageIntoConsole($command, $direcotryName);
        }
    }

    private function cdIntoADirectory(Process $command, $directoryName)
    {
        $commandLine = $command->getCommandLine();
        $commandLine = "cd " . $this->baseFolder
            . $directoryName
            . " && " . $commandLine;

        $command->setCommandLine($commandLine);
    }

    private function displayResultMessageIntoConsole(Process $command, $direcotryName)
    {
        if ($command->isSuccessful())
            $this->displayMessageIntoConsole("dumpping was ok in " . $direcotryName . " command was : " . $command->getCommandLine());
        else
            $this->displayMessageIntoConsole("dumpping failed in " . $direcotryName . ", error is : " . $command->getErrorOutput());
    }

    public function composeAll()
    {
        foreach ($this->getAllComposerCommands() as $commandHolder) {
            $this->setDirectoryToBaseFolderOnCommand($commandHolder);
            $this->compose($commandHolder);
        }
    }

    private function getAllComposerCommands()
    {
        foreach ($this->getAllComponents() as $component)
            if ($component instanceof Composerable)
                $this->commands[] = $component->getComposerCommand();

        return $this->commands;
    }

    private function setDirectoryToBaseFolderOnCommand(Process $command)
    {
        $commandLine = $command->getCommandLine();
        $commandLine = "cd " . $this->baseFolder . " && " . $commandLine;
        $command->setCommandLine($commandLine);
    }

    private function compose(Process $command)
    {
        $command->setTimeout(null);
        $command->run();
    }

    public function setBaseFolder($baseFolder)
    {
        $this->baseFolder = $baseFolder;
    }
}